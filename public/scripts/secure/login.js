$(function(){
	var element = 'login';
	$('#login').click(function(){
		login_(element);
	});
	$('#form').submit(function(){
		return false;
	});
});
function login_(element){
	disabled_();
	var error = '';
	var email = $('#form #email').val(),
		password = $('#form #password').val();
	if(email.length < 5){
		$('#email_').addClass('error');
		error += '<li>' + language['pleaseEnterYourEmail'] + '</li>';
	}
	else if(!validator.isEmail(email)){
		$('#email_').addClass('error');
		error += '<li>' + language['pleaseEnterAValidEmail'] + '</li>';
	}
	if(password.length < 8){
		$('#password_').addClass('error');
		error += '<li>' + language['pleaseEnterYourPassword'] + '</li>';
	}
	if(error == ''){
		var data = {
			email: email,
			password: password
		};
		sendRequest_(element, data);
	}
	else
		error_(error);
}

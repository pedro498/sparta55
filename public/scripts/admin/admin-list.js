function initialize_(table, element){
	$('#add').click(function(){
		add_(element);
	});
	$('#table tbody').on('click', 'tr', function(){
		edit_($(this).children(':first').html(), element);
	});
	$('#table tbody').on('click', '.delete', function(){
		delete_($(this).parents('tr').children(':first').html(), element, table, $(this).parents('tr'));
		return false;
	});
	$('#table tbody').on('click', '.download', function(e){
		window.open($(this).parents('a').attr('href'));
		return false;
	});
}
function add_(element){
	location_('/admin/' + element + '/edit');	
}
function edit_(_id, element){
	location_('/admin/' + element + '/edit/' + _id);
}
function delete_(_id, element, table, row){
	disabled_();
	deleteRecord_(_id, element, table, row);
}
function deleteRecord_(_id, element, table, row){
	$.ajax({
		url: '../../api/' + element + '/' + _id,
		dataType: 'JSON',
		type: 'DELETE',
		cache: false,
		timeout: 5000,
		success: function(response){
			table.row(row).remove().draw();
			success_('<li>' + language['theRecordWasDeleted'] + '</li>');
		},
		error: function(error){
			error_('<li>' + language['thereWasAnErrorDeletingThisRecord'] + '</li>');
		}
	});
}
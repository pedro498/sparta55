// Modules declarations.
var mongoose = require('mongoose'),
	validation = require('../libraries/validation.js'),
	languages = require('../libraries/languages.js'),
	language = 'es',
	validator = require('validator'),
	schema = mongoose.Schema,
	locationsSchema = new schema({
		name: {
			type: String,
			min: 3,
			required: true,
			unique: true,
			index: true
		},
		email: {
			type: String,
			min: 5,
			required: true,
			unique: true,
			index: true
		},
		phone: {
			type: String,
			min: 3,
			required: true
		},
		address: {
			type: String,
			min: 3,
			required: true
		},
		postCode: {
			type: String,
			min: 3,
			required: true
		},
		price20: {
			type: Number,
			min: 0,
			required: true
		},
		price55: {
			type: Number,
			min: 0,
			required: true
		},
		city: {
			type: schema.Types.ObjectId,
			ref: 'Cities',
			required: true,
			index: true
		},
		classes: [{
			type: schema.Types.ObjectId,
			ref: 'Classes',
			index: true
		}],
		createdAt: {
			type: Date
		},
		updatedAt: {
			type: Date
		}
	});
locationsSchema.pre('validate', function(next){
	if(typeof this.name === 'undefined' || this.name.length < 3)
		validation.nextError(next, 'pleaseEnterTheName');
	else if(typeof this.email === 'undefined' || this.email.length < 5)
		validation.nextError(next, 'pleaseEnterTheEmail');
	else if(!validator.isEmail(this.email))
		validation.nextError(next, 'pleaseEnterAValidEmail');
	if(typeof this.phone === 'undefined' || this.phone.length < 3)
		validation.nextError(next, 'pleaseEnterThePhone');
	if(typeof this.address === 'undefined' || this.address.length < 3)
		validation.nextError(next, 'pleaseEnterTheAddress');
	if(typeof this.postCode === 'undefined' || this.postCode.length < 3)
		validation.nextError(next, 'pleaseEnterThePostCode');
	else if(typeof this.city === 'undefined')
		validation.nextError(next, 'pleaseSelectTheCity');
	else if(typeof this.price20 === 'undefined' || this.price20.length < 3)
		validation.nextError(next, 'pleaseEnterThePrice20');
	else if(typeof this.price55 === 'undefined' || this.price55.length < 3)
		validation.nextError(next, 'pleaseEnterThePrice55');
	next();
});
locationsSchema.pre('save', function(next){
	this.updatedAt = new Date();
	if(!this.createdAt)
		this.createdAt = this.updatedAt;
	next();
});
module.exports = mongoose.model('Locations', locationsSchema);

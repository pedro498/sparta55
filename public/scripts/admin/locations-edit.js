$(function(){
	var element = 'locations';
	initialize_(element);
});
function save_(element){
	disabled_();
	var error = '';
	var _id = $('#form #_id').val(),
		name = $('#form #name').val(),
		email = $('#form #email').val(),
		phone = $('#form #phone').val(),
		address = $('#form #address').val(),
		postCode = $('#form #postCode').val(),
		city = $('#form #city option:selected').val(),
		price20 = $('#form #price20').val(),
		price55 = $('#form #price55').val();
	if(name.length < 3){
		$('#name_').addClass('error');
		error += '<li>' + language['pleaseEnterTheName'] + '</li>';
	}
	if(_id.length == 0 && email.length < 5){
		$('#email_').addClass('error');
		error += '<li>' + language['pleaseEnterTheEmail'] + '</li>';
	}
	else if(_id.length == 0 && !validator.isEmail(email)){
		$('#email_').addClass('error');
		error += '<li>' + language['pleaseEnterAValidEmail'] + '</li>';
	}
	if(phone.length < 3){
		$('#phone_').addClass('error');
		error += '<li>' + language['pleaseEnterThePhone'] + '</li>';
	}
	if(address.length < 3){
		$('#address_').addClass('error');
		error += '<li>' + language['pleaseEnterTheAddress'] + '</li>';
	}
	if(postCode.length < 3){
		$('#postCode_').addClass('error');
		error += '<li>' + language['pleaseEnterThePostCode'] + '</li>';
	}
	if(city.length == 0){
		$('#city_').addClass('error');
		error += '<li>' + language['pleaseSelectTheCity'] + '</li>';
	}
	if(price20 <= 0){
		$('#price20_').addClass('error');
		error += '<li>' + language['pleaseEnterThePrice20'] + '</li>';
	}
	if(price55 <= 0){
		$('#price55_').addClass('error');
		error += '<li>' + language['pleaseEnterThePrice55'] + '</li>';
	}
	if(error == ''){
		var data = {
			name: name,
			email: email,
			phone: phone,
			address: address,
			postCode: postCode,
			city: city,
			price20: price20,
			price55: price55
		};
		saveRequest_(element, data);
	}
	else
		error_(error);
}

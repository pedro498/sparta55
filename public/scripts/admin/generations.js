$(function(){
	var element = 'generations';
	var table = $('#table').DataTable({
		'language': datatablesLanguage,
		'autoWidth': false,
		'responsive': false,
		'columnDefs': [{
			'targets': 0,
			'className': 'hidden',
			'searchable': false,
			'orderable': false
		}, {
			'targets': [-1, -2],
			'width': '30px',
			'searchable': false,
			'orderable': false,
			'className': 'center padding'
		}],
		'order': [[5, 'asc'],[6, 'asc'],[2, 'asc'],[1, 'asc']],
		'buttons': ['copy', 'print'],
		'pagingType': datatablesPagingType,
		'drawCallback': datatablesDrawCallback,
		'dom': datatablesDom,
		'initComplete': datatablesInitComplete
	});
	initialize_(table, element);
	$('#filterDateFrom, #filterDateTo').on("change paste keyup", function(){
		table.draw();
	});
});
$.fn.dataTable.ext.search.push(
  function(settings, data, dataIndex){
		var filterDateFrom = $('#filterDateFrom').val();
		var filterDateTo = $('#filterDateTo').val();
		var startDate = data[2];
		if(typeof filterDateFrom !== 'undefined'){
			filterDateFrom = filterDateFrom.split("/");
			filterDateFrom = new Date(+filterDateFrom[2], +filterDateFrom[1] - 1, +filterDateFrom[0]);
		}
		if(typeof filterDateTo !== 'undefined'){
			filterDateTo = filterDateTo.split("/");
			filterDateTo = new Date(+filterDateTo[2], +filterDateTo[1] - 1, +filterDateTo[0]);
		}
		if(typeof startDate !== 'undefined'){
			startDate = startDate.split("/");
			startDate = new Date(+startDate[2], +startDate[1] - 1, +startDate[0]);
		}
    if((isNaN(filterDateFrom) && isNaN(filterDateTo)) ||
      (isNaN(filterDateFrom) && startDate <= filterDateTo) ||
      (filterDateFrom <= startDate && isNaN(filterDateTo)) ||
      (filterDateFrom <= startDate && startDate <= filterDateTo)){
        return true;
    }
    return false;
	}
);

$(function(){
	var element = 'dietary';
	var elementParent = 'users';
	var tableDietary = $('#tableDietary').DataTable({
		'language': datatablesLanguagePartial,
		'autoWidth': false,
		'responsive': false,
		'columnDefs': [{
			'targets': 0,
			'className': 'hidden',
			'searchable': true,
			'orderable': false
		}, {
			'targets': [5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35],
			'className': 'hidden'
		}],
		'order': [[1, 'dsc']],
		'buttons': ['copy', 'print'],
    'pagingType': datatablesPagingType,
		'drawCallback': datatablesDrawCallback,
		'dom': 't<"ui grid"<"ten wide column"Bi><"right floated six wide column"p>>',
		'initComplete': datatablesInitComplete
	});
	$('#tableDietary tbody').on('click', 'tr', function(){
		var tr = $(this);
		var row = tableDietary.row(tr);
		if(row.child.isShown()){
			row.child.hide();
			tr.removeClass('shown');
		}else{
			row.child(formatDietary(row.data())).show();
			tr.addClass('shown');
		}
	});
	$('#showDietary').click(function(){
		$('#formDietary')[0].reset();
		$('.ui.modal#modalDietary').modal('show');
		$('.ui.dropdown').dropdown();
	});
	$('#addDietary').click(function(){
		disabled_();
		var error = '';
		var _id = $('#form #_id').val(),
			mealsPerDay = $('#formDietary #mealsPerDay').val(),
			collations = $('#formDietary #collations').val(),
			waterPerDay = $('#formDietary #waterPerDay').val(),
			outsideMealsPerWeek = $('#formDietary #outsideMealsPerWeek').val(),
			places = $('#formDietary #places').val(),
			foodIntolerance = $('#formDietary #foodIntolerance').val(),
			nonPreferredFoods = $('#formDietary #nonPreferredFoods').val(),
			cravingsInTheDay = $('#formDietary #cravingsInTheDay').val(),
			supplements = $('#formDietary #supplements').val(),
			alcoholConsumption = $('#formDietary #alcoholConsumption').val(),
			tobaccoConsumption = $('#formDietary #tobaccoConsumption').val(),
			coffeeConsumption = $('#formDietary #coffeeConsumption').val(),
			fruits = $('#formDietary #fruits').is(':checked'),
			chicken = $('#formDietary #chicken').is(':checked'),
			soda = $('#formDietary #soda').is(':checked'),
			sausages = $('#formDietary #sausages').is(':checked'),
			candies = $('#formDietary #candies').is(':checked'),
			vegetables = $('#formDietary #vegetables').is(':checked'),
			meat = $('#formDietary #meat').is(':checked'),
			cofee = $('#formDietary #cofee').is(':checked'),
			tortilla = $('#formDietary #tortilla').is(':checked'),
			milk = $('#formDietary #milk').is(':checked'),
			rice = $('#formDietary #rice').is(':checked'),
			seafood = $('#formDietary #seafood').is(':checked'),
			juice = $('#formDietary #juice').is(':checked'),
			cookies = $('#formDietary #cookies').is(':checked'),
			yoghurt = $('#formDietary #yoghurt').is(':checked'),
			greens = $('#formDietary #greens').is(':checked'),
			eggs = $('#formDietary #eggs').is(':checked'),
			tea = $('#formDietary #tea').is(':checked'),
			bread = $('#formDietary #bread').is(':checked'),
			nutritionalDiagnosis = $('#formDietary #nutritionalDiagnosis').val(),
			nutritionalTreatment = $('#formDietary #nutritionalTreatment option:selected').val();
		var data = {
			dietary: {
				mealsPerDay: mealsPerDay,
				collations: collations,
				waterPerDay: waterPerDay,
				outsideMealsPerWeek: outsideMealsPerWeek,
				places: places,
				foodIntolerance: foodIntolerance,
				nonPreferredFoods: nonPreferredFoods,
				cravingsInTheDay: cravingsInTheDay,
				supplements: supplements,
				alcoholConsumption: alcoholConsumption,
				tobaccoConsumption: tobaccoConsumption,
				coffeeConsumption: coffeeConsumption,
				fruits: fruits,
				chicken: chicken,
				soda: soda,
				sausages: sausages,
				candies: candies,
				vegetables: vegetables,
				meat: meat,
				cofee: cofee,
				tortilla: tortilla,
				milk: milk,
				rice: rice,
				seafood: seafood,
				juice: juice,
				cookies: cookies,
				yoghurt: yoghurt,
				greens: greens,
				eggs: eggs,
				tea: tea,
				bread: bread,
				nutritionalDiagnosis: nutritionalDiagnosis,
				nutritionalTreatment: nutritionalTreatment
			}
		}
		for(var key in data.dietary){
			if(data.dietary[key] == '' && typeof(data.dietary[key]) !== 'boolean')
				error = language['pleaseEnterTheInfoRequired'];
		}
		if(error == ''){
			function successCallback(response){
				$('.ui.modal#modalDietary').modal('hide');
				tableDietary.row.add([
					response._id,
					formatDateTime(response.createdAt),
					response.mealsPerDay,
					response.collations,
					response.waterPerDay,
					response.outsideMealsPerWeek,
					response.places,
					response.foodIntolerance,
					response.nonPreferredFoods,
					response.cravingsInTheDay,
					response.supplements,
					response.alcoholConsumption,
					response.tobaccoConsumption,
					response.coffeeConsumption,
					response.fruits,
					response.chicken,
					response.soda,
					response.sausages,
					response.candies,
					response.vegetables,
					response.meat,
					response.cofee,
					response.tortilla,
					response.milk,
					response.rice,
					response.seafood,
					response.juice,
					response.cookies,
					response.yoghurt,
					response.greens,
					response.eggs,
					response.tea,
					response.bread,
					response.nutritionalDiagnosis,
					response.nutritionalTreatment,
					response.createdBy
				]).column(1).order('desc').draw(false);
				$('#formDietary')[0].reset();
			}
			savePartialsRequest_(element, elementParent, data, successCallback);
		}
		else{
			alert_(error);
		}
		return false;
	});
});
function formatDietary(data){
	var lastVisible = 4;
	return '<table class="details">' +
		'<tr>' +
		rowString('outsideMealsPerWeek', data[5]) +
		rowString('places', data[6]) +
		'</tr>' +
		'<tr>' +
		rowString('foodIntolerance', data[7]) +
		rowString('nonPreferredFoods', data[8]) +
		'</tr>' +
		'<tr>' +
		rowString('cravingsInTheDay', data[9]) +
		rowString('supplements', data[10]) +
		'</tr>' +
		'<tr>' +
		rowString('alcoholConsumption', data[11]) +
		rowString('tobaccoConsumption', data[12]) +
		'</tr>' +
		'<tr>' +
		rowString('coffeeConsumption', data[13]) +
		rowEmpty() +
		'</tr>' +
		'<tr>' +
		rowSection('frequentFoods', 'start') +
		'</tr>' +
		'<tr>' +
		rowBoolean('fruits', data[14]) +
		rowBoolean('chicken', data[15]) +
		'</tr>' +
		'<tr>' +
		rowBoolean('soda', data[16]) +
		rowBoolean('sausages', data[17]) +
		'</tr>' +
		'<tr>' +
		rowBoolean('candies', data[18]) +
		rowBoolean('vegetables', data[19]) +
		'</tr>' +
		'<tr>' +
		rowBoolean('meat', data[20]) +
		rowBoolean('cofee', data[21]) +
		'</tr>' +
		'<tr>' +
		rowBoolean('tortilla', data[22]) +
		rowBoolean('milk', data[23]) +
		'</tr>' +
		'<tr>' +
		rowBoolean('rice', data[24]) +
		rowBoolean('seafood', data[25]) +
		'</tr>' +
		'<tr>' +
		rowBoolean('juice', data[26]) +
		rowBoolean('cookies', data[27]) +
		'</tr>' +
		'<tr>' +
		rowBoolean('yoghurt', data[28]) +
		rowBoolean('greens', data[29]) +
		'</tr>' +
		'<tr>' +
		rowBoolean('eggs', data[30]) +
		rowBoolean('tea', data[31]) +
		'</tr>' +
		'<tr>' +
		rowBoolean('bread', data[32]) +
		rowEmpty() +
		'</tr>' +
		'<tr>' +
		rowSection('frequentFoods', 'end') +
		'</tr>' +
		'<tr>' +
		rowString('nutritionalDiagnosis', data[33]) +
		rowDiet('nutritionalTreatment', data[34]) +
		'</tr>' +
		'<tr>' +
		rowString('createdBy', data[35]) +
		rowEmpty() +
		'</tr>' +
		'</table>';
}

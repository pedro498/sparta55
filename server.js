// Modules declaration.
var express = require('express'),
	expressMinify = require('express-minify'),
	bodyParser = require('body-parser'),
	cookieParser = require('cookie-parser'),
	methodOverride = require('method-override'),
	mongoose = require('mongoose'),
	bluebird = require('bluebird'),
	morgan = require('morgan'),
	stylus = require('stylus'),
	configuration = require('./libraries/configuration.js'),
	app = module.exports = express(),
// Database model declarations.
	Users = require('./models/users.js')(app, mongoose),
	States = require('./models/states.js')(app, mongoose),
	Cities = require('./models/cities.js')(app, mongoose),
	Locations = require('./models/locations.js')(app, mongoose),
	Categories = require('./models/categories.js')(app, mongoose),
	Classes = require('./models/classes.js')(app, mongoose),
	Generations = require('./models/generations.js')(app, mongoose);
// App configuration.
app.set('url', configuration.url);
app.set('email', configuration.email);
app.set('facebook', configuration.facebook);
app.set('twitter', configuration.twitter);
app.set('instagram', configuration.instagram);
app.set('secret', configuration.server.secret);
app.set('openPay', configuration.openPay);
app.set('aws', configuration.aws);
app.set('sendGrid', configuration.sendGrid);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
// Middlewares configuration.
app.use(expressMinify());
app.use(methodOverride());
app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(stylus.middleware({
	src: __dirname + '/styles',
	dest: __dirname + '/public/styles'
}));
app.use(express.static(__dirname + '/public'));
// Routers configuration.
app.use('/secure', require('./routes/secure.js'));
app.use('/api', require('./routes/api.js'));
app.use('/admin', require('./routes/admin.js'));
app.use('/', function(req, res){
    res.redirect('/admin');
});
// Database inicialization.
mongoose.Promise = bluebird;
mongoose.connect('mongodb://' + '@' + configuration.database.host + ':' + configuration.database.port + '/' + configuration.database.name, {keepAlive: 300000, connectTimeoutMS: 30000, useMongoClient: true, poolSize: 2}, function(err, res){
	if(err)
		process.exit(1);
	else{
		console.log('MongoDB');
		// App Inicialization.
		app.listen(configuration.server.port, configuration.server.ip, function(err, res){
			if(!err) console.log('NodeJS');
		});
	}
});

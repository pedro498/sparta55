// Modules declarations.
var mongoose = require('mongoose'),
	validation = require('../libraries/validation.js'),
	languages = require('../libraries/languages.js'),
	language = 'es',
	validator = require('validator'),
	roles = ['system', 'admin', 'coach', 'nutrologist', 'receptionist', 'user'],
	trainingPlans = ['t20days', 't55days'],
	paymentMethods = ['cash', 'voucher'],
	paymentQuantities = ['1', '2'],
	sexes = ['male', 'female'],
	status = ['active', 'inactive', 'pending'],
	schema = mongoose.Schema,
	usersSchema = new schema({
		name: {
			type: String,
			min: 3,
			required: true
		},
		lastName: {
			type: String,
			min: 3,
			required: true
		},
		role: {
			type: String,
			enum: roles,
			required: true
		},
		email: {
			type: String,
			min: 5,
			required: true,
			unique: true,
			index: true
		},
		password: {
			type: String,
			min: 8,
			required: true
		},
		avatar: {
			type: String
		},
		assistanceId:{
			type: String,
			unique: true
		},
		birthday: {
			type: Date
		},
		sex: {
			type: String,
			enum: sexes
		},
		phone: {
			type: String
		},
		emergencyPhone: {
			type: String
		},
		address: {
			type: String
		},
		postCode: {
			type: String
		},
		city: {
			type: schema.Types.ObjectId,
			ref: 'Cities',
			required: true,
			index: true
		},
		status: {
			type: String,
			enum: status
		},
		location: {
			type: schema.Types.ObjectId,
			ref: 'Locations'
		},
		contracts: [{
			classe: {
				type: schema.Types.ObjectId,
				ref: 'Classes'
			},
			trianingPlan: {
				type: String,
				enum: trainingPlans
			},
			paymentMethod: {
				type: String,
				enum: paymentMethods
			},
			paymentQuantity: {
				type: String,
				enum: paymentQuantities
			},
			sugestedPrice: {
				type: Number,
				min: 0
			},
			price: {
				type: Number,
				min: 0
			},
			createdBy: {
				type: schema.Types.ObjectId,
				ref: 'Users',
				index: true
			},
			createdAt: {
				type: Date
			}
		}],
		payments: [{
			contract: {
				type: schema.Types.ObjectId,
				ref: 'Contacts'
			},
			paymentMethod: {
				type: String,
				enum: paymentMethods
			},
			price: {
				type: Number,
				min: 0
			},
			paymentDate: {
				type: Date
			},
			createdBy: {
				type: schema.Types.ObjectId,
				ref: 'Users',
				index: true
			},
			createdAt: {
				type: Date
			}
		}],
		clinical: [{
			diagnosis: {
				type: String
			},
			medicines: {
				type: String
			},
			hoursOfSleep: {
				type: String
			},
			obesity: {
				type: Boolean
			},
			diabetes: {
				type: Boolean
			},
			hta: {
				type: Boolean
			},
			cancer: {
				type: Boolean
			},
			hypercholesterolemia: {
				type: Boolean
			},
			createdBy: {
				type: schema.Types.ObjectId,
				ref: 'Users',
				index: true
			},
			createdAt: {
				type: Date
			}
		}],
		dietary: [{
			mealsPerDay: {
				type: String
			},
			collations: {
				type: String
			},
			waterPerDay: {
				type: String
			},
			outsideMealsPerWeek: {
				type: String
			},
			places: {
				type: String
			},
			foodIntolerance: {
				type: String
			},
			nonPreferredFoods: {
				type: String
			},
			cravingsInTheDay: {
				type: String
			},
			supplements: {
				type: String
			},
			alcoholConsumption: {
				type: String
			},
			tobaccoConsumption: {
				type: String
			},
			coffeeConsumption: {
				type: String
			},
			fruits: {
				type: Boolean
			},
			chicken: {
				type: Boolean
			},
			soda: {
				type: Boolean
			},
			sausages: {
				type: Boolean
			},
			candies: {
				type: Boolean
			},
			vegetables: {
				type: Boolean
			},
			meat: {
				type: Boolean
			},
			cofee: {
				type: Boolean
			},
			tortilla: {
				type: Boolean
			},
			milk: {
				type: Boolean
			},
			rice: {
				type: Boolean
			},
			seafood: {
				type: Boolean
			},
			juice: {
				type: Boolean
			},
			cookies: {
				type: Boolean
			},
			yoghurt: {
				type: Boolean
			},
			greens: {
				type: Boolean
			},
			eggs: {
				type: Boolean
			},
			tea: {
				type: Boolean
			},
			bread: {
				type: Boolean
			},
			nutritionalDiagnosis: {
				type: String
			},
			nutritionalTreatment: {
				type: String
			},
			createdBy: {
				type: schema.Types.ObjectId,
				ref: 'Users',
				index: true
			},
			createdAt: {
				type: Date
			}
		}],
		anthropometric: [{
			weight: {
				type: String
			},
			height: {
				type: String
			},
			imc: {
				type: String
			},
			percentBodyFat: {
				type: String
			},
			percentBodyMass: {
				type: String
			},
			percentVisceralFat: {
				type: String
			},
			waist: {
				type: String
			},
			abdomen: {
				type: String
			},
			hip: {
				type: String
			},
			bicep: {
				type: String
			},
			tricep: {
				type: String
			},
			thigh: {
				type: String
			},
			leg: {
				type: String
			},
			subscapular: {
				type: String
			},
			suprailiac: {
				type: String
			},
			supraspinal: {
				type: String
			},
			goal: {
				type: String
			},
			createdBy: {
				type: schema.Types.ObjectId,
				ref: 'Users',
				index: true
			},
			createdAt: {
				type: Date
			}
		}],
		tests: [{
			squats: {
				type: String
			},
			pushUps: {
				type: String
			},
			sitUps: {
				type: String
			},
			burpees: {
				type: String
			},
			joggys: {
				type: String
			},
			longeJump: {
				type: String
			},
			vUps: {
				type: String
			},
			inchworms: {
				type: String
			},
			bottoms: {
				type: String
			},
			trusters: {
				type: String
			},
			wallBall: {
				type: String
			},
			jJacks: {
				type: String
			},
			sideSuicides: {
				type: String
			},
			jumpLenght1: {
				type: String
			},
			jumpLenght2: {
				type: String
			},
			jumpLenght3: {
				type: String
			},
			createdBy: {
				type: schema.Types.ObjectId,
				ref: 'Users',
				index: true
			},
			createdAt: {
				type: Date
			}
		}],
		photos: [{
			front: {
				type: String
			},
			back: {
				type: String
			},
			createdBy: {
				type: schema.Types.ObjectId,
				ref: 'Users',
				index: true
			},
			createdAt: {
				type: Date
			}
		}],
		assistance: [{
			arrival: {
				type: Date
			},
			departure: {
				type: Date
			},
			createdAt: {
				type: Date
			}
		}],
		nss: {
			type: String
		},
		curp: {
			type: String
		},
		rfc: {
			type: String
		},
		admissionDate: {
			Date
		},
		dischargeDate: {
			Date
		},
		ine: {
			type: String
		},
		confidentialityAgreement: {
			type: String
		},
		temporalContract: {
			type: String
		},
		permanentContract: {
			type: String
		},
		studySheet: {
			type: String
		},
		resignationLetter: {
			type: String
		},
		verify: {
			type: schema.Types.ObjectId,
			index: true
		},
		forgot: {
			type: schema.Types.ObjectId,
			index: true
		},
		verified: {
			type: Boolean
		},
		createdAt: {
			type: Date
		},
		updatedAt: {
			type: Date
		}
	});
usersSchema.pre('validate', function(next){
	if(typeof this.name === 'undefined' || this.name.length < 3)
		validation.nextError(next, 'pleaseEnterTheName');
	else if(typeof this.lastName === 'undefined' || this.lastName.length < 3)
		validation.nextError(next, 'pleaseEnterTheLastName');
	else if(typeof this.role === 'undefined')
		validation.nextError(next, 'pleaseSelectTheRole');
	else if(typeof this.email === 'undefined' || this.email.length < 5)
		validation.nextError(next, 'pleaseEnterTheEmail');
	else if(!validator.isEmail(this.email))
		validation.nextError(next, 'pleaseEnterAValidEmail');
	else if(typeof this.password === 'undefined' || this.password.length < 8)
		validation.nextError(next, 'pleaseEnterThePassword');
	else if(!validator.isIn(this.role, roles))
		validation.nextError(next, 'pleaseSelectAValidRole');
	else if(typeof this.city === 'undefined')
		validation.nextError(next, 'pleaseSelectTheCity');
	next();
});
usersSchema.pre('save', function(next){
	this.updatedAt = new Date();
	if(!this.createdAt)
		this.createdAt = this.updatedAt;
	for (var i in this.clinical) {
		if(!i.createdAt)
			i.createdAt = this.updatedAt;
	};
	for (var i in this.dietary) {
		if(!i.createdAt)
			i.createdAt = this.updatedAt;
	};
	for (var i in this.anthropometric) {
		if(!i.createdAt)
			i.createdAt = this.updatedAt;
	};
	for (var i in this.tests) {
		if(!i.createdAt)
			i.createdAt = this.updatedAt;
	};
	for (var i in this.photos) {
		if(!i.createdAt)
			i.createdAt = this.updatedAt;
	};
	for (var i in this.assistance) {
		if(!i.createdAt)
			i.createdAt = this.updatedAt;
	};
	next();
});
module.exports = mongoose.model('Users', usersSchema);

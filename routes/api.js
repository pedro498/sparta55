// Modules declarations.
var express = require('express'),
	multer = require('multer'),
	fs = require('fs'),
	lwip = require('lwip'),
	aws = require('aws-sdk'),
	router = express.Router(),
	app = require('../server.js'),
	languages = require('../libraries/languages.js'),
	language = 'es',
	UsersController = require('../controllers/api/users.js'),
	StatesController = require('../controllers/api/states.js'),
	CitiesController = require('../controllers/api/cities.js'),
	LocationsController = require('../controllers/api/locations.js'),
	CategoriesController = require('../controllers/api/categories.js'),
	ClassesController = require('../controllers/api/classes.js'),
	GenerationsController = require('../controllers/api/generations.js'),
	TokenController = require('../controllers/token/token.js');
aws.config.update({
	accessKeyId: app.settings.aws.accessKeyId,
	secretAccessKey: app.settings.aws.secretAccessKey,
	region: app.settings.aws.region
});
var storageEmployees = multer.diskStorage({
	destination: function(req, file, next){
		next(null, 'public/files/employees/');
	},
	filename: function(req, file, next){
		var fileType = (file.mimetype == 'image/jpeg' ? '.jpg' : '.pdf');
		next(null, req.user._id + Date.now() + fileType);
	}
});
var multerEmployees = multer({
	storage: storageEmployees,
	limits: {
		fileSize: 2048000
	},
	fileFilter: function(req, file, next){
		if(file && (file.mimetype !== 'image/jpeg' && file.mimetype !== 'application/pdf')){
  			req.fileValidationerr = 'Wrong mimetype';
			next(null, false);
		}
		next(null, true);
	}
}).fields([{name: 'avatar', maxCount: 1 }, {name: 'ine', maxCount: 1}, {name: 'confidentialityAgreement', maxCount: 1}, {name: 'temporalContract', maxCount: 1}, {name: 'permanentContract', maxCount: 1}, {name: 'studySheet', maxCount: 1}, {name: 'resignationLetter', maxCount: 1}]);
var multerEmployees_ = function(req, res){
	multerEmployees(req, res, function(err){
		if(err)
    		return res.status(401).jsonp({'message': languages.strings(language, 'pleaseSelectASmallerFile')});
		else if(req.fileValidationerr)
    		return res.status(401).jsonp({'message': languages.strings(language, 'pleaseSelectAPDFFile')});
		else{
			if(req.files){
				if(req.files['avatar']){
	    			lwip.open(req.files['avatar'][0].path, function(err, image){
						var size = image.width();
						if(image.width() > image.height())
							size = image.height();
						image.crop(size, size, function(err, image){
							image.resize(120, 120, function(err, image){
								image.writeFile(req.files['avatar'][0].path, function(err){
									s3 = new aws.S3();
									fs.readFile(req.files['avatar'][0].path, function(err,data){
										if(!err){
											s3.putObject({
												Bucket: req.app.get('aws').bucket,
												Key: 'avatars/' + req.files['avatar'][0].filename,
												Body: data,
												ACL: 'public-read'
											}, function(err, data){
											    fs.unlink(req.files['avatar'][0].path, function(err){});
											});
										}
									});
								});
							});
						});
					});
				}
				if(req.files['ine']){
					s3 = new aws.S3();
					fs.readFile(req.files['ine'][0].path, function(err,data){
						if(!err){
							s3.putObject({
								Bucket: req.app.get('aws').bucket,
								Key: 'ine/' + req.files['ine'][0].filename,
								Body: data,
								ACL: 'public-read'
							}, function(err, data){
							    fs.unlink(req.files['ine'][0].path, function(err){});
							});
						}
					});
				}
				if(req.files['confidentialityAgreement']){
					s3 = new aws.S3();
					fs.readFile(req.files['confidentialityAgreement'][0].path, function(err,data){
						if(!err){
							s3.putObject({
								Bucket: req.app.get('aws').bucket,
								Key: 'confidentialityAgreement/' + req.files['confidentialityAgreement'][0].filename,
								Body: data,
								ACL: 'public-read'
							}, function(err, data){
							    fs.unlink(req.files['confidentialityAgreement'][0].path, function(err){});
							});
						}
					});
				}
				if(req.files['temporalContract']){
					s3 = new aws.S3();
					fs.readFile(req.files['temporalContract'][0].path, function(err,data){
						if(!err){
							s3.putObject({
								Bucket: req.app.get('aws').bucket,
								Key: 'temporalContract/' + req.files['temporalContract'][0].filename,
								Body: data,
								ACL: 'public-read'
							}, function(err, data){
							    fs.unlink(req.files['temporalContract'][0].path, function(err){});
							});
						}
					});
				}
				if(req.files['permanentContract']){
					s3 = new aws.S3();
					fs.readFile(req.files['permanentContract'][0].path, function(err,data){
						if(!err){
							s3.putObject({
								Bucket: req.app.get('aws').bucket,
								Key: 'permanentContract/' + req.files['permanentContract'][0].filename,
								Body: data,
								ACL: 'public-read'
							}, function(err, data){
							    fs.unlink(req.files['permanentContract'][0].path, function(err){});
							});
						}
					});
				}
				if(req.files['studySheet']){
					s3 = new aws.S3();
					fs.readFile(req.files['studySheet'][0].path, function(err,data){
						if(!err){
							s3.putObject({
								Bucket: req.app.get('aws').bucket,
								Key: 'studySheet/' + req.files['studySheet'][0].filename,
								Body: data,
								ACL: 'public-read'
							}, function(err, data){
							    fs.unlink(req.files['studySheet'][0].path, function(err){});
							});
						}
					});
				}
				if(req.files['resignationLetter']){
					s3 = new aws.S3();
					fs.readFile(req.files['resignationLetter'][0].path, function(err,data){
						if(!err){
							s3.putObject({
								Bucket: req.app.get('aws').bucket,
								Key: 'resignationLetter/' + req.files['resignationLetter'][0].filename,
								Body: data,
								ACL: 'public-read'
							}, function(err, data){
							    fs.unlink(req.files['resignationLetter'][0].path, function(err){});
							});
						}
					});
				}
			}
			if(req.method == 'POST')
				UsersController.postEmployee(req, res);
			else if(req.method == 'PUT')
				UsersController.putEmployee(req, res);
		}
	});
}
var storageUsers = multer.diskStorage({
	destination: function(req, file, next){
		next(null, 'public/files/users/');
	},
	filename: function(req, file, next){
		next(null, req.user._id + Date.now() + '.jpg');
	}
});
var multerUsers = multer({
	storage: storageUsers,
	limits: {
		fileSize: 2048000
	},
	fileFilter: function(req, file, next){
		if(file && file.mimetype !== 'image/jpeg'){
  			req.fileValidationerr = 'Wrong mimetype';
			next(null, false);
		}
		next(null, true);
	}
}).single('avatar');
var multerUsers_ = function(req, res){
	multerUsers(req, res, function(err){
		if(err)
    		return res.status(401).jsonp({'message': languages.strings(language, 'pleaseSelectASmallerFile')});
		else if(req.fileValidationerr)
    		return res.status(401).jsonp({'message': languages.strings(language, 'pleaseSelectAJPGFile')});
		else{
			if(req.file){
				lwip.open(req.file.path, function(err, image){
					var size = image.width();
					if(image.width() > image.height())
						size = image.height();
					image.crop(size, size, function(err, image){
						image.resize(120, 120, function(err, image){
							image.writeFile(req.file.path, function(err){
								s3 = new aws.S3();
								fs.readFile(req.file.path, function(err,data){
									if(!err){
										s3.putObject({
											Bucket: req.app.get('aws').bucket,
											Key: 'avatars/' + req.file.filename,
											Body: data,
											ACL: 'public-read'
										}, function(err, data){
										    fs.unlink(req.file.path, function(err){});
										});
									}
								});
							});
						});
					});
				});
			}
			if(req.method == 'POST')
				UsersController.postUser(req, res);
			else if(req.method == 'PUT')
				UsersController.putUser(req, res);
		}
	});
}
var storagePhotos = multer.diskStorage({
	destination: function(req, file, next){
		next(null, 'public/files/photos/');
	},
	filename: function(req, file, next){
		next(null, req.user._id + Date.now() + '.jpg');
	}
});
var multerPhotos = multer({
	storage: storagePhotos,
	limits: {
		fileSize: 2048000
	},
	fileFilter: function(req, file, next){
		if(file && file.mimetype !== 'image/jpeg'){
  			req.fileValidationerr = 'Wrong mimetype';
			next(null, false);
		}
		next(null, true);
	}
}).fields([{name: 'front', maxCount: 1 }, {name: 'back', maxCount: 1}]);
var multerPhotos_ = function(req, res){
	multerPhotos(req, res, function(err){
		if(err)
    		return res.status(401).jsonp({'message': languages.strings(language, 'pleaseSelectASmallerFile')});
		else if(req.fileValidationerr)
      		return res.status(401).jsonp({'message': languages.strings(language, 'pleaseSelectAJPGFile')});
		else{
			if(req.files){
				lwip.open(req.files['front'][0].path, function(err, image){
					var size = image.width();
					if(image.width() > image.height())
						size = image.height();
					image.crop(size, size, function(err, image){
						image.resize(1024, 1024, function(err, image){
							image.writeFile(req.files['front'][0].path, function(err){
								s3 = new aws.S3();
								fs.readFile(req.files['front'][0].path, function(err,data){
									if(!err){
										s3.putObject({
											Bucket: req.app.get('aws').bucket,
											Key: 'photos/' + req.files['front'][0].filename,
											Body: data,
											ACL: 'public-read'
										}, function(err, data){
										    fs.unlink(req.files['front'][0].path, function(err){});
										});
									}
								});
							});
						});
					});
				});
				lwip.open(req.files['back'][0].path, function(err, image){
					var size = image.width();
					if(image.width() > image.height())
						size = image.height();
					image.crop(size, size, function(err, image){
						image.resize(1024, 1024, function(err, image){
							image.writeFile(req.files['back'][0].path, function(err){
								s3 = new aws.S3();
								fs.readFile(req.files['back'][0].path, function(err,data){
									if(!err){
										s3.putObject({
											Bucket: req.app.get('aws').bucket,
											Key: 'photos/' + req.files['back'][0].filename,
											Body: data,
											ACL: 'public-read'
										}, function(err, data){
										    fs.unlink(req.files['back'][0].path, function(err){});
										});
									}
								});
							});
						});
					});
				});
			}
			if(req.method == 'POST')
				UsersController.postUserPhotos(req, res);
		}
	});
}
router.use(TokenController.apiToken);
// Public users setup routes.
router.route('/public/users/setup')
.get(UsersController.getPublicUsersSetup)
// Employees routes.
router.route('/employees')
.get(UsersController.getEmployees)
.post(multerEmployees_)
router.route('/employees/:id')
.get(UsersController.getEmployee)
.put(multerEmployees_)
.delete(UsersController.deleteEmployee)
// Users routes.
router.route('/users')
.get(UsersController.getUsers)
.post(multerUsers_)
router.route('/users/:id')
.get(UsersController.getUser)
.put(multerUsers_)
.delete(UsersController.deleteUser)
router.route('/users/:id/contracts')
.post(UsersController.postUserContracts)
router.route('/users/:id/payments')
.post(UsersController.postUserPayments)
router.route('/users/:id/clinical')
.post(UsersController.postUserClinical)
router.route('/users/:id/dietary')
.post(UsersController.postUserDietary)
router.route('/users/:id/anthropometric')
.post(UsersController.postUserAnthropometric)
router.route('/users/:id/tests')
.post(UsersController.postUserTests)
router.route('/users/:id/photos')
.post(multerPhotos_)
// Public users routes.
router.route('/public/users/login')
.get(UsersController.getPublicUserLogin)
router.route('/public/users/verify')
.put(UsersController.putPublicUsersVerify)
router.route('/public/users/verify/:verify')
.get(UsersController.getPublicUsersVerify)
.put(UsersController.putPublicUsersVerify)
router.route('/public/users/forgot')
.put(UsersController.putPublicUsersForgot)
router.route('/public/users/forgot/:forgot')
.get(UsersController.getPublicUsersForgot)
.put(UsersController.putPublicUsersForgot)
router.route('/public/users/assistance')
.put(UsersController.putPublicUsersAssists)
router.route('/public/users/logout')
.get(UsersController.getPublicUsersLogout)
// States routes.
router.route('/states')
.get(StatesController.getStates)
.post(StatesController.postState)
router.route('/states/:id')
.get(StatesController.getState)
.put(StatesController.putState)
.delete(StatesController.deleteState)
// Public states routes.
router.route('/public/states')
.get(StatesController.getPublicStates)
// Cities routes.
router.route('/cities')
.get(CitiesController.getCities)
.post(CitiesController.postCity)
router.route('/cities/:id')
.get(CitiesController.getCity)
.put(CitiesController.putCity)
.delete(CitiesController.deleteCity)
// Public cities routes.
router.route('/public/cities')
.get(CitiesController.getPublicCities)
// Locations routes.
router.route('/locations')
.get(LocationsController.getLocations)
.post(LocationsController.postLocation)
router.route('/locations/:id')
.get(LocationsController.getLocation)
.put(LocationsController.putLocation)
.delete(LocationsController.deleteLocation)
// Public locations routes.
router.route('/public/locations')
.get(LocationsController.getPublicLocations)
// Categories routes.
router.route('/categories')
.get(CategoriesController.getCategories)
.post(CategoriesController.postCategory)
router.route('/categories/:id')
.get(CategoriesController.getCategory)
.put(CategoriesController.putCategory)
.delete(CategoriesController.deleteCategory)
// Public categories routes.
router.route('/public/categories')
.get(CategoriesController.getPublicCategories)
// Classes routes.
router.route('/classes')
.get(ClassesController.getClasses)
.post(ClassesController.postClasse)
router.route('/classes/:id')
.get(ClassesController.getClasse)
.put(ClassesController.putClasse)
.delete(ClassesController.deleteClasse)
// Public classes routes.
router.route('/public/classes')
.get(ClassesController.getPublicClasses)
// Generations routes.
router.route('/generations')
.get(GenerationsController.getGenerations)
.post(GenerationsController.postGeneration)
router.route('/generations/:id')
.get(GenerationsController.getGeneration)
.put(GenerationsController.putGeneration)
.delete(GenerationsController.deleteGeneration)
// Public generatoins routes.
router.route('/public/generations')
.get(GenerationsController.getPublicGenerations)
module.exports = router;

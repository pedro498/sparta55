$(function(){
	var element = 'classes';
	initialize_(element);
});
function save_(element){
	disabled_();
	var error = '';
	var _id = $('#form #_id').val(),
		startTimeHours = $('#form #startTimeHours option:selected').val(),
		startTimeMinutes = $('#form #startTimeMinutes option:selected').val(),
		startTimeAmpm = $('#form #startTimeAmpm option:selected').val(),
		location = $('#form #location option:selected').val(),
		category = $('#form #category option:selected').val(),
		classRoom = $('#form #classRoom').val();
	if(startTimeHours == ''){
		$('#startTimeHours_').addClass('error');
		error += '<li>' + language['pleaseEnterTheStartTime'] + '</li>';
	}
	if(startTimeMinutes == ''){
		$('#startTimeMinutes_').addClass('error');
		error += '<li>' + language['pleaseEnterTheStartTime'] + '</li>';
	}
	if(startTimeAmpm == ''){
		$('#startTimeAmpm_').addClass('error');
		error += '<li>' + language['pleaseEnterTheStartTime'] + '</li>';
	}
	if(location.length == 0){
		$('#location_').addClass('error');
		error += '<li>' + language['pleaseSelectTheLocation'] + '</li>';
	}
	if(category.length == 0){
		$('#category_').addClass('error');
		error += '<li>' + language['pleaseSelectTheCategory'] + '</li>';
	}
	if(classRoom.length < 3){
		$('#classRoom_').addClass('error');
		error += '<li>' + language['pleaseEnterTheClassRoom'] + '</li>';
	}
	if(error == ''){
		var data = {
			startTime: {
				hours: startTimeHours,
				minutes: startTimeMinutes,
				ampm: startTimeAmpm
			},
			location: location,
			category: category,
			classRoom: classRoom
		};
		saveRequest_(element, data);
	}
	else
		error_(error);
}

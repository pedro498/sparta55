$(function(){
	var element = 'forgot';
	$('#forgot').click(function(){
		forgot_(element);
	});
	$('#form').submit(function(){
		return false;
	});
});
function forgot_(element){
	disabled_();
	var error = '';
	var forgot = $('#form #_forgot').val(),
		email = $('#form #email').val(),
		password = $('#form #password').val();
	if(forgot.length > 0){
		email = undefined;
		if(password.length < 8){
			$('#password_').addClass('error');
			error += '<li>' + language['pleaseEnterYourPassword'] + '</li>';
		}
	}
	else{
		forgot = undefined;
		password = undefined;
		if(email.length < 5){
			$('#email_').addClass('error');
			error += '<li>' + language['pleaseEnterYourEmail'] + '</li>';
		}
		else if(!validator.isEmail(email)){
			$('#email_').addClass('error');
			error += '<li>' + language['pleaseEnterAValidEmail'] + '</li>';
		}
	}
	if(error == ''){
		var data = {
			email: email,
			password: password
		};
		sendRequest_(element, data);
	}
	else
		error_(error);
}

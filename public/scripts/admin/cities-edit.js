$(function(){
	var element = 'cities';
	initialize_(element);
});
function save_(element){
	disabled_();
	var error = '';
	var _id = $('#form #_id').val(),
		name = $('#form #name').val(),
		state = $('#form #state option:selected').val();
	if(name.length < 3){
		$('#name_').addClass('error');
		error += '<li>' + language['pleaseEnterTheName'] + '</li>';
	}
	if(state.length == 0){
		$('#state_').addClass('error');
		error += '<li>' + language['pleaseSelectTheState'] + '</li>';
	}
	if(error == ''){
		var data = {
			name: name,
			state: state
		};
		saveRequest_(element, data);
	}
	else
		error_(error);
}
// Modules declarations.
var mongoose = require('mongoose'),
	validation = require('../libraries/validation.js'),
	languages = require('../libraries/languages.js'),
	language = 'es',
	validator = require('validator'),
	hours = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
	minutes = ['00', '05', '10', '15', '20', '25', '30', '35', '40', '45', '50', '55'],
	ampm = ['am', 'pm']
	schema = mongoose.Schema,
	classesSchema = new schema({
		startTime: {
			hours: {
				type: String,
				enum: hours,
				required: true
			},
			minutes: {
				type: String,
				enum: minutes,
				required: true
			},
			ampm: {
				type: String,
				enum: ampm,
				required: true
			}
		},
		location: {
			type: schema.Types.ObjectId,
			ref: 'Locations',
			required: true,
			index: true
		},
		category: {
			type: schema.Types.ObjectId,
			ref: 'Categories',
			required: true,
			index: true
		},
		classRoom: {
			type: String,
			min: 3,
			required: true
		},
		users: [{
			type: schema.Types.ObjectId,
			ref: 'Users',
			index: true
		}],
		createdAt: {
			type: Date
		},
		updatedAt: {
			type: Date
		}
	});
classesSchema.pre('validate', function(next){
	if(typeof this.startTime === 'undefined')
		validation.nextError(next, 'pleaseEnterTheStartTime');
	else if(!validator.isIn(this.startTime.hours, hours))
		validation.nextError(next, 'pleaseEnterTheStartTime');
	else if(!validator.isIn(this.startTime.minutes, minutes))
		validation.nextError(next, 'pleaseEnterTheStartTime');
	else if(!validator.isIn(this.startTime.ampm, ampm))
		validation.nextError(next, 'pleaseEnterTheStartTime');
	else if(typeof this.classRoom === 'undefined' || this.classRoom.length < 3)
		validation.nextError(next, 'pleaseEnterTheClassRoom');
	else if(typeof this.location === 'undefined')
		validation.nextError(next, 'pleaseSelectTheLocation');
	else if(typeof this.category === 'undefined')
		validation.nextError(next, 'pleaseSelectTheCategory');
	next();
});
classesSchema.pre('save', function(next){
	this.updatedAt = new Date();
	if(!this.createdAt)
		this.createdAt = this.updatedAt;
	next();
});
module.exports = mongoose.model('Classes', classesSchema);

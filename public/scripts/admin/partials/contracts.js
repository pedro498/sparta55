$(function(){
	var element = 'contracts';
	var elementParent = 'users';
	var tableContracts = $('#tableContracts').DataTable({
		'language': datatablesLanguagePartial,
		'autoWidth': false,
		'responsive': false,
		'columnDefs': [{
			'targets': 0,
			'className': 'hidden',
			'searchable': true,
			'orderable': false
		}, {
			'targets': [6,7,8,9],
			'className': 'hidden'
		}],
		'order': [[1, 'dsc']],
		'buttons': ['copy', 'print'],
    'pagingType': datatablesPagingType,
		'drawCallback': datatablesDrawCallback,
		'dom': 't<"ui grid"<"ten wide column"Bi><"right floated six wide column"p>>',
		'initComplete': datatablesInitComplete
	});
	$('#tableContracts tbody').on('click', 'tr', function(){
		var tr = $(this);
		var row = tableContracts.row(tr);
		if(row.child.isShown()){
			row.child.hide();
			tr.removeClass('shown');
		}else{
			row.child(formatContracts(row.data())).show();
			tr.addClass('shown');
		}
	});
	$('#showContracts').click(function(){
		$('#t20days_').hide();
		$('#t55days_').hide();
		$('#formContracts')[0].reset();
		$('.ui.modal#modalContracts').modal('show');
	});
	$('#trainingPlan').change(function(){
		var trainingPlan = $('#trainingPlan option:selected').val();
		$('#sugestedPrice').val($('#' + trainingPlan).val());
	});
	$('#addContracts').click(function(){
		disabled_();
		var error = '';
		var _id = $('#form #_id').val(),
			classe = $('#formContracts #classe option:selected').val(),
			trainingPlan = $('#formContracts #trainingPlan option:selected').val(),
			sugestedPrice = $('#formContracts #sugestedPrice').val(),
			price = $('#formContracts #price').val(),
			startDate = $('#formContracts #startDate').val(),
			paymentMethod = $('#formContracts #paymentMethod option:selected').val(),
			paymentQuantity = $('#formContracts #paymentQuantity option:selected').val();
		var data = {
			contracts: {
				classe: classe,
				trainingPlan: trainingPlan,
				sugestedPrice: sugestedPrice,
				price: price,
				startDate: startDate,
				paymentMethod: paymentMethod,
				paymentQuantity: paymentQuantity
			}
		}
		console.log(data);
		return false;
		for(var key in data.contracts){
			if(data.contracts[key] == '' && typeof(data.contracts[key]) !== 'boolean')
				error = language['pleaseEnterTheInfoRequired'];
		}
		if(error == ''){
			function successCallback(response){
				$('.ui.modal#modalContracts').modal('hide');
				tableContracts.row.add([
					response._id,
					formatDateTime(response.createdAt),
					response.location,
					response.category,
					response.classe,
					response.generation,
					response.paymentMethod,
					response.paymentQuantity,
					response.price,
					response.createdBy
				]).column(1).order('desc').draw(false);
				$('#formContracts')[0].reset();
			}
			savePartialsRequest_(element, elementParent, data, successCallback);
		}
		else{
			alert_(error);
		}
		return false;
	});
});
function formatContracts(data){
	return '<table class="details">' +
		'<tr>' +
		rowString('generation', data[5]) +
		rowString('paymentMethod', data[6]) +
		'</tr>' +
		'<tr>' +
		rowString('paymentQuantity', data[7]) +
		rowString('createdBy', data[9]) +
		'</tr>' +
		'</table>';
}

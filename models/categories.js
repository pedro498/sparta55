// Modules declarations.
var mongoose = require('mongoose'),
	validation = require('../libraries/validation.js'),
	languages = require('../libraries/languages.js'),
	language = 'es',
	schema = mongoose.Schema,
	categoriesSchema = new schema({
		name: {
			type: String,
			min: 3,
			required: true,
			unique: true,
			index: true
		},
		classes: [{
			type: schema.Types.ObjectId,
			ref: 'Classes',
			index: true
		}],
		createdAt: {
			type: Date
		},
		updatedAt: {
			type: Date
		}
	});
categoriesSchema.pre('validate', function(next){
	if(typeof this.name === 'undefined' || this.name.length < 3)
		validation.nextError(next, 'pleaseEnterTheName');
	next();
});
categoriesSchema.pre('save', function(next){
	this.updatedAt = new Date();
	if(!this.createdAt)
		this.createdAt = this.updatedAt;
	next();
});
module.exports = mongoose.model('Categories', categoriesSchema);

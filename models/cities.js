// Modules declarations.
var mongoose = require('mongoose'),
	validation = require('../libraries/validation.js'),
	languages = require('../libraries/languages.js'),
	language = 'es',
	schema = mongoose.Schema,
	citiesSchema = new schema({
		name: {
			type: String,
			min: 3,
			required: true,
			unique: true,
			index: true
		},
		state: {
			type: schema.Types.ObjectId,
			ref: 'States',
			required: true,
			index: true
		},
		users: [{
			type: schema.Types.ObjectId,
			ref: 'Users',
			index: true
		}],
		locations: [{
			type: schema.Types.ObjectId,
			ref: 'Locations',
			index: true
		}],
		createdAt: {
			type: Date
		},
		updatedAt: {
			type: Date
		}
	});
citiesSchema.pre('validate', function(next){
	if(typeof this.name === 'undefined' || this.name.length < 3)
		validation.nextError(next, 'pleaseEnterTheName');
	else if(typeof this.state === 'undefined')
		validation.nextError(next, 'pleaseSelectTheState');
	next();
});
citiesSchema.pre('save', function(next){
	this.updatedAt = new Date();
	if(!this.createdAt)
		this.createdAt = this.updatedAt;
	next();
});
module.exports = mongoose.model('Cities', citiesSchema);
$(function(){
	var element = 'payments';
	var elementParent = 'users';
	var tablePayments = $('#tablePayments').DataTable({
		'language': datatablesLanguagePartial,
		'autoWidth': false,
		'responsive': false,
		'columnDefs': [{
			'targets': 0,
			'className': 'hidden',
			'searchable': true,
			'orderable': false
		}, {
			'targets': [3, 7],
			'className': 'hidden'
		}],
		'order': [[1, 'dsc']],
		'buttons': ['copy', 'print'],
    'pagingType': datatablesPagingType,
		'drawCallback': datatablesDrawCallback,
		'dom': 't<"ui grid"<"ten wide column"Bi><"right floated six wide column"p>>',
		'initComplete': datatablesInitComplete
	});
	$('#tablePayments tbody').on('click', 'tr', function(){
		var tr = $(this);
		var row = tablePayments.row(tr);
		if(row.child.isShown()){
			row.child.hide();
			tr.removeClass('shown');
		}else{
			row.child(formatPayments(row.data())).show();
			tr.addClass('shown');
		}
	});
	$('#showPayments').click(function(){
		$('#formPayments')[0].reset();
		$('.ui.modal#modalPayments').modal('show');
	});
	$('#addPayments').click(function(){
		disabled_();
		var error = '';
		var _id = $('#form #_id').val(),
			diagnosis = $('#formPayments #diagnosis').val(),
			medicines = $('#formPayments #medicines').val(),
			hoursOfSleep = $('#formPayments #hoursOfSleep').val(),
			obesity = $('#formPayments #obesity').is(':checked'),
			diabetes = $('#formPayments #diabetes').is(':checked'),
			hta = $('#formPayments #hta').is(':checked'),
			cancer = $('#formPayments #cancer').is(':checked'),
			hypercholesterolemia = $('#formPayments #hypercholesterolemia').is(':checked');
		var data = {
			payments: {
				diagnosis: diagnosis,
				medicines: medicines,
				hoursOfSleep: hoursOfSleep,
				obesity: obesity,
				diabetes: diabetes,
				hta: hta,
				cancer: cancer,
				hypercholesterolemia: hypercholesterolemia
			}
		}
		for(var key in data.payments){
			if(data.payments[key] == '' && typeof(data.payments[key]) !== 'boolean')
				error = language['pleaseEnterTheInfoRequired'];
		}
		if(error == ''){
			function successCallback(response){
				$('.ui.modal#modalPayments').modal('hide');
				tablePayments.row.add([
					response._id,
					formatDateTime(response.createdAt),
					response.diagnosis,
					response.medicines,
					response.hoursOfSleep,
					response.obesity,
					response.diabetes,
					response.hta,
					response.cancer,
					response.hypercholesterolemia,
					response.createdBy
				]).column(1).order('desc').draw(false);
				$('#formPayments')[0].reset();
			}
			savePartialsRequest_(element, elementParent, data, successCallback);
		}
		else{
			alert_(error);
		}
		return false;
	});
});
function formatPayments(data){
	return '<table class="details">' +
		'<tr>' +
		rowString('generation', data[5]) +
		rowString('paymentMethod', data[6]) +
		'</tr>' +
		'<tr>' +
		rowString('paymentQuantity', data[7]) +
		rowString('price', data[8]) +
		'</tr>' +
		'<tr>' +
		rowString('createdBy', data[9]) +
		rowEmpty() +
		'</tr>' +
		'</table>';
}

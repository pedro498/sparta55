// Modules declarations.
var express = require('express'),
	router = express.Router(),
	SecureController = require('../controllers/secure/secure.js'),
	TokenController = require('../controllers/token/token.js');
router.use(TokenController.secureToken);
// Secure routes.
router.route('/login')
.get(SecureController.getLogin)
router.route('/verify')
.get(SecureController.getVerify)
router.route('/verify/:verify')
.get(SecureController.getVerify)
router.route('/forgot')
.get(SecureController.getForgot)
router.route('/forgot/:forgot')
.get(SecureController.getForgot)
router.route('/assistance')
.get(SecureController.getAssists);
module.exports = router;

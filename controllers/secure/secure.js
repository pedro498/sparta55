// Modules declarations.
var restler = require('restler'),
	languages = require('../../libraries/languages.js'),
	language = 'es';
// Secure controllers.
module.exports.getLogin =  function(req, res){
	var path = 'login';
	res.render('secure/' + path, {
		title: languages.strings(language, path),
		description: languages.strings(language, 'loginToUseOurServices'),
		languages: languages,
		language: language,
		path: path
	});
};
module.exports.getVerify =  function(req, res){
	var path = 'verify',
		pathUsers = 'users';
	if(req.params.verify){
		if(req.params.verify.length == 24){
			restler.get(req.protocol + '://' + req.hostname + '/api/public/' + pathUsers + '/' + path + '/' + req.params.verify).on('complete', function(user){
				if(user.verify){
					res.render('secure/' + path, {
						title: languages.strings(language, path),
						description: languages.strings(language, 'enterAPasswordToVerifyYourAccount'),
						languages: languages,
						language: language,
						path: path,
						user: user
					});
				}
				else
					res.redirect('/secure/verify');
			});
		}
		else
			res.redirect('/secure/verify');
	}
	else{
		res.render('secure/' + path, {
			title: languages.strings(language, path),
			description: languages.strings(language, 'enterYourEmailToVerifyYourAccount'),
			languages: languages,
			language: language,
			path: path
		});
	}
};
module.exports.getForgot =  function(req, res){
	var path = 'forgot',
		pathUsers = 'users';
	if(req.params.forgot){
		if(req.params.forgot.length == 24){
			restler.get(req.protocol + '://' + req.hostname + '/api/public/' + pathUsers + '/' + path + '/' + req.params.forgot).on('complete', function(user){
				if(user.forgot){
					res.render('secure/' + path, {
						title: languages.strings(language, path),
						description: languages.strings(language, 'enterYourNewPassword'),
						languages: languages,
						language: language,
						path: path,
						user: user
					});
				}
				else
					res.redirect('/secure/forgot');
			});
		}
		else
			res.redirect('/secure/forgot');
	}
	else{
		res.render('secure/' + path, {
			title: languages.strings(language, path),
			description: languages.strings(language, 'enterYourEmailToResetYourPassword'),
			languages: languages,
			language: language,
			path: path
		});
	}
};
module.exports.getAssists =  function(req, res){
	var path = 'assistance',
		pathUsers = 'users';
	res.render('secure/' + path, {
		title: languages.strings(language, path),
		description: languages.strings(language, 'enterYourIDToRegisterYourArrivalOrDeparture'),
		languages: languages,
		language: language,
		path: path
	});
};

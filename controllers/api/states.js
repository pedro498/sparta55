// Modules declarations.
var mongoose = require('mongoose'),
	validation = require('../../libraries/validation.js'),
	languages = require('../../libraries/languages.js'),
	language = 'es',
	capitalize = require('capitalize'),
	States = mongoose.model('States');
// States controllers.
function saveState(state, res){
	state.save(function(err, state){
		if(validation.save(err, state, res, 'state'))
			res.status(200).jsonp(state);
	});
}
function removeState(state, res){
	state.remove(function(err){
		validation.remove(err, state, res, 'state');
	});
}
module.exports.getStates = function(req, res){
	States.find(function(err, states){
		if(validation.find(err, states, res, 'states'))
			res.status(200).jsonp(states);
	}).select('name cities').sort('name').lean();
};
module.exports.postState = function(req, res){
	var state = new States();
	if(typeof req.body.name !== 'undefined')
		state.name = capitalize.words(req.body.name);
	saveState(state, res);
};
module.exports.getState = function(req, res){
	States.findById(req.params.id, function(err, state){
		if(validation.find(err, state, res, 'state'))
			res.status(200).jsonp(state);
	}).select('name cities').populate([{
		path: 'cities',
		select: 'name',
		options: {
			sort: {
				'name': 1
			}
		}
	}]).lean();
};
module.exports.putState = function(req, res){
	States.findById(req.params.id, function(err, state){
		if(validation.find(err, state, res, 'state')){
			if(typeof req.body.name !== 'undefined')
				state.name = capitalize.words(req.body.name);
			saveState(state, res);
		}
	});
};
module.exports.deleteState = function(req, res){
	States.findById(req.params.id, function(err, state){
		if(validation.find(err, state, res, 'state')){
			if(state.cities.length > 0)
				validation.notRemove(err, state, res, 'state');
			else
				removeState(state, res);
		}
	});
};
// Public states controllers.
module.exports.getPublicStates = function(req, res){
	States.find(function(err, states){
		if(validation.find(err, states, res, 'states'))
			res.status(200).jsonp(states);
	}).select('name').sort('name');
};
$(function(){
	var element = 'classes';
	var table = $('#table').DataTable({
		'language': datatablesLanguage,
		'autoWidth': false,
		'responsive': false,
		'columnDefs': [{
			'targets': 0,
			'className': 'hidden',
			'searchable': false,
			'orderable': false
		}, {
			'targets': [-1, -2],
			'width': '30px',
			'searchable': false,
			'orderable': false,
			'className': 'center padding'
		}],
		'order': [[3, 'asc'],[4, 'asc'],[5, 'asc'],[2, 'asc'],[1, 'asc']],
		'buttons': ['copy', 'print'],
		'pagingType': datatablesPagingType,
		'drawCallback': datatablesDrawCallback,
		'dom': datatablesDom,
		'initComplete': datatablesInitComplete
	});
	initialize_(table, element);
});

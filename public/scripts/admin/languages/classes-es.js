var element = 'clase';
var elements = 'clases';
var language = {
	theRecordWasDeleted: 'La ' + element + ' fue eliminada',
	theRecordWasSaved: 'La ' + element + ' fue guardada',
	thereWasAnErrorDeletingThisRecord: 'Ha ocurrido un error al eliminar esta ' + element
}
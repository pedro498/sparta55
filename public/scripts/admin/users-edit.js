$(function(){
	var element = 'users';
	initialize_(element);
	$('#newPassword').click(function(){
		$('#newPassword').hide();
		$('#password').show();
		return false;
	});
	$('#avatarFile').click(function(){
		$('#avatar').click();
		return false;
	});
	$("#avatar").change(function(){
		var file = this.files[0],
		fileExtention = file.name.split('.').pop().toLowerCase(),
		fileSize = file.size;
		if(fileSize > 2048000 || fileExtention != 'jpg'){
			$('#form #avatar').val('');
			$('#avatarFile span').html(language['avatar']);
			if(fileSize > 2048000)
				alert(language['pleaseSelectASmallerFile']);
			else if(fileExtention != 'jpg')
				alert(language['pleaseSelectAJPGFile']);
		}
		else{
			avatar = $('#form #avatar').val();
			$('#avatarFile span').html(avatar.match(/[^\/\\]+$/));
		}
	});
	if($('#form #_id').val() != ''){
		$("#barcode").JsBarcode($('#form #_id').val());
		$('#barcodeFile').click(function(){
			w = window.open(document.getElementById('barcode').src);
			return false;
		});
	}
});
function save_(element){
	disabled_();
	var error = '';
	var _id = $('#form #_id').val(),
		name = $('#form #name').val(),
		lastName = $('#form #lastName').val(),
		role = $('#form #role option:selected').val(),
		email = $('#form #email').val(),
		password = $('#form #password').val(),
		birthday = $('#form #birthday').val(),
		sex = $('#form #sex').val(),
		phone = $('#form #phone').val(),
		emergencyPhone = $('#form #emergencyPhone').val(),
		address = $('#form #address').val(),
		postCode = $('#form #postCode').val(),
		city = $('#form #city option:selected').val(),
		status = $('#form #status option:selected').val(),
		location = $('#form #location option:selected').val(),
		avatar = $('#form #avatar').val();
	if(name.length < 3){
		$('#name_').addClass('error');
		error += '<li>' + language['pleaseEnterTheName'] + '</li>';
	}
	if(lastName.length < 3){
		$('#lastName_').addClass('error');
		error += '<li>' + language['pleaseEnterTheLastName'] + '</li>';
	}
	if(role.length == 0){
		$('#role_').addClass('error');
		error += '<li>' + language['pleaseSelectTheRole'] + '</li>';
	}
	if(_id.length == 0 && email.length < 5){
		$('#email_').addClass('error');
		error += '<li>' + language['pleaseEnterTheEmail'] + '</li>';
	}
	else if(_id.length == 0 && !validator.isEmail(email)){
		$('#email_').addClass('error');
		error += '<li>' + language['pleaseEnterAValidEmail'] + '</li>';
	}
	if(_id.length > 0 && password.length > 0 && password.length < 8){
		$('#password_').addClass('error');
		error += '<li>' + language['pleaseEnterThePassword'] + '</li>';
	}
	if(city.length == 0){
		$('#city_').addClass('error');
		error += '<li>' + language['pleaseSelectTheCity'] + '</li>';
	}
	if(error == ''){
		if(_id.length > 0)
			email = undefined;
		if(_id.length > 0 && password.length == 0)
			password = undefined;
		if(status.length == 0)
			status = undefined;
		if(location.length == 0)
			location = undefined;
		var data = {
			name: name,
			lastName: lastName,
			role: role,
			email: email,
			password: password,
			birthday: birthday,
			sex: sex,
			phone: phone,
			emergencyPhone: emergencyPhone,
			address: address,
			postCode: postCode,
			city: city,
			status: status,
			location: location
		};
		var dataFile = new FormData();
		if(avatar && avatar.length > 0){
			dataFile.append('avatar', $('#avatar')[0].files[0]);
			for(var key in data){
				if(data[key] != undefined)
					dataFile.append(key, data[key]);
			}
			saveRequest_(element, dataFile, true);
		}
		else
			saveRequest_(element, data);
	}
	else
		error_(error);
}

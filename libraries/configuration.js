// General configuration.
module.exports.email = 'sparta55mx@outlook.com';
module.exports.facebook = 'http://www.facebook.com/Sparta55';
module.exports.twitter = 'http://www.twitter.com/Sparta55';
module.exports.instagram = 'http://www.instagram.com/Sparta55';
// Server configuration.
module.exports.server = {
	ip: process.env.IP || '127.0.0.1',
	port: process.env.PORT || '80',
	secret: 'Sparta55Sp1.2.3.4.5.$'
}
// URL configuration.
if(process.env.NODE_ENV){
	if(process.env.NODE_ENV == 'production'){
		module.exports.url = '';
	}
	else if(process.env.NODE_ENV == 'testing'){
		module.exports.url = 'http://nodejs-mongo-persistent-sparta55.a3c1.starter-us-west-1.openshiftapps.com';
	}
}
else{
	module.exports.url = 'http://localhost';
}
// Database configuration.
if(process.env.NODE_ENV){
	if(process.env.NODE_ENV == 'production'){
		module.exports.database = {
			user: '',
			password: '',
			host: '',
			port: '',
			name: ''
		}
	}
	else if(process.env.NODE_ENV == 'testing'){
		module.exports.database = {
			user: 'Sparta55',
			password: 'Sp1.2.3.4.5.$',
			host: 'localhost',
			port: '27017',
			name: 'Sparta55'
		}
	}
}
else{
	module.exports.database = {
		user: 'Sparta55',
		password: 'Sp1.2.3.4.5.$',
		host: 'localhost',
		port: '27017',
		name: 'Sparta55'
	}
}
// Openpay configuration.
if(process.env.NODE_ENV){
	if(process.env.NODE_ENV == 'production'){
		module.exports.openPay = {
			id: '',
			apiKey: '',
			publicKey: '',
			sandbox: false,
			productionReady: true
		}
	}
	else if(process.env.NODE_ENV == 'testing'){
		module.exports.openPay = {
			id: '',
			apiKey: '',
			publicKey: '',
			sandbox: true,
			productionReady: false
		}
	}
}
else{
	module.exports.openPay = {
		id: '',
		apiKey: '',
		publicKey: '',
		sandbox: true,
		productionReady: false
	}
}
// AWS configuration.
if(process.env.NODE_ENV){
	if(process.env.NODE_ENV == 'production'){
		module.exports.aws = {
			accessKeyId: '',
			secretAccessKey: '',
			region: 'us-west-2',
			bucket: '',
			url: 'https://s3-us-west-2.amazonaws.com'
		}
	}
	else if(process.env.NODE_ENV == 'testing'){
		module.exports.aws = {
			accessKeyId: 'AKIAI36T3LNO3VBOPP3Q',
			secretAccessKey: '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',
			region: 'us-west-2',
			bucket: 'sparta55testing',
			url: 'https://s3-us-west-2.amazonaws.com'
		}
	}
}
else{
	module.exports.aws = {
		accessKeyId: 'AKIAI36T3LNO3VBOPP3Q',
		secretAccessKey: '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',
		region: 'us-west-2',
		bucket: 'sparta55testing',
		url: 'https://s3-us-west-2.amazonaws.com'
	}
}
// SendGrid configuration.
module.exports.sendGrid = {
	auth: {
		api_key: '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'
	}
}

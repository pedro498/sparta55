function success_(message, url){
	$('#message .content').html(message);
	$('#message').addClass('success');
	$('#message').transition('fade in', 1000);
	enabled_();
	if(url !== undefined && url != ''){
		setTimeout(function(){
			location_(url);
		}, 500);
	}
}
function error_(message, url){
	$('#message .content').html(message);
	$('#message').addClass('error');
	$('#message').transition('fade in', 1000);
	enabled_();
	if(url !== undefined && url != ''){
		setTimeout(function(){
			location_(url);
		}, 500);
	}
}
function disabled_(){
	if($('#message').css('display') != 'none'){
		$('#message').transition('fade out', 0);
		$('#message').removeClass('success');
		$('#message').removeClass('error');
		$('#message .content').html('');
	}
	$('.field').removeClass('error');
	$('#form').addClass('loading');
}
function enabled_(){
	$('#form').removeClass('loading');
}
function reset_(element){
	$('#message').transition('fade out', 0);
	$('#message').removeClass('success');
	$('#message').removeClass('error');
	$('#message .content').html('');
	$('#form')[0].reset();
	$('#form #' + element).focus();
}
function location_(url){
	$(location).attr('href', url);
}
function sendRequest_(element, data){
	var url = '../../../api/public/users/' + element,
		type = '',
		verify = $('#_verify').val(),
		forgot = $('#_forgot').val();
	if(element == 'login')
		type = 'GET';
	else if(element == 'verify' || element == 'forgot' || element == 'assistance'){
		type = 'PUT';
		if(verify && verify.length > 0)
			url = url + '/' + verify;
		else if(forgot && forgot.length > 0)
			url = url + '/' + forgot;
	}
	$.ajax({
		url: url,
		dataType: 'json',
		type: type,
		cache: false,
		timeout: 5000,
		data: data,
		processData: true,
		contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
		success: function(response){
			if(element != 'assistance')
				success_('<li>' + response.message + '</li>', '/admin/users/profile');
			else{
				success_('<li>' + response.message + '</li>');
				setTimeout(function(){
					reset_(element);
				}, 2000);
			}
		},
		error: function(error){
			var message = language['thereWasAnErrorPleaseTryLater'];
			if(error.responseJSON != undefined)
				message = error.responseJSON.message;
			error_('<li>' + message + '</li>');
			if(element == 'assistance'){
				setTimeout(function(){
					reset_(element);
				}, 2000);
			}
		}
	});
}

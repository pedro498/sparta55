// Modules declarations.
var restler = require('restler'),
	languages = require('../../libraries/languages.js'),
	language = 'es',
	roles = [
		{key: 'system', val: languages.strings(language, 'system')},
		{key: 'admin', val: languages.strings(language, 'admin')},
		{key: 'coach', val: languages.strings(language, 'coach')},
		{key: 'nutrologist', val: languages.strings(language, 'nutrologist')},
		{key: 'receptionist', val: languages.strings(language, 'receptionist')},
		{key: 'user', val: languages.strings(language, 'user')}
	],
	sexes = [
		{key: 'male', val: languages.strings(language, 'male')},
		{key: 'female', val: languages.strings(language, 'female')}
	],
	status = [
		{key: 'active', val: languages.strings(language, 'active')},
		{key: 'inactive', val: languages.strings(language, 'inactive')},
		{key: 'pending', val: languages.strings(language, 'pending')}
	],
	hours = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
	minutes = ['00', '05', '10', '15', '20', '25', '30', '35', '40', '45', '50', '55'],
	ampm = ['am', 'pm'],
	diets = [
		{key: '1800K40H30P30L', val: '1800K40H30P30L'},
		{key: '1800K45H25P30L', val: '1800K45H25P30L'}
	],
	trainingPlans = [
		{key: 't20days', val: languages.strings(language, 't20days')},
		{key: 't55days', val: languages.strings(language, 't55days')}
	],
	paymentMethods = [
		{key: 'cash', val: languages.strings(language, 'cash')},
		{key: 'voucher', val: languages.strings(language, 'voucher')}
	],
	paymentQuantities = [
		{key: '1', val: languages.strings(language, '1')},
		{key: '2', val: languages.strings(language, '2')}
	]
// Employees controllers.
module.exports.getEmployees =  function(req, res){
	var path = 'employees';
	restler.get(req.protocol + '://' + req.get('host') + '/api/' + path + '/' + '?userToken=' + req.cookies.userToken).on('complete', function(data){
		res.render('admin/' + path, {
			title: languages.strings(language, path),
			user: req.user,
			languages: languages,
			language: language,
			bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
			path: path,
			data: data,
			roles: roles,
			sexes: sexes,
			status: status
		});
	});
};
module.exports.getEmployeesEdit =  function(req, res){
	var path = 'employees-edit',
		pathParent = 'employees',
		pathCities = 'cities',
		pathLocations = 'locations';
	restler.get(req.protocol + '://' + req.get('host') + '/api/public/' + pathCities).on('complete', function(cities){
		restler.get(req.protocol + '://' + req.get('host') + '/api/public/' + pathLocations).on('complete', function(locations){
			if(req.params.id){
				restler.get(req.protocol + '://' + req.get('host') + '/api/' + pathParent + '/' + req.params.id + '?userToken=' + req.cookies.userToken).on('complete', function(data){
					if(data && data._id){
						res.render('admin/' + path, {
							title: languages.strings(language, 'edit'),
							user: req.user,
							languages: languages,
							language: language,
							bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
							path: path,
							pathParent: pathParent,
							data: data,
							roles: roles,
							sexes: sexes,
							cities: cities,
							status: status,
							locations: locations
						});
					}
					else
						res.redirect('/admin/' + pathParent + '/edit');
				});
			}
			else{
				res.render('admin/' + path, {
					title: languages.strings(language, 'add'),
					user: req.user,
					languages: languages,
					language: language,
					bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
					path: path,
					pathParent: pathParent,
					roles: roles,
					sexes: sexes,
					cities: cities,
					status: status,
					locations: locations
				});
			}
		});
	});
};
module.exports.getEmployeesProfile =  function(req, res){
	var path = 'employees-edit',
		pathParent = 'employees',
		pathCities = 'cities',
		pathLocations = 'locations';
	if(req.user._id){
		restler.get(req.protocol + '://' + req.get('host') + '/api/public/' + pathCities).on('complete', function(cities){
			restler.get(req.protocol + '://' + req.get('host') + '/api/public/' + pathLocations).on('complete', function(locations){
				restler.get(req.protocol + '://' + req.get('host') + '/api/' + pathParent + '/' + req.user._id + '?userToken=' + req.cookies.userToken).on('complete', function(data){
					if(data && data._id){
						res.render('admin/' + path, {
							title: languages.strings(language, 'profile'),
							description: languages.strings(language, 'profileDescriptionEdit'),
							user: req.user,
							languages: languages,
							language: language,
							bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
							path: path,
							pathParent: pathParent,
							data: data,
							roles: roles,
							sexes: sexes,
							cities: cities,
							status: status,
							locations: locations
						});
					}
					else
						res.redirect('/admin/' + pathParent + '/edit');
				});
			});
		});
	}
	else{
		res.redirect('/admin/' + pathParent + '/edit');
	}
};
// Users controllers.
module.exports.getUsers =  function(req, res){
	var path = 'users';
	restler.get(req.protocol + '://' + req.get('host') + '/api/' + path + '/' + '?userToken=' + req.cookies.userToken).on('complete', function(data){
		res.render('admin/' + path, {
			title: languages.strings(language, path),
			user: req.user,
			languages: languages,
			language: language,
			bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
			path: path,
			data: data,
			roles: roles,
			sexes: sexes
		});
	});
};
module.exports.getUsersEdit =  function(req, res){
	var path = 'users-edit',
		pathParent = 'users',
		pathCities = 'cities',
		pathLocations = 'locations',
		pathCategories = 'categories',
		pathClasses = 'classes',
		pathGenerations = 'generations';
	restler.get(req.protocol + '://' + req.get('host') + '/api/public/' + pathCities).on('complete', function(cities){
		restler.get(req.protocol + '://' + req.get('host') + '/api/public/' + pathLocations).on('complete', function(locations){
			if(req.params.id){
				restler.get(req.protocol + '://' + req.get('host') + '/api/' + pathParent + '/' + req.params.id + '?userToken=' + req.cookies.userToken).on('complete', function(data){
					if(data && data._id){
						restler.get(req.protocol + '://' + req.get('host') + '/api/public/' + pathCategories).on('complete', function(categories){
							restler.get(req.protocol + '://' + req.get('host') + '/api/public/' + pathClasses).on('complete', function(classes){
								restler.get(req.protocol + '://' + req.get('host') + '/api/public/' + pathGenerations).on('complete', function(generations){
									res.render('admin/' + path, {
										title: languages.strings(language, 'edit'),
										user: req.user,
										languages: languages,
										language: language,
										bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
										path: path,
										pathParent: pathParent,
										data: data,
										roles: roles,
										sexes: sexes,
										cities: cities,
										status: status,
										locations: locations,
										categories: categories,
										classes: classes,
										generations: generations,
										diets: diets,
										trainingPlans: trainingPlans,
										paymentMethods: paymentMethods,
										paymentQuantities: paymentQuantities
									});
								});
							});
						});
					}
					else
						res.redirect('/admin/' + pathParent + '/edit');
				});
			}
			else{
				res.render('admin/' + path, {
					title: languages.strings(language, 'add'),
					user: req.user,
					languages: languages,
					language: language,
					bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
					path: path,
					pathParent: pathParent,
					roles: roles,
					sexes: sexes,
					cities: cities,
					status: status,
					locations: locations
				});
			}
		});
	});
};
module.exports.getUsersProfile =  function(req, res){
	var path = 'users-edit',
		pathParent = 'users',
		pathCities = 'cities',
		pathLocations = 'locations';
	if(req.user._id){
		restler.get(req.protocol + '://' + req.get('host') + '/api/public/' + pathCities).on('complete', function(cities){
			restler.get(req.protocol + '://' + req.get('host') + '/api/public/' + pathLocations).on('complete', function(locations){
				restler.get(req.protocol + '://' + req.get('host') + '/api/' + pathParent + '/' + req.user._id + '?userToken=' + req.cookies.userToken).on('complete', function(data){
					if(data && data._id){
						res.render('admin/' + path, {
							title: languages.strings(language, 'profile'),
							description: languages.strings(language, 'profileDescriptionEdit'),
							user: req.user,
							languages: languages,
							language: language,
							bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
							path: path,
							pathParent: pathParent,
							data: data,
							roles: roles,
							sexes: sexes,
							cities: cities,
							status: status,
							locations: locations
						});
					}
					else
						res.redirect('/admin/' + pathParent + '/edit');
				});
			});
		});
	}
	else{
		res.redirect('/admin/' + pathParent + '/edit');
	}
};
// States controllers.
module.exports.getStates =  function(req, res){
	var path = 'states';
	restler.get(req.protocol + '://' + req.get('host') + '/api/' + path + '/' + '?userToken=' + req.cookies.userToken).on('complete', function(data){
		res.render('admin/' + path, {
			title: languages.strings(language, path),
			user: req.user,
			languages: languages,
			language: language,
			bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
			path: path,
			data: data
		});
	});
};
module.exports.getStatesEdit =  function(req, res){
	var path = 'states-edit',
		pathParent = 'states';
	if(req.params.id){
		restler.get(req.protocol + '://' + req.get('host') + '/api/' + pathParent + '/' + req.params.id + '?userToken=' + req.cookies.userToken).on('complete', function(data){
			if(data && data._id){
				res.render('admin/' + path, {
					title: languages.strings(language, 'edit'),
					user: req.user,
					languages: languages,
					language: language,
					bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
					path: path,
					pathParent: pathParent,
					data: data
				});
			}
			else
				res.redirect('/admin/' + pathParent + '/edit');
		});
	}
	else{
		res.render('admin/' + path, {
			title: languages.strings(language, 'add'),
			user: req.user,
			languages: languages,
			language: language,
			bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
			path: path,
			pathParent: pathParent
		});
	}
};
// Cities controllers.
module.exports.getCities =  function(req, res){
	var path = 'cities';
	restler.get(req.protocol + '://' + req.get('host') + '/api/' + path + '/' + '?userToken=' + req.cookies.userToken).on('complete', function(data){
		res.render('admin/' + path, {
			title: languages.strings(language, path),
			user: req.user,
			languages: languages,
			language: language,
			bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
			path: path,
			data: data
		});
	});
};
module.exports.getCitiesEdit =  function(req, res){
	var path = 'cities-edit',
		pathParent = 'cities',
		pathStates = 'states';
	restler.get(req.protocol + '://' + req.get('host') + '/api/public/' + pathStates).on('complete', function(states){
		if(req.params.id){
			restler.get(req.protocol + '://' + req.get('host') + '/api/' + pathParent + '/' + req.params.id + '?userToken=' + req.cookies.userToken).on('complete', function(data){
				if(data && data._id){
					res.render('admin/' + path, {
						title: languages.strings(language, 'edit'),
						user: req.user,
						languages: languages,
						language: language,
						bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
						path: path,
						pathParent: pathParent,
						data: data,
						states: states
					});
				}
				else
					res.redirect('/admin/' + pathParent + '/edit');
			});
		}
		else{
			res.render('admin/' + path, {
				title: languages.strings(language, 'add'),
				user: req.user,
				languages: languages,
				language: language,
				bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
				path: path,
				pathParent: pathParent,
				states: states
			});
		}
	});
};
// Locations controllers.
module.exports.getLocations =  function(req, res){
	var path = 'locations';
	restler.get(req.protocol + '://' + req.get('host') + '/api/' + path + '/' + '?userToken=' + req.cookies.userToken).on('complete', function(data){
		res.render('admin/' + path, {
			title: languages.strings(language, path),
			user: req.user,
			languages: languages,
			language: language,
			bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
			path: path,
			data: data
		});
	});
};
module.exports.getLocationsEdit =  function(req, res){
	var path = 'locations-edit',
		pathParent = 'locations',
		pathCities = 'cities';
	restler.get(req.protocol + '://' + req.get('host') + '/api/public/' + pathCities).on('complete', function(cities){
		if(req.params.id){
			restler.get(req.protocol + '://' + req.get('host') + '/api/' + pathParent + '/' + req.params.id + '?userToken=' + req.cookies.userToken).on('complete', function(data){
				if(data && data._id){
					res.render('admin/' + path, {
						title: languages.strings(language, 'edit'),
						user: req.user,
						languages: languages,
						language: language,
						bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
						path: path,
						pathParent: pathParent,
						data: data,
						cities: cities
					});
				}
				else
					res.redirect('/admin/' + pathParent + '/edit');
			});
		}
		else{
			res.render('admin/' + path, {
				title: languages.strings(language, 'add'),
				user: req.user,
				languages: languages,
				language: language,
				bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
				path: path,
				pathParent: pathParent,
				cities: cities
			});
		}
	});
};
// Categories controllers.
module.exports.getCategories =  function(req, res){
	var path = 'categories';
	restler.get(req.protocol + '://' + req.get('host') + '/api/' + path + '/' + '?userToken=' + req.cookies.userToken).on('complete', function(data){
		res.render('admin/' + path, {
			title: languages.strings(language, path),
			user: req.user,
			languages: languages,
			language: language,
			bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
			path: path,
			data: data
		});
	});
};
module.exports.getCategoriesEdit =  function(req, res){
	var path = 'categories-edit',
		pathParent = 'categories';
	if(req.params.id){
		restler.get(req.protocol + '://' + req.get('host') + '/api/' + pathParent + '/' + req.params.id + '?userToken=' + req.cookies.userToken).on('complete', function(data){
			if(data && data._id){
				res.render('admin/' + path, {
					title: languages.strings(language, 'edit'),
					user: req.user,
					languages: languages,
					language: language,
					bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
					path: path,
					pathParent: pathParent,
					data: data
				});
			}
			else
				res.redirect('/admin/' + pathParent + '/edit');
		});
	}
	else{
		res.render('admin/' + path, {
			title: languages.strings(language, 'add'),
			user: req.user,
			languages: languages,
			language: language,
			bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
			path: path,
			pathParent: pathParent
		});
	}
};
// Classes controllers.
module.exports.getClasses =  function(req, res){
	var path = 'classes';
	restler.get(req.protocol + '://' + req.get('host') + '/api/' + path + '/' + '?userToken=' + req.cookies.userToken).on('complete', function(data){
		res.render('admin/' + path, {
			title: languages.strings(language, path),
			user: req.user,
			languages: languages,
			language: language,
			bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
			path: path,
			data: data
		});
	});
};
module.exports.getClassesEdit =  function(req, res){
	var path = 'classes-edit',
		pathParent = 'classes',
		pathLocations = 'locations',
		pathCategories = 'categories';
	restler.get(req.protocol + '://' + req.get('host') + '/api/public/' + pathLocations).on('complete', function(locations){
		restler.get(req.protocol + '://' + req.get('host') + '/api/public/' + pathCategories).on('complete', function(categories){
			if(req.params.id){
				restler.get(req.protocol + '://' + req.get('host') + '/api/' + pathParent + '/' + req.params.id + '?userToken=' + req.cookies.userToken).on('complete', function(data){
					if(data && data._id){
						res.render('admin/' + path, {
							title: languages.strings(language, 'edit'),
							user: req.user,
							languages: languages,
							language: language,
							bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
							path: path,
							pathParent: pathParent,
							data: data,
							hours: hours,
							minutes: minutes,
							ampm: ampm,
							locations: locations,
							categories: categories
						});
					}
					else
						res.redirect('/admin/' + pathParent + '/edit');
				});
			}
			else{
				res.render('admin/' + path, {
					title: languages.strings(language, 'add'),
					user: req.user,
					languages: languages,
					language: language,
					bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
					path: path,
					pathParent: pathParent,
					hours: hours,
					minutes: minutes,
					ampm: ampm,
					locations: locations,
					categories: categories
				});
			}
		});
	});
};
// Generations controllers.
module.exports.getGenerations =  function(req, res){
	var path = 'generations';
	restler.get(req.protocol + '://' + req.get('host') + '/api/' + path + '/' + '?userToken=' + req.cookies.userToken).on('complete', function(data){
		res.render('admin/' + path, {
			title: languages.strings(language, path),
			user: req.user,
			languages: languages,
			language: language,
			bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
			path: path,
			data: data
		});
	});
};
module.exports.getGenerationsEdit =  function(req, res){
	var path = 'generations-edit',
		pathParent = 'generations',
		pathLocations = 'locations',
		pathCategories = 'categories';
	restler.get(req.protocol + '://' + req.get('host') + '/api/public/' + pathLocations).on('complete', function(locations){
		restler.get(req.protocol + '://' + req.get('host') + '/api/public/' + pathCategories).on('complete', function(categories){
			if(req.params.id){
				restler.get(req.protocol + '://' + req.get('host') + '/api/' + pathParent + '/' + req.params.id + '?userToken=' + req.cookies.userToken).on('complete', function(data){
					if(data && data._id){
						res.render('admin/' + path, {
							title: languages.strings(language, 'edit'),
							user: req.user,
							languages: languages,
							language: language,
							bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
							path: path,
							pathParent: pathParent,
							data: data,
							hours: hours,
							minutes: minutes,
							ampm: ampm,
							locations: locations,
							categories: categories
						});
					}
					else
						res.redirect('/admin/' + pathParent + '/edit');
				});
			}
			else{
				res.render('admin/' + path, {
					title: languages.strings(language, 'add'),
					user: req.user,
					languages: languages,
					language: language,
					bucket: req.app.get('aws').url + '/' + req.app.get('aws').bucket,
					path: path,
					pathParent: pathParent,
					hours: hours,
					minutes: minutes,
					ampm: ampm,
					locations: locations,
					categories: categories
				});
			}
		});
	});
};

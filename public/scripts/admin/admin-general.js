$(function(){
	$('table tfoot td').each(function(){
		if(this.className == 'input')
			$(this).html('<input type="text" placeholder="' + language['filter'] + '"/>');
		if(this.className == 'input calendar')
			$(this).html('<input class="ui calendar" type="text" placeholder="' + language['filter'] + '"/>');
	});
});
var datatablesPagingType = 'numbers';
var datatablesDom = '<"ui grid form"<"#actions.ten wide column"><"six wide column right"f>>t<"ui grid"<"ten wide column"Bi><"right floated six wide column"p>>';
function datatablesDrawCallback(){
	var dataTables_paginate = $('.dataTables_paginate');
	var page_numbers = dataTables_paginate.find('span').children().clone(true);
	dataTables_paginate.empty().append(page_numbers);
	dataTables_paginate.addClass('right floated ui pagination menu');
	dataTables_paginate.children().each(function(k, v){
		$(v).addClass('item');
		if($(v).hasClass('current'))
			$(v).addClass('active');
		if($(v).hasClass('ellipsis'))
			$(v).addClass('disabled');
	});
}
function datatablesInitComplete(){
	var dataTables_length = $('.dataTables_length');
	var select = dataTables_length.find('select').clone(true);
	dataTables_length.find('select').remove();
	select.attr('id', select.attr('name') + '_select').addClass('ui dropdown');
	dataTables_length.find('label').attr('for', select.attr('name') + '_select');
	dataTables_length.append(select);
	dataTables_length.addClass('field');
	select.dropdown();
	var dataTables_filter = $('.dataTables_filter');
	var input = dataTables_filter.find('input').clone(true);
	dataTables_filter.find('input').remove();
	input.attr('id', dataTables_filter.attr('id') + '_input');
	dataTables_filter.find('label').attr('for', dataTables_filter.attr('id') + '_input');
	dataTables_filter.append(input);
	$('#actions').append($('.actions#append'));
	this.api().columns().every(function(){
		var column = this;
		if(column.footer()){
			$('input', this.footer()).on('keyup change', function(){
				if(column.search() !== this.value)
					column.search(this.value).draw();
			});
		}
	});
	this.api().columns().every(function(){
		var column = this;
		if(column.footer() && column.footer().className == 'select'){
			var select = $('<select class="ui dropdown fluid search filter"><option value="">' + language['filter'] + '</option></select>').appendTo($(column.footer()).empty()).on('change', function(){
				var val = $.fn.dataTable.util.escapeRegex($(this).val());
				column.search(val ? '^'+val+'$':'', true, false).draw();
			});
			column.data().unique().sort().each(function(d, j){
				select.append('<option value="'+d+'">'+d+'</option>');
			});
			$('.ui.dropdown.filter').dropdown();
		}
	});
}
function formatDateTime(dateString){
	var date = new Date(dateString);
	var day = date.getDate();
	var month = date.getMonth();
	var year = date.getFullYear();
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var seconds = date.getSeconds();
	return (day <= 9 ? '0' + day : day) + '/' + (month <= 9 ? '0' + month : month) + '/' + year + ' ' + (hours <= 9 ? '0' + hours : hours) + ':' + (minutes <= 9 ? '0' + minutes : minutes) + ':' + (seconds <= 9 ? '0' + seconds : seconds);
}
function formatDate(dateString){
	var date = new Date(dateString);
	var day = date.getDate();
	var month = date.getMonth();
	var year = date.getFullYear().toString().substr(2, 2);
	return (day <= 9 ? '0' + day : day) + '/' + (month <= 9 ? '0' + month : month) + '/' + year;
}

var language = {
	// Login strings.
	pleaseEnterYourEmail: 'Por favor ingresa tu email (al menos 5 caracteres)',
	pleaseEnterAValidEmail: 'Por favor ingresa un email válido',
	pleaseEnterYourPassword: 'Por favor ingresa tu contraseña (al menos 8 caracteres)',
	pleaseEnterYourID: 'Por favor ingresa tu ID'
}

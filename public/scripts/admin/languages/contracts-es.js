var element = 'contract';
var elements = 'contracts';
var language = {
	theRecordWasDeleted: 'El ' + element + ' fue eliminado',
	theRecordWasSaved: 'El ' + element + ' fue guardado',
	thereWasAnErrorDeletingThisRecord: 'Ha ocurrido un error al eliminar este ' + element
}
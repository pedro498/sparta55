// Modules declarations.
var languages = require('./languages.js'),
	language = 'es',
	jade = require('jade'),
	fs = require('fs'),
	path = require('path'),
	nodemailer = require('nodemailer'),
	nodemailerSendgridTransport = require('nodemailer-sendgrid-transport');
module.exports.sendWelcomeEmail = function(user, req){
	var subject = languages.strings(language, 'welcome'),
		content = languages.strings(language, 'welcomeEmail'),
		link = '/secure/verify/' + user.verify,
		cta = languages.strings(language, 'verify');
	sendEmail(user, subject, content, link, cta, req);
}
module.exports.sendWelcomeEmailEmployee = function(user, req){
var subject = languages.strings(language, 'welcome'),
	content = languages.strings(language, 'welcomeEmailEmployee'),
	link = '/secure/verify/' + user.verify,
	cta = languages.strings(language, 'verify');
sendEmail(user, subject, content, link, cta, req);
}
module.exports.sendVerifyEmail = function(user, req){
	var subject = languages.strings(language, 'verify'),
		content = languages.strings(language, 'verifyEmail'),
		link = '/secure/verify/' + user.verify,
		cta = languages.strings(language, 'verify');
	sendEmail(user, subject, content, link, cta, req);
}
module.exports.sendForgotEmail = function(user, req){
	var subject = languages.strings(language, 'forgot'),
		content = languages.strings(language, 'passwordEmail'),
		link = '/secure/forgot/' + user.forgot,
		cta = languages.strings(language, 'forgot');
	sendEmail(user, subject, content, link, cta, req);
}
function sendEmail(user, subject, content, link, cta, req){
	var url = req.app.get('url'),
		email = req.app.get('email'),
		facebook = req.app.get('facebook'),
		twitter = req.app.get('twitter'),
		instagram = req.app.get('instagram'),
		sendGrid = req.app.get('sendGrid');
	var transporter = nodemailer.createTransport(nodemailerSendgridTransport(sendGrid));
	fs.readFile(path.join(__dirname, '../views/emails/emails.jade'), 'utf8', function(err, template){
		var fn = jade.compile(template),
			html = fn({
				title: languages.strings(language, 'hi') + ' ' + user.name,
				brand: languages.strings(language, 'brand'),
				url: url,
				content: content,
				link: url + link,
				cta: cta,
				thanksForYourPreference: languages.strings(language, 'thanksForYourPreference'),
				teamBrand: languages.strings(language, 'teamBrand'),
				facebookLabel: languages.strings(language, 'facebook'),
				twitterLabel: languages.strings(language, 'twitter'),
				instagramLabel: languages.strings(language, 'instagram'),
				facebook: facebook,
				twitter: twitter,
				instagram: instagram
			}
		);
		transporter.sendMail({
			from: '"' + languages.strings(language, 'brand') + '" ' + email,
			to: '"' + user.name + ' ' + user.lastName + '" ' + user.email,
			subject: subject,
			html: html
		}, function(err, info){});
	});
}

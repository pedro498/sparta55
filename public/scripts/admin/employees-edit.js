$(function(){
	var element = 'employees';
	initialize_(element);
	$('#newPassword').click(function(){
		$('#newPassword').hide();
		$('#password').show();
		return false;
	});
	$('#avatarFile').click(function(){
		$('#avatar').click();
		return false;
	});
	$("#avatar").change(function(){
		var file = this.files[0],
		fileExtention = file.name.split('.').pop().toLowerCase(),
		fileSize = file.size;
		if(fileSize > 2048000 || fileExtention != 'jpg'){
			$('#form #avatar').val('');
			$('#avatarFile span').html(language['avatar']);
			if(fileSize > 2048000)
				alert(language['pleaseSelectASmallerFile']);
			else if(fileExtention != 'jpg')
				alert(language['pleaseSelectAJPGFile']);
		}
		else{
			avatar = $('#form #avatar').val();
			$('#avatarFile span').html(avatar.match(/[^\/\\]+$/));
		}
	});
	$('#ineFile').click(function(){
		$('#ine').click();
		return false;
	});
	$("#ine").change(function(){
		var file = this.files[0],
		fileExtention = file.name.split('.').pop().toLowerCase(),
		fileSize = file.size;
		if(fileSize > 2048000 || fileExtention != 'pdf'){
			$('#form #ine').val('');
			$('#ineFile span').html(language['ine']);
			if(fileSize > 2048000)
				alert(language['pleaseSelectASmallerFile']);
			else if(fileExtention != 'pdf')
				alert(language['pleaseSelectAPDFFile']);
		}
		else{
			ine = $('#form #ine').val();
			$('#ineFile span').html(ine.match(/[^\/\\]+$/));
		}
	});
	$('#confidentialityAgreementFile').click(function(){
		$('#confidentialityAgreement').click();
		return false;
	});
	$("#confidentialityAgreement").change(function(){
		var file = this.files[0],
		fileExtention = file.name.split('.').pop().toLowerCase(),
		fileSize = file.size;
		if(fileSize > 2048000 || fileExtention != 'pdf'){
			$('#form #confidentialityAgreement').val('');
			$('#confidentialityAgreementFile span').html(language['confidentialityAgreement']);
			if(fileSize > 2048000)
				alert(language['pleaseSelectASmallerFile']);
			else if(fileExtention != 'pdf')
				alert(language['pleaseSelectAPDFFile']);
		}
		else{
			confidentialityAgreement = $('#form #confidentialityAgreement').val();
			$('#confidentialityAgreementFile span').html(confidentialityAgreement.match(/[^\/\\]+$/));
		}
	});
	$('#temporalContractFile').click(function(){
		$('#temporalContract').click();
		return false;
	});
	$("#temporalContract").change(function(){
		var file = this.files[0],
		fileExtention = file.name.split('.').pop().toLowerCase(),
		fileSize = file.size;
		if(fileSize > 2048000 || fileExtention != 'pdf'){
			$('#form #temporalContract').val('');
			$('#temporalContractFile span').html(language['temporalContract']);
			if(fileSize > 2048000)
				alert(language['pleaseSelectASmallerFile']);
			else if(fileExtention != 'pdf')
				alert(language['pleaseSelectAPDFFile']);
		}
		else{
			temporalContract = $('#form #temporalContract').val();
			$('#temporalContractFile span').html(temporalContract.match(/[^\/\\]+$/));
		}
	});
	$('#permanentContractFile').click(function(){
		$('#permanentContract').click();
		return false;
	});
	$("#permanentContract").change(function(){
		var file = this.files[0],
		fileExtention = file.name.split('.').pop().toLowerCase(),
		fileSize = file.size;
		if(fileSize > 2048000 || fileExtention != 'pdf'){
			$('#form #permanentContract').val('');
			$('#permanentContractFile span').html(language['permanentContract']);
			if(fileSize > 2048000)
				alert(language['pleaseSelectASmallerFile']);
			else if(fileExtention != 'pdf')
				alert(language['pleaseSelectAPDFFile']);
		}
		else{
			permanentContract = $('#form #permanentContract').val();
			$('#permanentContractFile span').html(permanentContract.match(/[^\/\\]+$/));
		}
	});
	$('#studySheetFile').click(function(){
		$('#studySheet').click();
		return false;
	});
	$("#studySheet").change(function(){
		var file = this.files[0],
		fileExtention = file.name.split('.').pop().toLowerCase(),
		fileSize = file.size;
		if(fileSize > 2048000 || fileExtention != 'pdf'){
			$('#form #studySheet').val('');
			$('#studySheetFile span').html(language['studySheet']);
			if(fileSize > 2048000)
				alert(language['pleaseSelectASmallerFile']);
			else if(fileExtention != 'pdf')
				alert(language['pleaseSelectAPDFFile']);
		}
		else{
			studySheet = $('#form #studySheet').val();
			$('#studySheetFile span').html(studySheet.match(/[^\/\\]+$/));
		}
	});
	$('#resignationLetterFile').click(function(){
		$('#resignationLetter').click();
		return false;
	});
	$("#resignationLetter").change(function(){
		var file = this.files[0],
		fileExtention = file.name.split('.').pop().toLowerCase(),
		fileSize = file.size;
		if(fileSize > 2048000 || fileExtention != 'pdf'){
			$('#form #resignationLetter').val('');
			$('#resignationLetterFile span').html(language['resignationLetter']);
			if(fileSize > 2048000)
				alert(language['pleaseSelectASmallerFile']);
			else if(fileExtention != 'pdf')
				alert(language['pleaseSelectAPDFFile']);
		}
		else{
			resignationLetter = $('#form #resignationLetter').val();
			$('#resignationLetterFile span').html(resignationLetter.match(/[^\/\\]+$/));
		}
	});
});
function save_(element){
	disabled_();
	var error = '';
	var _id = $('#form #_id').val(),
		name = $('#form #name').val(),
		lastName = $('#form #lastName').val(),
		role = $('#form #role option:selected').val(),
		email = $('#form #email').val(),
		password = $('#form #password').val(),
		birthday = $('#form #birthday').val(),
		sex = $('#form #sex').val(),
		phone = $('#form #phone').val(),
		emergencyPhone = $('#form #emergencyPhone').val(),
		address = $('#form #address').val(),
		postCode = $('#form #postCode').val(),
		city = $('#form #city option:selected').val(),
		status = $('#form #status option:selected').val(),
		location = $('#form #location option:selected').val(),
		nss = $('#form #nss').val(),
		curp = $('#form #curp').val(),
		rfc = $('#form #rfc').val(),
		admissionDate = $('#form #admissionDate').val(),
		dischargeDate = $('#form #dischargeDate').val(),
		avatar = $('#form #avatar').val(),
		ine = $('#form #ine').val(),
		confidentialityAgreement = $('#form #confidentialityAgreement').val(),
		temporalContract = $('#form #temporalContract').val(),
		permanentContract = $('#form #permanentContract').val(),
		studySheet = $('#form #studySheet').val(),
		resignationLetter = $('#form #resignationLetter').val();
	if(name.length < 3){
		$('#name_').addClass('error');
		error += '<li>' + language['pleaseEnterTheName'] + '</li>';
	}
	if(lastName.length < 3){
		$('#lastName_').addClass('error');
		error += '<li>' + language['pleaseEnterTheLastName'] + '</li>';
	}
	if(role.length == 0){
		$('#role_').addClass('error');
		error += '<li>' + language['pleaseSelectTheRole'] + '</li>';
	}
	if(_id.length == 0 && email.length < 5){
		$('#email_').addClass('error');
		error += '<li>' + language['pleaseEnterTheEmail'] + '</li>';
	}
	else if(_id.length == 0 && !validator.isEmail(email)){
		$('#email_').addClass('error');
		error += '<li>' + language['pleaseEnterAValidEmail'] + '</li>';
	}
	if(_id.length > 0 && password.length > 0 && password.length < 8){
		$('#password_').addClass('error');
		error += '<li>' + language['pleaseEnterThePassword'] + '</li>';
	}
	if(city.length == 0){
		$('#city_').addClass('error');
		error += '<li>' + language['pleaseSelectTheCity'] + '</li>';
	}
	if(error == ''){
		if(_id.length > 0)
			email = undefined;
		if(_id.length > 0 && password.length == 0)
			password = undefined;
		if(status.length == 0)
			status = undefined;
		if(location.length == 0)
			location = undefined;
		var data = {
			name: name,
			lastName: lastName,
			role: role,
			email: email,
			password: password,
			birthday: birthday,
			sex: sex,
			phone: phone,
			emergencyPhone: emergencyPhone,
			address: address,
			postCode: postCode,
			city: city,
			status: status,
			location: location,
			nss: nss,
			curp: curp,
			rfc: rfc,
			admissionDate: admissionDate,
			dischargeDate: dischargeDate
		};
		var dataFile = new FormData();
		if(avatar && avatar.length > 0)
			dataFile.append('avatar', $('#avatar')[0].files[0]);
		if(ine && ine.length > 0)
			dataFile.append('ine', $('#ine')[0].files[0]);
		if(confidentialityAgreement && confidentialityAgreement.length > 0)
			dataFile.append('confidentialityAgreement', $('#confidentialityAgreement')[0].files[0]);
		if(temporalContract && temporalContract.length > 0)
			dataFile.append('temporalContract', $('#temporalContract')[0].files[0]);
		if(permanentContract && permanentContract.length > 0)
			dataFile.append('permanentContract', $('#permanentContract')[0].files[0]);
		if(studySheet && studySheet.length > 0)
			dataFile.append('studySheet', $('#studySheet')[0].files[0]);
		if(resignationLetter && resignationLetter.length > 0)
			dataFile.append('resignationLetter', $('#resignationLetter')[0].files[0]);
		if(avatar && avatar.length > 0 || ine && ine.length > 0 || confidentialityAgreement && confidentialityAgreement.length > 0 || temporalContract && temporalContract.length > 0 || permanentContract && permanentContract.length > 0 || studySheet && studySheet.length > 0 || resignationLetter && resignationLetter.length > 0){
			for(var key in data){
				if(data[key] != undefined)
					dataFile.append(key, data[key]);
			}
			saveRequest_(element, dataFile, true);
		}
		else
			saveRequest_(element, data);
	}
	else
		error_(error);
}

// Modules declarations.
var fs = require('fs'),
	languages = require('./languages.js'),
	language = 'es',
	thereWasAnErrorPleaseTryLaterString = languages.strings(language, 'thereWasAnErrorPleaseTryLater');
module.exports.find = function(err, element, res, key){
	var notFoundString = languages.strings_(language, key + 'NotFound');
	if(err)
		res.status(500).jsonp({'message': thereWasAnErrorPleaseTryLaterString});
	else if(!element)
		res.status(404).jsonp({'message': notFoundString});
	else
		return true;
	return false;
};
module.exports.save = function(err, element, res, key){
	var notSavedString = languages.strings_(language, key + 'NotSaved');
	var alreadyExistsString = languages.strings_(language, key + 'AlreadyExists');
	if(err){
		if(err.code == 11000)
			res.status(500).jsonp({'message': alreadyExistsString});
		else{
			if(err.errors)
				res.status(500).jsonp({'message': err.errors[Object.keys(err.errors)[Object.keys(err.errors).length - 1]].message});
			else
				res.status(500).jsonp({'message': err.message});
		}
	}
	else if(!element)
		res.status(404).jsonp({'message': notSavedString});
	else
		return true;
	return false;
};
module.exports.remove = function(err, element, res, key){
	var deletedString = languages.strings_(language, key + 'Deleted');
	if(err)
		res.status(500).jsonp({'message': thereWasAnErrorPleaseTryLaterString});
	else
		res.status(202).jsonp({'message': deletedString});
};
module.exports.canRemove = function(err, element, res){
	if(err)
		res.status(500).jsonp({'message': thereWasAnErrorPleaseTryLaterString});
	else if(!element)
		return true;
	return false;
};
module.exports.notRemove = function(err, element, res, key){
	var notDeletedString = languages.strings_(language, key + 'NotDeleted');
	if(err)
		res.status(500).jsonp({'message': thereWasAnErrorPleaseTryLaterString});
	else
		res.status(500).jsonp({'message': notDeletedString});
};
module.exports.deleteFile = function(filePath){
	fs.unlink(filePath, function(err){});
}
module.exports.nextError = function(next, error){
	next(new Error(languages.strings(language, error)));
}

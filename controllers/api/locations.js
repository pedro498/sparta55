// Modules declarations.
var mongoose = require('mongoose'),
	validation = require('../../libraries/validation.js'),
	languages = require('../../libraries/languages.js'),
	language = 'es',
	capitalize = require('capitalize'),
	Locations = mongoose.model('Locations'),
	Cities = mongoose.model('Cities');
// Locations controllers.
function saveLocation(location, res){
	location.save(function(err, location){
		if(validation.save(err, location, res, 'location'))
			res.status(200).jsonp(location);
	});
}
function saveLocationCity(location, city, res){
	location.city = city._id;
	location.save(function(err, location){
		if(validation.save(err, location, res, 'location')){
			city.locations.push(location);
			city.save(function(err, city){
				if(validation.save(err, city, res, 'city'))
					res.status(200).jsonp(location);
			});
		}
	});
}
function removeLocation(location, res){
	location.remove(function(err){
		validation.remove(err, location, res, 'location');
	});
}
module.exports.getLocations = function(req, res){
	Locations.find(function(err, locations){
		if(validation.find(err, locations, res, 'locations'))
			res.status(200).jsonp(locations);
	}).select('name email phone city classes price20 price55').populate([{
		path: 'city',
		select: 'name'
	}]).sort('name').lean();
};
module.exports.postLocation = function(req, res){
	var location = new Locations();
	if(typeof req.body.name !== 'undefined')
		location.name = capitalize.words(req.body.name);
	if(typeof req.body.email !== 'undefined')
		location.email = req.body.email.toLowerCase();
	if(typeof req.body.phone !== 'undefined')
		location.phone = req.body.phone.toUpperCase();
	if(typeof req.body.address !== 'undefined')
		location.address = capitalize.words(req.body.address);
	if(typeof req.body.postCode !== 'undefined')
		location.postCode = req.body.postCode.toUpperCase();
	if(typeof req.body.price20 !== 'undefined')
		location.price20 = req.body.price20.toUpperCase();
	if(typeof req.body.price55 !== 'undefined')
		location.price55 = req.body.price55.toUpperCase();
	Cities.findById(req.body.city, function(err, city){
		if(validation.find(err, city, res, 'city'))
			saveLocationCity(location, city, res);
	});
};
module.exports.getLocation = function(req, res){
	Locations.findById(req.params.id, function(err, location){
		if(validation.find(err, location, res, 'location'))
			res.status(200).jsonp(location);
	}).select('name email phone address postCode city price20 price55 classes').populate([{
		path: 'city',
		select: 'name'
	}, {
		path: 'classes',
		select: 'startTime category classRoom',
		populate: {
			path: 'category',
			select: 'name'
		},
		options: {
			sort: {
				'category': 1,
				'classRoom': 1,
				'startTime.ampm': 1,
				'startTime.hours': 1,
				'startTime.minutes': 1
			}
		}
	}]).lean();
};
module.exports.putLocation = function(req, res){
	Locations.findById(req.params.id, function(err, location){
		if(validation.find(err, location, res, 'location')){
			if(typeof req.body.name !== 'undefined')
				location.name = capitalize.words(req.body.name);
			if(typeof req.body.email !== 'undefined')
				location.email = req.body.email;
			if(typeof req.body.phone !== 'undefined')
				location.phone = req.body.phone.toUpperCase();
			if(typeof req.body.address !== 'undefined')
				location.address = capitalize.words(req.body.address);
			if(typeof req.body.postCode !== 'undefined')
				location.postCode = req.body.postCode.toUpperCase();
			if(typeof req.body.price20 !== 'undefined')
				location.price20 = req.body.price20.toUpperCase();
			if(typeof req.body.price55 !== 'undefined')
				location.price55 = req.body.price55.toUpperCase();
			if(typeof req.body.city === 'undefined')
				req.body.city = location.city;
			if(typeof req.body.city !== 'undefined' && req.body.city == String(location.city))
				saveLocation(location, res);
			else{
				Cities.findById(req.body.city, function(err, city){
					if(validation.find(err, city, res, 'city')){
						Cities.findById(location.city, function(err, cityRel){
							if(validation.find(err, cityRel, res, 'city')){
								cityRel.locations.pull(location);
								cityRel.save(function(err, cityRel){
									if(validation.save(err, cityRel, res, 'city'))
										saveLocationCity(location, city, res);
								});
							}
						});
					}
				});
			}
		}
	});
};
module.exports.deleteLocation = function(req, res){
	Locations.findById(req.params.id, function(err, location){
		if(validation.find(err, location, res, 'location')){
			if((location.classes && location.classes.length > 0))
				validation.notRemove(err, location, res, 'location');
			else{
				Cities.findById(location.city, function(err, cityRel){
					if(validation.canRemove(err, cityRel, res, 'city'))
						removeLocation(location, res);
					else{
						cityRel.locations.pull(location);
						cityRel.save(function(err, cityRel){
							if(validation.save(err, cityRel, res, 'city'))
								removeLocation(location, res);
						});
					}
				});
			}
		}
	});
};
// Public locations controllers.
module.exports.getPublicLocations = function(req, res){
	Locations.find(function(err, locations){
		if(validation.find(err, locations, res, 'locations'))
			res.status(200).jsonp(locations);
	}).select('name').sort('name').lean();
};

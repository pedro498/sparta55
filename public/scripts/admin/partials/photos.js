$(function(){
	var element = 'photos';
	var elementParent = 'users';
	var tablePhotos = $('#tablePhotos').DataTable({
		'language': datatablesLanguagePartial,
		'autoWidth': false,
		'responsive': false,
		'columnDefs': [{
			'targets': 0,
			'className': 'hidden',
			'searchable': true,
			'orderable': false
		}, {
			'targets': [2,3,4],
			'className': 'hidden'
		}],
		'order': [[1, 'dsc']],
		'buttons': ['copy', 'print'],
    'pagingType': datatablesPagingType,
		'drawCallback': datatablesDrawCallback,
		'dom': 't<"ui grid"<"ten wide column"Bi><"right floated six wide column"p>>',
		'initComplete': datatablesInitComplete
	});
	$('#tablePhotos tbody').on('click', 'tr', function(){
		var tr = $(this);
		var row = tablePhotos.row(tr);
		if(row.child.isShown()){
			row.child.hide();
			tr.removeClass('shown');
		}else{
			row.child(formatPhotos(row.data())).show();
			tr.addClass('shown');
		}
	});
	$('#showPhotos').click(function(){
		$('#formPhotos')[0].reset();
		$('.ui.modal#modalPhotos').modal('show');
	});
	$('#frontFile').click(function(){
		$('#front').click();
		return false;
	});
	$("#front").change(function(){
		var file = this.files[0],
		fileExtention = file.name.split('.').pop().toLowerCase(),
		fileSize = file.size;
		if(fileSize > 2048000 || fileExtention != 'jpg'){
			$('#formPhotos #front').val('');
			$('#frontFile span').html(language['front']);
			if(fileSize > 2048000)
				alert(language['pleaseSelectASmallerFile']);
			else if(fileExtention != 'jpg')
				alert(language['pleaseSelectAJPGFile']);
		}
		else{
			front = $('#formPhotos #front').val();
			$('#frontFile span').html(front.match(/[^\/\\]+$/));
		}
	});
	$('#backFile').click(function(){
		$('#back').click();
		return false;
	});
	$("#back").change(function(){
		var file = this.files[0],
		fileExtention = file.name.split('.').pop().toLowerCase(),
		fileSize = file.size;
		if(fileSize > 2048000 || fileExtention != 'jpg'){
			$('#formPhotos #back').val('');
			$('#backFile span').html(language['back']);
			if(fileSize > 2048000)
				alert(language['pleaseSelectASmallerFile']);
			else if(fileExtention != 'jpg')
				alert(language['pleaseSelectAJPGFile']);
		}
		else{
			back = $('#formPhotos #back').val();
			$('#backFile span').html(back.match(/[^\/\\]+$/));
		}
	});
	$('#addPhotos').click(function(){
		disabled_();
		var error = '';
		var _id = $('#form #_id').val(),
			front = $('#formPhotos #front').val(),
			back = $('#formPhotos #back').val();
		var data = {
			photos: {
				front: front,
				back: back
			}
		}
		for(var key in data.photos){
			if(data.photos[key] == '' && typeof(data.photos[key]) !== 'boolean')
				error = language['pleaseEnterTheInfoRequired'];
		}
		if(error == ''){
			function successCallback(response){
				$('.ui.modal#modalPhotos').modal('hide');
				tablePhotos.row.add([
					response._id,
					formatDateTime(response.createdAt),
					response.front,
					response.back,
					response.createdBy
				]).column(1).order('desc').draw(false);
				$('#formPhotos')[0].reset();
			}
			var dataFile = new FormData();
			if(front && front.length > 0 && back && back.length > 0){
				dataFile.append('front', $('#front')[0].files[0]);
				dataFile.append('back', $('#back')[0].files[0]);
				for(var key in data){
					if(data[key] != undefined)
						dataFile.append(key, data[key]);
				}
				savePartialsRequest_(element, elementParent, dataFile, successCallback, true);
			}
			else
				savePartialsRequest_(element, elementParent, data, successCallback);
		}
		else{
			alert_(error);
		}
		return false;
	});
});
function formatPhotos(data){
	return '<table class="details">' +
		'<tr>' +
		rowImage('front', data[2]) +
		rowImage('back', data[3]) +
		'</tr>' +
		'<tr>' +
		rowString('createdBy', data[4]) +
		rowEmpty() +
		'</tr>' +
		'</table>';
}

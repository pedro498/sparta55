$(function(){
	var element = 'clinical';
	var elementParent = 'users';
	var tableClinical = $('#tableClinical').DataTable({
		'language': datatablesLanguagePartial,
		'autoWidth': false,
		'responsive': false,
		'columnDefs': [{
			'targets': 0,
			'className': 'hidden',
			'searchable': true,
			'orderable': false
		}, {
			'targets': [5,6,7,8,9,10],
			'className': 'hidden'
		}],
		'order': [[1, 'dsc']],
		'buttons': ['copy', 'print'],
    'pagingType': datatablesPagingType,
		'drawCallback': datatablesDrawCallback,
		'dom': 't<"ui grid"<"ten wide column"Bi><"right floated six wide column"p>>',
		'initComplete': datatablesInitComplete
	});
	$('#tableClinical tbody').on('click', 'tr', function(){
		var tr = $(this);
		var row = tableClinical.row(tr);
		if(row.child.isShown()){
			row.child.hide();
			tr.removeClass('shown');
		}else{
			row.child(formatClinical(row.data())).show();
			tr.addClass('shown');
		}
	});
	$('#showClinical').click(function(){
		$('#formClinical')[0].reset();
		$('.ui.modal#modalClinical').modal('show');
	});
	$('#addClinical').click(function(){
		disabled_();
		var error = '';
		var _id = $('#form #_id').val(),
			diagnosis = $('#formClinical #diagnosis').val(),
			medicines = $('#formClinical #medicines').val(),
			hoursOfSleep = $('#formClinical #hoursOfSleep').val(),
			obesity = $('#formClinical #obesity').is(':checked'),
			diabetes = $('#formClinical #diabetes').is(':checked'),
			hta = $('#formClinical #hta').is(':checked'),
			cancer = $('#formClinical #cancer').is(':checked'),
			hypercholesterolemia = $('#formClinical #hypercholesterolemia').is(':checked');
		var data = {
			clinical: {
				diagnosis: diagnosis,
				medicines: medicines,
				hoursOfSleep: hoursOfSleep,
				obesity: obesity,
				diabetes: diabetes,
				hta: hta,
				cancer: cancer,
				hypercholesterolemia: hypercholesterolemia
			}
		}
		for(var key in data.clinical){
			if(data.clinical[key] == '' && typeof(data.clinical[key]) !== 'boolean')
				error = language['pleaseEnterTheInfoRequired'];
		}
		if(error == ''){
			function successCallback(response){
				$('.ui.modal#modalClinical').modal('hide');
				tableClinical.row.add([
					response._id,
					formatDateTime(response.createdAt),
					response.diagnosis,
					response.medicines,
					response.hoursOfSleep,
					response.obesity,
					response.diabetes,
					response.hta,
					response.cancer,
					response.hypercholesterolemia,
					response.createdBy
				]).column(1).order('desc').draw(false);
				$('#formClinical')[0].reset();
			}
			savePartialsRequest_(element, elementParent, data, successCallback);
		}
		else{
			alert_(error);
		}
		return false;
	});
});
function formatClinical(data){
	return '<table class="details">' +
		'<tr>' +
		rowSection('familyHistory', 'start') +
		'</tr>' +
		'<tr>' +
		rowBoolean('obesity', data[5]) +
		rowBoolean('diabetes', data[6]) +
		'</tr>' +
		'<tr>' +
		rowBoolean('hta', data[7]) +
		rowBoolean('cancer', data[8]) +
		'</tr>' +
		'<tr>' +
		rowBoolean('hypercholesterolemia', data[9]) +
		rowEmpty() +
		'<tr>' +
		rowSection('familyHistory', 'end') +
		'</tr>' +
		'<tr>' +
		rowString('createdBy', data[10]) +
		rowEmpty() +
		'</tr>' +
		'</table>';
}

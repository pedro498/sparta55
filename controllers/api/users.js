// Modules declarations.
var mongoose = require('mongoose'),
	validation = require('../../libraries/validation.js'),
	emails = require('../../libraries/emails.js'),
	languages = require('../../libraries/languages.js'),
	language = 'es',
	capitalize = require('capitalize'),
	bcryptjs = require('bcryptjs'),
	jsonWebToken = require('jsonwebtoken'),
	Users = mongoose.model('Users'),
	States = mongoose.model('States'),
	Cities = mongoose.model('Cities');
// Users controllers.
function saveUser(user, res){
	user.save(function(err, user){
		if(validation.save(err, user, res, 'user')){
			if(!err && user.deleteFile && user.deleteFile != '')
				validation.deleteFile('public/files/avatars/' + user.deleteFile);
			res.status(200).jsonp(user);
		}
	});
}
function saveUserCity(user, city, res){
	user.city = city._id;
	user.save(function(err, user){
		if(validation.save(err, user, res, 'user')){
			if(!err && user.deleteFile && user.deleteFile != '')
				validation.deleteFile('public/files/avatars/' + user.deleteFile);
			city.users.push(user);
			city.save(function(err, city){
				if(validation.save(err, city, res, 'city'))
					res.status(200).jsonp(user);
			});
		}
	});
}
function saveUserCityEmail(user, city, req, res){
	user.city = city._id;
	user.save(function(err, user){
		if(validation.save(err, user, res, 'user')){
			city.users.push(user);
			city.save(function(err, city){
				if(validation.save(err, city, res, 'city')){
					if(user.role == 'user')
						emails.sendWelcomeEmail(user, req);
					else
						emails.sendWelcomeEmailEmployee(user, req);
						res.status(200).jsonp(user);
				}
			});
		}
	});
}
function saveUserPartial(user, partial, res){
	user.save(function(err, user){
		if(validation.save(err, user, res, 'user'))
			res.status(200).jsonp(partial);
	});
}
function saveUserMessage(user, message, res){
	user.save(function(err, user){
		if(validation.save(err, user, res, 'user'))
			res.status(200).jsonp(message);
	});
}
function removeUser(user, res){
	user.remove(function(err){
		if(!err && user.avatar && user.avatar != '')
			validation.deleteFile('public/files/avatars/' + user.avatar);
		validation.remove(err, user, res, 'user');
	});
}
module.exports.getEmployees = function(req, res){
	var options = {
		role: {
			$ne: 'user'
		}
	};
	Users.find(options, function(err, users){
		if(validation.find(err, users, res, 'users'))
			res.status(200).jsonp(users);
	}).select('name lastName email role city status location').populate([{
		path: 'city',
		select: 'name'
	}]).sort('name lastName').lean();
};
module.exports.postEmployee = function(req, res){
	var user = new Users({
		verify: mongoose.Types.ObjectId(),
		verified: false,
		password: bcryptjs.hashSync(req.app.get('secret'), bcryptjs.genSaltSync(10)),
		status: 'active'
	});
	if(typeof req.body.name !== 'undefined')
		user.name = capitalize.words(req.body.name);
	if(typeof req.body.lastName !== 'undefined')
		user.lastName = capitalize.words(req.body.lastName);
	if(typeof req.body.role !== 'undefined')
		user.role = req.body.role;
	if(typeof req.body.email !== 'undefined')
		user.email = req.body.email.toLowerCase();
	if(typeof req.body.birthday !== 'undefined'){
		var birthday = req.body.birthday.split("/");
		birthday = new Date(+birthday[2], +birthday[1] - 1, +birthday[0]);
		if(isNaN(birthday) == false){
			user.birthday = birthday;
		}
		else
			user.birthday = undefined;
	}
	if(typeof req.body.sex !== 'undefined'){
		if(req.body.sex != '')
			user.sex = req.body.sex;
		else
			user.sex = undefined;
	}
	if(typeof req.body.phone !== 'undefined')
		user.phone = req.body.phone.toUpperCase();
	if(typeof req.body.emergencyPhone !== 'undefined')
		user.emergencyPhone = req.body.emergencyPhone.toUpperCase();
	if(typeof req.body.address !== 'undefined')
		user.address = capitalize.words(req.body.address);
	if(typeof req.body.postCode !== 'undefined')
		user.postCode = req.body.postCode.toUpperCase();
	if(typeof req.body.status !== 'undefined')
		user.status = req.body.status;
	if(typeof req.body.location !== 'undefined')
		user.location = req.body.location;
	if(typeof req.body.nss !== 'undefined')
		user.nss = req.body.nss.toUpperCase();
	if(typeof req.body.curp !== 'undefined')
		user.curp = req.body.curp.toUpperCase();
	if(typeof req.body.rfc !== 'undefined')
		user.rfc = req.body.rfc.toUpperCase();
	if(typeof req.files !== 'undefined'){
		if(typeof req.files.avatar !== 'undefined')
			user.avatar = req.files.avatar[0].filename;
		if(typeof req.files.ine !== 'undefined')
			user.ine = req.files.ine[0].filename;
		if(typeof req.files.confidentialityAgreement !== 'undefined')
			user.confidentialityAgreement = req.files.confidentialityAgreement[0].filename;
		if(typeof req.files.temporalContract !== 'undefined')
			user.temporalContract = req.files.temporalContract[0].filename;
		if(typeof req.files.permanentContract !== 'undefined')
			user.permanentContract = req.files.permanentContract[0].filename;
		if(typeof req.files.studySheet !== 'undefined')
			user.studySheet = req.files.studySheet[0].filename;
		if(typeof req.files.resignationLetter !== 'undefined')
			user.resignationLetter = req.files.resignationLetter[0].filename;
	}
	Cities.findById(req.body.city, function(err, city){
		if(validation.find(err, city, res, 'city'))
			saveUserCityEmail(user, city, req, res);
	});
};
module.exports.getEmployee = function(req, res){
	var options = {
		_id: req.params.id,
		role: {
			$ne: 'user'
		}
	};
	Users.findOne(options, function(err, user){
		if(validation.find(err, user, res, 'user'))
			res.status(200).jsonp(user);
	}).select('name lastName role email avatar birthday sex phone emergencyPhone address postCode city status location nss curp rfc admissionDate dischargeDate ine confidentialityAgreement temporalContract permanentContract studySheet resignationLetter').populate([{
		path: 'city',
		select: 'name'
	}]).lean();
};
module.exports.putEmployee = function(req, res){
	var options = {
		_id: req.params.id,
		role: {
			$ne: 'user'
		}
	};
	if(req.user.role != 'system'){
		req.body.role = undefined;
	}
	Users.findOne(options, function(err, user){
		if(validation.find(err, user, res, 'user')){
			if(typeof req.body.name !== 'undefined')
				user.name = capitalize.words(req.body.name);
			if(typeof req.body.lastName !== 'undefined')
				user.lastName = capitalize.words(req.body.lastName);
			if(typeof req.body.role !== 'undefined')
				user.role = req.body.role;
			if(typeof req.body.password !== 'undefined')
				user.password = bcryptjs.hashSync(req.body.password, bcryptjs.genSaltSync(10));
			if(typeof req.body.birthday !== 'undefined'){
				var birthday = req.body.birthday.split("/");
				birthday = new Date(+birthday[2], +birthday[1] - 1, +birthday[0]);
				if(isNaN(birthday) == false){
					user.birthday = birthday;
				}
				else
					user.birthday = undefined;
			}
			if(typeof req.body.sex !== 'undefined'){
				if(req.body.sex != '')
					user.sex = req.body.sex;
				else
					user.sex = undefined;
			}
			if(typeof req.body.phone !== 'undefined')
				user.phone = req.body.phone.toUpperCase();
			if(typeof req.body.emergencyPhone !== 'undefined')
				user.emergencyPhone = req.body.emergencyPhone.toUpperCase();
			if(typeof req.body.address !== 'undefined')
				user.address = capitalize.words(req.body.address);
			if(typeof req.body.postCode !== 'undefined')
				user.postCode = req.body.postCode.toUpperCase();
			if(typeof req.body.status !== 'undefined')
				user.status = req.body.status;
			if(typeof req.body.location !== 'undefined')
				user.location = req.body.location;
			if(typeof req.body.nss !== 'undefined')
				user.nss = req.body.nss.toUpperCase();
			if(typeof req.body.curp !== 'undefined')
				user.curp = req.body.curp.toUpperCase();
			if(typeof req.body.rfc !== 'undefined')
				user.rfc = req.body.rfc.toUpperCase();
			if(typeof req.body.ine !== 'undefined'){
				if(req.body.ine != '')
					user.ine = req.body.ine;
				else
					user.ine = undefined;
			}
			if(typeof req.body.admissionDate !== 'undefined'){
				var admissionDate = req.body.admissionDate.split("/");
				admissionDate = new Date(+admissionDate[2], +admissionDate[1] - 1, +admissionDate[0]);
				if(isNaN(admissionDate) == false){
					user.admissionDate = admissionDate;
				}
				else
					user.admissionDate = undefined;
			}
			if(typeof req.body.dischargeDate !== 'undefined'){
				var dischargeDate = req.body.dischargeDate.split("/");
				dischargeDate = new Date(+dischargeDate[2], +dischargeDate[1] - 1, +dischargeDate[0]);
				if(isNaN(dischargeDate) == false){
					user.dischargeDate = dischargeDate;
				}
				else
					user.dischargeDate = undefined;
			}
			if(typeof req.files !== 'undefined'){
				if(typeof req.files.avatar !== 'undefined')
					user.avatar = req.files.avatar[0].filename;
				if(typeof req.files.ine !== 'undefined')
					user.ine = req.files.ine[0].filename;
				if(typeof req.files.confidentialityAgreement !== 'undefined')
					user.confidentialityAgreement = req.files.confidentialityAgreement[0].filename;
				if(typeof req.files.temporalContract !== 'undefined')
					user.temporalContract = req.files.temporalContract[0].filename;
				if(typeof req.files.permanentContract !== 'undefined')
					user.permanentContract = req.files.permanentContract[0].filename;
				if(typeof req.files.studySheet !== 'undefined')
					user.studySheet = req.files.studySheet[0].filename;
				if(typeof req.files.resignationLetter !== 'undefined')
					user.resignationLetter = req.files.resignationLetter[0].filename;
			}
			if(typeof req.body.city === 'undefined')
				req.body.city = user.city;
			if(typeof req.body.city !== 'undefined' && req.body.city == String(user.city))
				saveUser(user, res);
			else{
				Cities.findById(req.body.city, function(err, city){
					if(validation.find(err, city, res, 'city')){
						Cities.findById(user.city, function(err, cityRel){
							if(validation.find(err, cityRel, res, 'city')){
								cityRel.users.pull(user);
								cityRel.save(function(err, cityRel){
									if(validation.save(err, cityRel, res, 'city'))
										saveUserCity(user, city, res);
								});
							}
						});
					}
				});
			}
		}
	});
};
module.exports.deleteEmployee = function(req, res){
	var options = {
		_id: req.params.id,
		role: {
			$ne: 'user'
		}
	};
	Users.findOne(options, function(err, user){
		if(validation.find(err, user, res, 'user')){
			Cities.findById(user.city, function(err, cityRel){
				if(validation.canRemove(err, cityRel, res, 'city'))
					removeUser(user, res);
				else{
					cityRel.users.pull(user);
					cityRel.save(function(err, cityRel){
						if(validation.save(err, cityRel, res, 'city'))
							removeUser(user, res);
					});
				}
			});
		}
	});
};
module.exports.getUsers = function(req, res){
	var options = {
		role: 'user'
	};
	if(req.user.role != 'system'){
		options.location = req.user.location;
	}
	Users.find(options, function(err, users){
		if(validation.find(err, users, res, 'users'))
			res.status(200).jsonp(users);
	}).select('name lastName email role city status location').populate([{
		path: 'city',
		select: 'name'
	}]).sort('name lastName').lean();
};
module.exports.postUser = function(req, res){
	var user = new Users({
		role: 'user',
		verify: mongoose.Types.ObjectId(),
		verified: false,
		password: bcryptjs.hashSync(req.app.get('secret'), bcryptjs.genSaltSync(10)),
		status: 'inactive',
		location: req.user.location
	});
	if(typeof req.body.name !== 'undefined')
		user.name = capitalize.words(req.body.name);
	if(typeof req.body.lastName !== 'undefined')
		user.lastName = capitalize.words(req.body.lastName);
	if(typeof req.body.email !== 'undefined')
		user.email = req.body.email.toLowerCase();
	if(typeof req.file !== 'undefined')
		user.avatar = req.file.filename;
	if(typeof req.body.birthday !== 'undefined'){
		var birthday = req.body.birthday.split("/");
		birthday = new Date(+birthday[2], +birthday[1] - 1, +birthday[0]);
		if(isNaN(birthday) == false){
			user.birthday = birthday;
		}
		else
			user.birthday = undefined;
	}
	if(typeof req.body.sex !== 'undefined'){
		if(req.body.sex != '')
			user.sex = req.body.sex;
		else
			user.sex = undefined;
	}
	if(typeof req.body.phone !== 'undefined')
		user.phone = req.body.phone.toUpperCase();
	if(typeof req.body.emergencyPhone !== 'undefined')
		user.emergencyPhone = req.body.emergencyPhone.toUpperCase();
	if(typeof req.body.address !== 'undefined')
		user.address = capitalize.words(req.body.address);
	if(typeof req.body.postCode !== 'undefined')
		user.postCode = req.body.postCode.toUpperCase();
	if(typeof req.body.status !== 'undefined')
		user.status = req.body.status;
	if(typeof req.body.location !== 'undefined')
		user.location = req.body.location;
	Cities.findById(req.body.city, function(err, city){
		if(validation.find(err, city, res, 'city'))
			saveUserCityEmail(user, city, req, res);
	});
};
module.exports.getUser = function(req, res){
	var options = {
		_id: req.params.id,
		role: 'user'
	};
	if(req.user.role == 'user' && req.params.id != req.user._id){
		options = {
			_id: undefined
		};
	}
	Users.findOne(options, function(err, user){
		if(validation.find(err, user, res, 'user'))
			res.status(200).jsonp(user);
	}).select('name lastName role email avatar birthday sex phone emergencyPhone address postCode city status location contracts payments clinical dietary anthropometric tests photos assistance').populate([{
		path: 'city',
		select: 'name'
	}, {
		path: 'contracts.createdBy',
		select: 'name lastName'
	}, {
		path: 'payments.createdBy',
		select: 'name lastName'
	}, {
		path: 'clinical.createdBy',
		select: 'name lastName'
	}, {
		path: 'dietary.createdBy',
		select: 'name lastName'
	}, {
		path: 'anthropometric.createdBy',
		select: 'name lastName'
	}, {
		path: 'tests.createdBy',
		select: 'name lastName'
	}, {
		path: 'photos.createdBy',
		select: 'name lastName'
	}, {
		path: 'assistance.createdBy',
		select: 'name lastName'
	}]).lean();
};
module.exports.putUser = function(req, res){
	var options = {
		_id: req.params.id,
		role: 'user'
	};
	if(req.user.role == 'user' && req.params.id != req.user._id){
		options = {
			_id: undefined
		};
	}
	if(req.user.role != 'system'){
		req.body.role = undefined;
	}
	Users.findOne(options, function(err, user){
		if(validation.find(err, user, res, 'user')){
			if(typeof req.file !== 'undefined')
				user.deleteFile = user.avatar;
			if(typeof req.body.name !== 'undefined')
				user.name = capitalize.words(req.body.name);
			if(typeof req.body.lastName !== 'undefined')
				user.lastName = capitalize.words(req.body.lastName);
			if(typeof req.body.password !== 'undefined')
				user.password = bcryptjs.hashSync(req.body.password, bcryptjs.genSaltSync(10));
			if(typeof req.file !== 'undefined')
				user.avatar = req.file.filename;
			if(typeof req.body.birthday !== 'undefined'){
				var birthday = req.body.birthday.split("/");
				birthday = new Date(+birthday[2], +birthday[1] - 1, +birthday[0]);
				if(isNaN(birthday) == false){
					user.birthday = birthday;
				}
				else
					user.birthday = undefined;
			}
			if(typeof req.body.sex !== 'undefined'){
				if(req.body.sex != '')
					user.sex = req.body.sex;
				else
					user.sex = undefined;
			}
			if(typeof req.body.phone !== 'undefined')
				user.phone = req.body.phone.toUpperCase();
			if(typeof req.body.emergencyPhone !== 'undefined')
				user.emergencyPhone = req.body.emergencyPhone.toUpperCase();
			if(typeof req.body.address !== 'undefined')
				user.address = capitalize.words(req.body.address);
			if(typeof req.body.postCode !== 'undefined')
				user.postCode = req.body.postCode.toUpperCase();
			if(typeof req.body.status !== 'undefined')
				user.status = req.body.status;
			if(typeof req.body.location !== 'undefined')
				user.location = req.body.location;
			if(typeof req.body.city === 'undefined')
				req.body.city = user.city;
			if(typeof req.body.city !== 'undefined' && req.body.city == String(user.city))
				saveUser(user, res);
			else{
				Cities.findById(req.body.city, function(err, city){
					if(validation.find(err, city, res, 'city')){
						Cities.findById(user.city, function(err, cityRel){
							if(validation.find(err, cityRel, res, 'city')){
								cityRel.users.pull(user);
								cityRel.save(function(err, cityRel){
									if(validation.save(err, cityRel, res, 'city'))
										saveUserCity(user, city, res);
								});
							}
						});
					}
				});
			}
		}
	});
};
module.exports.deleteUser = function(req, res){
	var options = {
		_id: req.params.id,
		role: 'user'
	};
	Users.findOne(options, function(err, user){
		if(validation.find(err, user, res, 'user')){
			Cities.findById(user.city, function(err, cityRel){
				if(validation.canRemove(err, cityRel, res, 'city'))
					removeUser(user, res);
				else{
					cityRel.users.pull(user);
					cityRel.save(function(err, cityRel){
						if(validation.save(err, cityRel, res, 'city'))
							removeUser(user, res);
					});
				}
			});
		}
	});
};
module.exports.postUserContracts = function(req, res){
	var options = {
		_id: req.params.id,
		role: 'user'
	};
	if(req.user.role != 'system'){
		req.body.role = undefined;
	}
	Users.findOne(options, function(err, user){
		if(validation.find(err, user, res, 'user')){
			var createdAt = new Date();
			var contracts = {};
			if(typeof req.body.contracts !== 'undefined')
				contracts = req.body.contracts;
			contracts.createdBy = req.user._id;
			contracts.createdAt = createdAt;
			user.contracts.push(contracts);
			contracts._id = user.contracts[user.contracts.length - 1]._id;
			contracts.createdBy = req.user.name + ' ' + req.user.lastName;
			saveUserPartial(user, contracts, res);
		}
	});
};
module.exports.postUserPayments = function(req, res){
	var options = {
		_id: req.params.id,
		role: 'user'
	};
	if(req.user.role != 'system'){
		req.body.role = undefined;
	}
	Users.findOne(options, function(err, user){
		if(validation.find(err, user, res, 'user')){
			var createdAt = new Date();
			var payment = {};
			if(typeof req.body.payment !== 'undefined')
				payment = req.body.payment;
			payment.createdBy = req.user._id;
			payment.createdAt = createdAt;
			user.payment.push(payment);
			payment._id = user.payment[user.payment.length - 1]._id;
			payment.createdBy = req.user.name + ' ' + req.user.lastName;
			saveUserPartial(user, payment, res);
		}
	});
};
module.exports.postUserClinical = function(req, res){
	var options = {
		_id: req.params.id,
		role: 'user'
	};
	if(req.user.role != 'system'){
		req.body.role = undefined;
	}
	Users.findOne(options, function(err, user){
		if(validation.find(err, user, res, 'user')){
			var createdAt = new Date();
			var clinical = {};
			if(typeof req.body.clinical !== 'undefined')
				clinical = req.body.clinical;
			clinical.createdBy = req.user._id;
			clinical.createdAt = createdAt;
			user.clinical.push(clinical);
			clinical._id = user.clinical[user.clinical.length - 1]._id;
			clinical.createdBy = req.user.name + ' ' + req.user.lastName;
			saveUserPartial(user, clinical, res);
		}
	});
};
module.exports.postUserDietary = function(req, res){
	var options = {
		_id: req.params.id,
		role: 'user'
	};
	if(req.user.role != 'system'){
		req.body.role = undefined;
	}
	Users.findOne(options, function(err, user){
		if(validation.find(err, user, res, 'user')){
			var createdAt = new Date();
			var dietary = {};
			if(typeof req.body.dietary !== 'undefined')
				dietary = req.body.dietary;
			dietary.createdBy = req.user._id;
			dietary.createdAt = createdAt;
			user.dietary.push(dietary);
			dietary._id = user.dietary[user.dietary.length - 1]._id;
			dietary.createdBy = req.user.name + ' ' + req.user.lastName;
			saveUserPartial(user, dietary, res);
		}
	});
};
module.exports.postUserAnthropometric = function(req, res){
	var options = {
		_id: req.params.id,
		role: 'user'
	};
	if(req.user.role != 'system'){
		req.body.role = undefined;
	}
	Users.findOne(options, function(err, user){
		if(validation.find(err, user, res, 'user')){
			var createdAt = new Date();
			var anthropometric = {};
			if(typeof req.body.anthropometric !== 'undefined')
				anthropometric = req.body.anthropometric;
			anthropometric.createdBy = req.user._id;
			anthropometric.createdAt = createdAt;
			user.anthropometric.push(anthropometric);
			anthropometric._id = user.anthropometric[user.anthropometric.length - 1]._id;
			anthropometric.createdBy = req.user.name + ' ' + req.user.lastName;
			saveUserPartial(user, anthropometric, res);
		}
	});
};
module.exports.postUserTests = function(req, res){
	var options = {
		_id: req.params.id,
		role: 'user'
	};
	if(req.user.role != 'system'){
		req.body.role = undefined;
	}
	Users.findOne(options, function(err, user){
		if(validation.find(err, user, res, 'user')){
			var createdAt = new Date();
			var tests = {};
			if(typeof req.body.tests !== 'undefined')
				tests = req.body.tests;
			tests.createdBy = req.user._id;
			tests.createdAt = createdAt;
			user.tests.push(tests);
			tests._id = user.tests[user.tests.length - 1]._id;
			tests.createdBy = req.user.name + ' ' + req.user.lastName;
			saveUserPartial(user, tests, res);
		}
	});
};
module.exports.postUserPhotos = function(req, res){
	var options = {
		_id: req.params.id,
		role: 'user'
	};
	if(req.user.role != 'system'){
		req.body.role = undefined;
	}
	Users.findOne(options, function(err, user){
		if(validation.find(err, user, res, 'user')){
			var createdAt = new Date();
			var photos = {};
			if(typeof req.files !== 'undefined'){
				photos.front = req.files.front[0].filename;
				photos.back = req.files.back[0].filename;
			}
			photos.createdBy = req.user._id;
			photos.createdAt = createdAt;
			user.photos.push(photos);
			photos._id = user.photos[user.photos.length - 1]._id;
			photos.createdBy = req.user.name + ' ' + req.user.lastName;
			saveUserPartial(user, photos, res);
		}
	});
};
// Public users controllers.
exports.getPublicUserLogin = function(req, res){
	var email, password;
	if(typeof req.query.email !== 'undefined')
		email = req.query.email.toLowerCase();
	if(typeof req.query.password !== 'undefined')
		password = req.query.password;
	Users.findOne({
		email: email
	}, function(err, user){
		if(validation.find(err, user, res, 'publicUser')){
			if(user.verified == true){
				bcryptjs.compare(password, user.password, function(err, match){
					if(validation.find(err, match, res, 'publicUser')){
						var userToken = jsonWebToken.sign({'email': user.email}, req.app.get('secret'), {
							expiresIn: 36000
						});
						res.clearCookie('userToken', {path: '/'});
						res.cookie('userToken', userToken, {path: '/', maxAge: 36000000});
						res.status(200).jsonp({'message': languages.strings(language, 'welcomeToBrand')});
					}
				});
			}
			else
				res.status(500).jsonp({'message': languages.strings(language, 'verifyYourAccountToLogin')});
		}
	}).lean();
};
module.exports.getPublicUsersVerify = function(req, res){
	Users.findOne({
		verify: mongoose.Types.ObjectId(req.params.verify)
	}, function(err, user){
		if(validation.find(err, user, res, 'publicVerify'))
			res.status(200).jsonp(user);
	}).select('-_id verify verified').lean();
}
module.exports.putPublicUsersVerify = function(req, res){
	if(req.params.verify){
		var password;
		if(typeof req.body.password !== 'undefined')
			password = bcryptjs.hashSync(req.body.password, bcryptjs.genSaltSync(10));
		Users.findOne({
			verify: mongoose.Types.ObjectId(req.params.verify)
		}, function(err, user){
			if(validation.find(err, user, res, 'publicVerify')){
				if(user.verified == false){
					user.password = password;
					user.verify = undefined;
					user.verified = true;
					user.save(function(err, user){
						if(validation.save(err, user, res, 'publicUser'))
							res.status(200).jsonp({'message': languages.strings(language, 'thanksForVerifyingYourAccountPleaseLogin')});
					});
				}
				else
					res.status(200).jsonp({'message': languages.strings(language, 'yourAccountHasAlreadyBeenVerifiedPleaseLogin')});
			}
		});
	}
	else if(req.body.email){
		var email;
		if(typeof req.body.email !== 'undefined')
			email = req.body.email.toLowerCase();
		Users.findOne({
			email: email
		}, function(err, user){
			if(validation.find(err, user, res, 'publicVerify')){
				if(user.verified == false){
					emails.sendVerifyEmail(user, req);
					res.status(200).jsonp({'message': languages.strings(language, 'pleaseCheckYourEmailWeHaveSentYouAMessage')});
				}
				else
					res.status(200).jsonp({'message': languages.strings(language, 'yourAccountHasAlreadyBeenVerifiedPleaseLogin')});
			}
		});
	}
}
module.exports.getPublicUsersForgot = function(req, res){
	Users.findOne({
		forgot: mongoose.Types.ObjectId(req.params.forgot)
	}, function(err, user){
		if(validation.find(err, user, res, 'publicForgot'))
			res.status(200).jsonp(user);
	}).select('-_id forgot').lean();
}
module.exports.putPublicUsersForgot = function(req, res){
	if(req.params.forgot){
		var password;
		if(typeof req.body.password !== 'undefined')
			password = bcryptjs.hashSync(req.body.password, bcryptjs.genSaltSync(10));
		Users.findOne({
			forgot: mongoose.Types.ObjectId(req.params.forgot)
		}, function(err, user){
			if(validation.find(err, user, res, 'publicForgot')){
				user.password = password;
				user.forgot = undefined;
				user.save(function(err, user){
					if(validation.save(err, user, res, 'publicUser'))
						res.status(200).jsonp({'message': languages.strings(language, 'yourPasswordHasBeenRestoredPleaseLogin')});
				});
			}
		});
	}
	else if(req.body.email){
		var email;
		if(typeof req.body.email !== 'undefined')
			email = req.body.email.toLowerCase();
		Users.findOne({
			email: email
		}, function(err, user){
			if(validation.find(err, user, res, 'publicForgot')){
				user.forgot = mongoose.Types.ObjectId();
				user.save(function(err, user){
					if(validation.save(err, user, res, 'publicUser')){
						emails.sendForgotEmail(user, req);
						res.status(200).jsonp({'message': languages.strings(language, 'pleaseCheckYourEmailWeHaveSentYouAMessage')});
					}
				});
			}
		});
	}
}
module.exports.putPublicUsersAssists = function(req, res){
	var options = {
		_id: req.body.assistance,
		role: 'user'
	};
	Users.findOne(options, function(err, user){
		if(validation.find(err, user, res, 'user')){
			var createdAt = new Date();
			var lastDay = new Date();
			lastDay.setDate(createdAt.getDate() - 1);
			if(user.assistance.length == 0 || user.assistance[user.assistance.length - 1].createdAt <= lastDay){
				var assistance = {};
				assistance.createdAt = createdAt;
				assistance.arrival = createdAt;
				if(user.assistance.length > 0 && typeof user.assistance[user.assistance.length - 1].departure === 'undefined')
					user.assistance[user.assistance.length - 1].departure = createdAt;
				user.assistance.push(assistance);
				saveUserMessage(user, {'message': languages.strings(language, 'welcomeToBrand')}, res);
			}
			else{
				if(typeof user.assistance[user.assistance.length - 1].departure === 'undefined'){
					user.assistance[user.assistance.length - 1].departure = createdAt;
					saveUserMessage(user, {'message': languages.strings(language, 'thanksForVisitingBrandComeBackSoon')}, res);
				}
				else
					res.status(401).jsonp({'message': languages.strings(language, 'accessDeniedAssistanceAlreadyRegistered')});
			}
		}
	});
}
module.exports.getPublicUsersLogout = function(req, res){
	res.clearCookie('userToken', {path: '/'});
	res.status(200).jsonp({'message': languages.strings(language, 'thanksForVisitingBrandComeBackSoon')});
};
module.exports.getPublicUsersSetup = function(req, res){
	console.log('1');
	Users.find(function(err, users){
		console.log('2');
		console.log(err);
		if(validation.find(err, users, res, 'users')){
			if(users.length > 0)
				res.status(401).jsonp({'message': languages.strings(language, 'accessDeniedPleaseLogin')});
			else{
				console.log('3');
				var state = new States();
				state.name = capitalize.words('Baja California');
				state.save(function(err, state){
					console.log('4');
					if(validation.save(err, state, res, 'state')){
						var city = new Cities();
						city.name = capitalize.words('Tijuana');
						city.state = state._id;
						city.save(function(err, city){
							console.log('5');
							if(validation.save(err, city, res, 'city')){
								state.cities.push(city);
								state.save(function(err, state){
									console.log('6');
									if(validation.save(err, state, res, 'state')){
										var user = new Users({
											role: 'system',
											verify: mongoose.Types.ObjectId(),
											verified: true
										});
										user.name = capitalize.words('Allan');
										user.lastName = capitalize.words('Corona');
										user.email = 'sparta55mx@outlook.com'.toLowerCase();
										user.password = bcryptjs.hashSync('Sp1.2.3.4.5.$', bcryptjs.genSaltSync(10));
										saveUserCityEmail(user, city, req, res);
									}
								});
							}
						});
					}
				});
			}
		}
	}).lean();
};

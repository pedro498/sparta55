$(function(){
	var element = 'generations';
	initialize_(element);
});
function save_(element){
	disabled_();
	var error = '';
	var _id = $('#form #_id').val(),
		name = $('#form #name').val(),
		startDate = $('#form #startDate').val(),
		finishDate = $('#form #finishDate').val(),
		price = $('#form #price').val(),
		location = $('#form #location option:selected').val(),
		category = $('#form #category option:selected').val();
	if(name == ''){
		$('#name_').addClass('error');
		error += '<li>' + language['pleaseEnterTheName'] + '</li>';
	}
	if(startDate == ''){
		$('#startDate_').addClass('error');
		error += '<li>' + language['pleaseEnterTheStartDate'] + '</li>';
	}
	if(finishDate == ''){
		$('#finishDate_').addClass('error');
		error += '<li>' + language['pleaseEnterTheFinishDate'] + '</li>';
	}
	if(price <= 0){
		$('#price_').addClass('error');
		error += '<li>' + language['pleaseEnterThePrice'] + '</li>';
	}
	if(location.length == 0){
		$('#location_').addClass('error');
		error += '<li>' + language['pleaseSelectTheLocation'] + '</li>';
	}
	if(category.length == 0){
		$('#category_').addClass('error');
		error += '<li>' + language['pleaseSelectTheCategory'] + '</li>';
	}
	if(error == ''){
		var data = {
			name: name,
			startDate: startDate,
			finishDate: finishDate,
			price: price,
			location: location,
			category: category
		};
		saveRequest_(element, data);
	}
	else
		error_(error);
}

// Modules declarations.
var mongoose = require('mongoose'),
	validation = require('../../libraries/validation.js'),
	languages = require('../../libraries/languages.js'),
	language = 'es',
	capitalize = require('capitalize'),
	Generations = mongoose.model('Generations'),
	Locations = mongoose.model('Locations'),
	Categories = mongoose.model('Categories');
// Generations controllers.
function saveGeneration(generation, res){
	generation.save(function(err, generation){
		if(validation.save(err, generation, res, 'generation'))
			res.status(200).jsonp(generation);
	});
}
function saveGenerationLocation(generation, location, res){
	generation.location = location._id;
	generation.save(function(err, generation){
		if(validation.save(err, generation, res, 'generation')){
			location.generations.push(generation);
			location.save(function(err, location){
				if(validation.save(err, location, res, 'location'))
					res.status(200).jsonp(generation);
			});
		}
	});
}
function saveGenerationCategory(generation, category, res){
	generation.category = category._id;
	generation.save(function(err, generation){
		if(validation.save(err, generation, res, 'generation')){
			category.generations.push(generation);
			category.save(function(err, category){
				if(validation.save(err, category, res, 'category'))
					res.status(200).jsonp(generation);
			});
		}
	});
}
function saveGenerationLocationCategory(generation, location, category, res){
	generation.location = location._id;
	generation.category = category._id;
	generation.save(function(err, generation){
		if(validation.save(err, generation, res, 'generation')){
			location.generations.push(generation);
			location.save(function(err, location){
				if(validation.save(err, location, res, 'location')){
					category.generations.push(generation);
					category.save(function(err, category){
						if(validation.save(err, category, res, 'category'))
							res.status(200).jsonp(generation);
					});
				}
			});
		}
	});
}
function removeGeneration(generation, res){
	generation.remove(function(err){
		validation.remove(err, generation, res, 'generation');
	});
}
module.exports.getGenerations = function(req, res){
	Generations.find(function(err, generations){
		if(validation.find(err, generations, res, 'generations'))
			res.status(200).jsonp(generations);
	}).select('name startDate finishDate price location category users').populate([{
		path: 'location',
		select: 'name'
	}, {
		path: 'category',
		select: 'name'
	}]).lean();
};
module.exports.postGeneration = function(req, res){
	var generation = new Generations();
	if(typeof req.body.name !== 'undefined')
		generation.name = capitalize.words(req.body.name);
	if(typeof req.body.startDate !== 'undefined'){
		var startDate = req.body.startDate.split("/");
		startDate = new Date(+startDate[2], +startDate[1] - 1, +startDate[0]);
		if(isNaN(startDate) == false){
			generation.startDate = startDate;
		}
		else
			generation.startDate = undefined;
	}
	if(typeof req.body.finishDate !== 'undefined'){
		var finishDate = req.body.finishDate.split("/");
		finishDate = new Date(+finishDate[2], +finishDate[1] - 1, +finishDate[0]);
		if(isNaN(finishDate) == false){
			generation.finishDate = finishDate;
		}
		else
			generation.finishDate = undefined;
	}
	if(typeof req.body.price !== 'undefined')
		generation.price = req.body.price.toUpperCase();
	Locations.findById(req.body.location, function(err, location){
		if(validation.find(err, location, res, 'location')){
			Categories.findById(req.body.category, function(err, category){
				if(validation.find(err, category, res, 'category'))
					saveGenerationLocationCategory(generation, location, category, res);
			});

		}
	});
};
module.exports.getGeneration = function(req, res){
	Generations.findById(req.params.id, function(err, generation){
		if(validation.find(err, generation, res, 'generation'))
			res.status(200).jsonp(generation);
	}).select('name startDate finishDate price location category users').populate([{
		path: 'location',
		select: 'name'
	}, {
		path: 'category',
		select: 'name'
	}, {
		path: 'users',
		select: 'name lastName',
		options: {
			sort: {
				'name': 1,
				'lastName': 1
			}
		}
	}]).lean();
};
module.exports.putGeneration = function(req, res){
	Generations.findById(req.params.id, function(err, generation){
		if(validation.find(err, generation, res, 'generation')){
			if(typeof req.body.name !== 'undefined')
				generation.name = capitalize.words(req.body.name);
			if(typeof req.body.startDate !== 'undefined'){
				var startDate = req.body.startDate.split("/");
				startDate = new Date(+startDate[2], +startDate[1] - 1, +startDate[0]);
				if(isNaN(startDate) == false){
					generation.startDate = startDate;
				}
				else
					generation.startDate = undefined;
			}
			if(typeof req.body.finishDate !== 'undefined'){
				var finishDate = req.body.finishDate.split("/");
				finishDate = new Date(+finishDate[2], +finishDate[1] - 1, +finishDate[0]);
				if(isNaN(finishDate) == false){
					generation.finishDate = finishDate;
				}
				else
					generation.finishDate = undefined;
			}
			if(typeof req.body.price !== 'undefined')
				generation.price = req.body.price.toUpperCase();
			if(typeof req.body.location === 'undefined')
				req.body.location = generation.location;
			if(typeof req.body.category === 'undefined')
				req.body.category = generation.category;
			if((typeof req.body.location !== 'undefined' && req.body.location == String(generation.location)) && (typeof req.body.category !== 'undefined' && req.body.category == String(generation.category)))
				saveGeneration(generation, res);
			else{
				if((typeof req.body.location !== 'undefined' && req.body.location != String(generation.location)) && (typeof req.body.category !== 'undefined' && req.body.category != String(generation.category))){
					Locations.findById(req.body.location, function(err, location){
						if(validation.find(err, location, res, 'location')){
							Locations.findById(generation.location, function(err, locationRel){
								if(validation.find(err, locationRel, res, 'location')){
									locationRel.generations.pull(generation);
									locationRel.save(function(err, locationRel){
										if(validation.save(err, locationRel, res, 'location')){
											Categories.findById(req.body.category, function(err, category){
												if(validation.find(err, category, res, 'category')){
													Categories.findById(generation.category, function(err, categoryRel){
														if(validation.find(err, categoryRel, res, 'category')){
															categoryRel.generations.pull(generation);
															categoryRel.save(function(err, categoryRel){
																if(validation.save(err, categoryRel, res, 'category'))
																	saveGenerationCategory(generation, category, res);
															});
														}
													});
												}
											});
										}
									});
								}
							});
						}
					});
				}
				else if(typeof req.body.location !== 'undefined' && req.body.location != String(generation.location)){
					Locations.findById(req.body.location, function(err, location){
						if(validation.find(err, location, res, 'location')){
							Locations.findById(generation.location, function(err, locationRel){
								if(validation.find(err, locationRel, res, 'location')){
									locationRel.generations.pull(generation);
									locationRel.save(function(err, locationRel){
										if(validation.save(err, locationRel, res, 'location'))
											saveGenerationLocation(generation, location, res);
									});
								}
							});
						}
					});
				}
				else if(typeof req.body.category !== 'undefined' && req.body.category != String(generation.category)){
					Categories.findById(req.body.category, function(err, category){
						if(validation.find(err, category, res, 'category')){
							Categories.findById(generation.category, function(err, categoryRel){
								if(validation.find(err, categoryRel, res, 'category')){
									categoryRel.generations.pull(generation);
									categoryRel.save(function(err, categoryRel){
										if(validation.save(err, categoryRel, res, 'category'))
											saveGenerationCategory(generation, category, res);
									});
								}
							});
						}
					});
				}
			}
		}
	});
};
module.exports.deleteGeneration = function(req, res){
	Generations.findById(req.params.id, function(err, generation){
		if(validation.find(err, generation, res, 'generation')){
			if(generation.users.length > 0)
				validation.notRemove(err, generation, res, 'generation');
			else{
				Locations.findById(generation.location, function(err, locationRel){
					if(validation.canRemove(err, locationRel, res, 'location')){
						Categories.findById(generation.category, function(err, categoryRel){
							if(validation.canRemove(err, categoryRel, res, 'category'))
								removeGeneration(generation, res);
							else{
								categoryRel.generations.pull(generation);
								categoryRel.save(function(err, categoryRel){
									if(validation.save(err, categoryRel, res, 'category'))
										removeGeneration(generation, res);
								});
							}
						});
					}
					else{
						locationRel.generations.pull(generation);
						locationRel.save(function(err, locationRel){
							if(validation.save(err, locationRel, res, 'location')){
								Categories.findById(generation.category, function(err, categoryRel){
									if(validation.canRemove(err, categoryRel, res, 'category'))
										removeGeneration(generation, res);
									else{
										categoryRel.generations.pull(generation);
										categoryRel.save(function(err, categoryRel){
											if(validation.save(err, categoryRel, res, 'category'))
												removeGeneration(generation, res);
										});
									}
								});
							}
						});
					}
				});
			}
		}
	});
};
// Public generations controllers.
module.exports.getPublicGenerations = function(req, res){
	Generations.find(function(err, generations){
		if(validation.find(err, generations, res, 'generations'))
			res.status(200).jsonp(generations);
	}).select('name price').lean();
};

// Modules declarations.
var mongoose = require('mongoose'),
	validation = require('../../libraries/validation.js'),
	languages = require('../../libraries/languages.js'),
	language = 'es',
	capitalize = require('capitalize'),
	Cities = mongoose.model('Cities'),
	States = mongoose.model('States');
// Cities controllers.
function saveCity(city, res){
	city.save(function(err, city){
		if(validation.save(err, city, res, 'city'))
			res.status(200).jsonp(city);
	});
}
function saveCityState(city, state, res){
	city.state = state._id;
	city.save(function(err, city){
		if(validation.save(err, city, res, 'city')){
			state.cities.push(city);
			state.save(function(err, state){
				if(validation.save(err, state, res, 'state'))
					res.status(200).jsonp(city);
			});
		}
	});
}
function removeCity(city, res){
	city.remove(function(err){
		validation.remove(err, city, res, 'city');
	});
}
module.exports.getCities = function(req, res){
	Cities.find(function(err, cities){
		if(validation.find(err, cities, res, 'cities'))
			res.status(200).jsonp(cities);
	}).select('name state users locations').populate([{
		path: 'state',
		select: 'name'
	}]).sort('name').lean();
};
module.exports.postCity = function(req, res){
	var city = new Cities();
	if(typeof req.body.name !== 'undefined')
		city.name = capitalize.words(req.body.name);
	States.findById(req.body.state, function(err, state){
		if(validation.find(err, state, res, 'state'))
			saveCityState(city, state, res);
	});
};
module.exports.getCity = function(req, res){
	Cities.findById(req.params.id, function(err, city){
		if(validation.find(err, city, res, 'city'))
			res.status(200).jsonp(city);
	}).select('name state users locations').populate([{
		path: 'state',
		select: 'name'
	}, {
		path: 'users',
		select: 'name lastName role',
		options: {
			sort: {
				'name': 1,
				'lastName': 1
			}
		}
	}, {
		path: 'locations',
		select: 'name',
		options: {
			sort: {
				'name': 1
			}
		}
	}]).lean();
};
module.exports.putCity = function(req, res){
	Cities.findById(req.params.id, function(err, city){
		if(validation.find(err, city, res, 'city')){
			if(typeof req.body.name !== 'undefined')
				city.name = capitalize.words(req.body.name);
			if(typeof req.body.state === 'undefined')
				req.body.state = city.state;
			if(typeof req.body.state !== 'undefined' && req.body.state == String(city.state))
				saveCity(city, res);
			else{
				States.findById(req.body.state, function(err, state){
					if(validation.find(err, state, res, 'state')){
						States.findById(city.state, function(err, stateRel){
							if(validation.find(err, stateRel, res, 'state')){
								stateRel.cities.pull(city);
								stateRel.save(function(err, stateRel){
									if(validation.save(err, stateRel, res, 'state'))
										saveCityState(city, state, res);
								});
							}
						});
					}
				});
			}
		}
	});
};
module.exports.deleteCity = function(req, res){
	Cities.findById(req.params.id, function(err, city){
		if(validation.find(err, city, res, 'city')){
			if(city.users.length > 0)
				validation.notRemove(err, city, res, 'city');
			else if(city.locations.length > 0)
				validation.notRemove(err, city, res, 'city');
			else{
				States.findById(city.state, function(err, stateRel){
					if(validation.canRemove(err, stateRel, res, 'state'))
						removeCity(city, res);
					else{
						stateRel.cities.pull(city);
						stateRel.save(function(err, stateRel){
							if(validation.save(err, stateRel, res, 'state'))
								removeCity(city, res);
						});
					}
				});
			}
		}
	});
};
// Public cities controllers.
module.exports.getPublicCities = function(req, res){
	Cities.find(function(err, cities){
		if(validation.find(err, cities, res, 'cities'))
			res.status(200).jsonp(cities);
	}).select('name').sort('name').lean();
};

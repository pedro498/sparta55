// Strings declarations.
module.exports.strings = function(language, key){
	if(language == 'es')
		return es[key];
	else
		return es[key];
}
module.exports.strings_ = function(language, key){
	if(key == 'usersNotFound')
		key = this.strings(language, 'noUsersFound');
	else if(key == 'userNotFound')
		key = this.strings(language, 'theUserWasNotFound');
	else if(key == 'userNotSaved')
		key = this.strings(language, 'theUserWasNotSaved');
	else if(key == 'userAlreadyExists')
		key = this.strings(language, 'theUserAlreadyExists');
	else if(key == 'userDeleted')
		key = this.strings(language, 'theUserWasDeleted');
	else if(key == 'userNotDeleted')
		key = this.strings(language, 'theUserCanNotBeDeleted');
	// Users public strings.
	else if(key == 'publicUserNotFound')
		key = this.strings(language, 'theEmailOrPasswordEnteredAreInvalid');
	else if(key == 'publicUserNotSaved')
		key = this.strings(language, 'thereWasAnErrorPleaseTryLater');
	else if(key == 'publicUserAlreadyExists')
		key = this.strings(language, 'theEmailEnteredIsAlreadyAssociatedWithAnAccount');
	else if(key == 'publicCityNotFound')
		key = this.strings(language, 'theCitySelectedIsInvalid');
	else if(key == 'publicCityNotSaved')
		key = this.strings(language, 'thereWasAnErrorPleaseTryLater');
	else if(key == 'publicCityAlreadyExists')
		key = this.strings(language, 'thereWasAnErrorPleaseTryLater');
	else if(key == 'publicVerifyNotFound')
		key = this.strings(language, 'theAccountEnteredWasNotFound');
	else if(key == 'publicForgotNotFound')
		key = this.strings(language, 'theAccountEnteredWasNotFound');
	// States strings.
	else if(key == 'statesNotFound')
		key = this.strings(language, 'noStatesFound');
	else if(key == 'stateNotFound')
		key = this.strings(language, 'theStateWasNotFound');
	else if(key == 'stateNotSaved')
		key = this.strings(language, 'theStateWasNotSaved');
	else if(key == 'stateAlreadyExists')
		key = this.strings(language, 'theStateAlreadyExists');
	else if(key == 'stateDeleted')
		key = this.strings(language, 'theStateWasDeleted');
	else if(key == 'stateNotDeleted')
		key = this.strings(language, 'theStateCanNotBeDeleted');
	// Cities strings.
	else if(key == 'citiesNotFound')
		key = this.strings(language, 'noCitiesFound');
	else if(key == 'cityNotFound')
		key = this.strings(language, 'theCityWasNotFound');
	else if(key == 'cityNotSaved')
		key = this.strings(language, 'theCityWasNotSaved');
	else if(key == 'cityAlreadyExists')
		key = this.strings(language, 'theCityAlreadyExists');
	else if(key == 'cityDeleted')
		key = this.strings(language, 'theCityWasDeleted');
	else if(key == 'cityNotDeleted')
		key = this.strings(language, 'theCityCanNotBeDeleted');
	// Locations strings.
	else if(key == 'locationsNotFound')
		key = this.strings(language, 'noLocationsFound');
	else if(key == 'locationNotFound')
		key = this.strings(language, 'theLocationWasNotFound');
	else if(key == 'locationNotSaved')
		key = this.strings(language, 'theLocationWasNotSaved');
	else if(key == 'locationAlreadyExists')
		key = this.strings(language, 'theLocationAlreadyExists');
	else if(key == 'locationDeleted')
		key = this.strings(language, 'theLocationWasDeleted');
	else if(key == 'locationNotDeleted')
		key = this.strings(language, 'theLocationCanNotBeDeleted');
	// Categories strings.
	else if(key == 'categoriesNotFound')
		key = this.strings(language, 'noCateogiresFound');
	else if(key == 'categoryNotFound')
		key = this.strings(language, 'theCategoryWasNotFound');
	else if(key == 'categoryNotSaved')
		key = this.strings(language, 'theCategoryWasNotSaved');
	else if(key == 'categoryAlreadyExists')
		key = this.strings(language, 'theCategoryAlreadyExists');
	else if(key == 'categoryDeleted')
		key = this.strings(language, 'theCategoryWasDeleted');
	else if(key == 'categoryNotDeleted')
		key = this.strings(language, 'theCategoryCanNotBeDeleted');
	// Classes strings.
	else if(key == 'classesNotFound')
		key = this.strings(language, 'noClassesFound');
	else if(key == 'classeNotFound')
		key = this.strings(language, 'theClasseWasNotFound');
	else if(key == 'classeNotSaved')
		key = this.strings(language, 'theClasseWasNotSaved');
	else if(key == 'classeAlreadyExists')
		key = this.strings(language, 'theClasseAlreadyExists');
	else if(key == 'classeDeleted')
		key = this.strings(language, 'theClasseWasDeleted');
	else if(key == 'classeNotDeleted')
		key = this.strings(language, 'theClasseCanNotBeDeleted');
	// Generations strings.
	else if(key == 'classesNotFound')
		key = this.strings(language, 'noGenerationsFound');
	else if(key == 'classeNotFound')
		key = this.strings(language, 'theGenerationWasNotFound');
	else if(key == 'classeNotSaved')
		key = this.strings(language, 'theGenerationWasNotSaved');
	else if(key == 'classeAlreadyExists')
		key = this.strings(language, 'theGenerationAlreadyExists');
	else if(key == 'classeDeleted')
		key = this.strings(language, 'theGenerationWasDeleted');
	else if(key == 'classeNotDeleted')
		key = this.strings(language, 'theGenerationCanNotBeDeleted');
	else
		key = '';
	return key;
}
// Spanish strings.
var brand = 'Sparta55';
var es = {
	// General strings.
	brand: brand,
	// Secure views titles strings.
	login: 'Iniciar sesión',
	verify: 'Verificar cuenta',
	forgot: 'Restaurar contraseña',
	welcome: 'Bienvenido',
	profile: 'Perfil',
	logout: 'Cerrar sesión',
	// Secure views content strings.
	loginToUseOurServices: 'Inicia sesión para utilizar nuestra aplicación',
	enterYourEmailToVerifyYourAccount: 'Ingresa tu email para verificar tu cuenta',
	verifyYourAccountToLogin: 'Verifica tu cuenta para iniciar sesión',
	enterAPasswordToVerifyYourAccount: 'Ingresa una contraseña para verificar tu cuenta',
	enterYourEmailToResetYourPassword: 'Ingresa tu email para restaurar tu contraseña',
	enterYourNewPassword: 'Ingresa tu nueva contraseña',
	enterYourIDToRegisterYourArrivalOrDeparture: 'Ingresa tu ID para registrar tu entrada ó sálida',
	notHaveAnAccount: '¿No tienes una cuenta?',
	forgotYourPassword: '¿Olvidaste tu contraseña?',
	agreement: 'Al hacer clic en crear una cuenta, aceptas los términos y condiciones y la política de privacidad de ' + brand,
	// Public views content strings.
	comingSoon: 'Próximamente',
	// Admin views titles strings.
	employees: 'Personal',
	users: 'Alumnos',
	states: 'Estados',
	cities: 'Ciudades',
	locations: 'Academias',
	categories: 'Categorías',
	classes: 'Clases',
	generations: 'Generaciones',
	reports: 'Reportes',
	navigation: 'Navegación',
	configuration: 'Configuración',
	editProfile: 'Editar perfil',
	actions: 'Acciones',
	undefined: 'Indefenido',
	administration: 'Administración',
	// Dividing titles strings.
	general: 'General',
	aditional: 'Adicional',
	contracts: 'Contratos',
	clinical: 'Clínicos',
	dietary: 'Dietéticos',
	anthropometric: 'Antropométricos',
	tests: 'Pruebas',
	photos: 'Fotos',
	assistance: 'Asistencias',
	payments: 'Pagos',
	schedules: 'Citas',
	labor: 'Laboral',
	generalInfo: 'Información general',
	contactInfo: 'Información de contacto',
	laborInfo: 'Información laboral',
	billingInfo: 'Información de pago',
	aditionalInfo: 'Información adicional',
	// Admin views menu fields strings.
	menu: 'Menú',
	filter: 'Filtrar',
	filterDateFrom: 'Filtrar desde',
	filterDateTo: 'Filtrar hasta',
	// Admin views users fields strings.
	id: 'ID',
	name: 'Nombre',
	lastName: 'Apellido',
	role: 'Rol',
	system: 'Sistema',
	admin: 'Admin',
	headCoach: 'Head Coach',
	mentalTraining: 'Mental Training',
	coach: 'Coach',
	nutrologist: 'Nutriólogo',
	receptionist: 'Recepcionista',
	user: 'Alumno',
	email: 'Email',
	password: 'Contraseña',
	enterNewPassword: 'Ingresa una nueva contraseña',
	avatar: 'Avatar',
	addNewAvatar: 'Agregar nuevo avatar',
	barcode: 'Código de barras',
	print: 'Imprimir',
	birthday: 'Fecha de nacimiento',
	sex: 'Sexo',
	male: 'Masculino',
	female: 'Femenino',
	phone: 'Teléfono',
	emergencyPhone: 'Teléfono de emergencia',
	address: 'Dirección',
	postCode: 'Código postal',
	city: 'Ciudad',
	nss: 'NSS',
	curp: 'CURP',
	rfc: 'RFC',
	status: 'Estatus',
	active: 'Activo',
	inactive: 'Inactivo',
	pending: 'Pendiente',
	admissionDate: 'Fecha de alta',
	dischargeDate: 'Fecha de baja',
	ine: 'INE',
	addNewIne: 'Agregar nuevo INE',
	confidentialityAgreement: 'Contrato de confidencialidad',
	addNewConfidentialityAgreement: 'Agregar nuevo contrato de confidencialidad',
	temporalContract: 'Contrato temporal',
	addNewTemporalContract: 'Agregar nuevo contrato temporal',
	permanentContract: 'Contrato permanente',
	addNewPermanentContract: 'Agregar nuevo contrato permanente',
	studySheet: 'Comprobante de estudios',
	addNewStudySheet: 'Agregar nuevo comprobante de estudios',
	resignationLetter: 'Carta de renuncia',
	addNewResignationLetter: 'Agregar nuevo carta de renuncia',
	createdAt: 'Creado',
	createdBy: 'Creado por',
	trainingPlan: 'Plan de entrenamiento',
	t20days: 'Mensual',
	t55days: 'Bimestral',
	paymentMethod: 'Método de pago',
	cash: 'Efectivo',
	voucher: 'Tarjeta',
	paymentQuantity: 'Cantidad de pagos',
	1: '1',
	2: '2',
	sugestedPrice: 'Precio sugerido',
	startDate: 'Fecha Inicial',
	contract: 'Contrato',
	paymentDate: 'Fecha de pago',
	diagnosis: 'Diagnósticos',
	medicines: 'Medicamentos',
	hoursOfSleep: 'Horas de sueño',
	familyHistory: 'Antecedentes familiares',
	obesity: 'Obesidad',
	diabetes: 'Diabetes',
	hta: 'HTA',
	cancer: 'Cancer',
	hypercholesterolemia: 'Hipercolesterolemia',
	mealsPerDay: 'Comidas por día',
	collations: 'Colaciones',
	waterPerDay: 'Agua por día',
	outsideMealsPerWeek: 'Comidas fuera de casa',
	places: 'Lugares',
	foodIntolerance: 'Intolerancia a alimentos',
	nonPreferredFoods: 'Alimentos no preferentes',
	cravingsInTheDay: 'Antojos en el día',
	supplements: 'Suplementos',
	alcoholConsumption: 'Consumo de alcohol',
	tobaccoConsumption: 'Consumo de tabaco',
	coffeeConsumption: 'Consumo de café',
	frequentFoods: 'Alimentos frecuentes',
	fruits: 'Frutas',
	chicken: 'Pollo',
	soda: 'Soda',
	sausages: 'Embutidos',
	candies: 'Dulces',
	vegetables: 'Vegetales',
	meat: 'Carne',
	cofee: 'Café',
	tortilla: 'Tortilla',
	milk: 'Leche',
	rice: 'Arroz',
	seafood: 'Marisco',
	juice: 'Jugo',
	cookies: 'Galletas',
	yoghurt: 'Yogur',
	greens: 'Legumbres',
	eggs: 'Huevos',
	tea: 'Té',
	bread: 'Pan',
	nutritionalDiagnosis: 'Diagnóstico nutricional',
	nutritionalTreatment: 'Tratamiento nutricional',
	weight: 'Peso',
	height: 'Altura',
	imc: 'Índice de masa corporal',
	percentBodyFat: '% de grasa corporal',
	percentBodyMass: '% de masa corporal',
	percentVisceralFat: '% de grasa visceral',
	waist: 'Cintura',
	abdomen: 'Abdomen',
	hip: 'Cadera',
	thigh: 'Muslo',
	bicep: 'Bicep',
	tricep: 'Tricep',
	leg: 'Pierna',
	subscapular: 'Subescapular',
	suprailiac: 'Suprailiaco',
	supraspinal: 'Supraespinal',
	goal: 'Meta',
	addAsAGoal: 'Agregar cómo meta',
	squats: 'Squats',
	pushUps: 'Push Ups',
	sitUps: 'Sit Ups',
	burpees: 'Burpees',
	joggys: 'Joggys',
	longeJump: 'Longe Jump',
	vUps: 'V Ups',
	inchworms: 'Inchworms',
	bottoms: 'Fondos',
	trusters: 'Trusters',
	wallBall: 'Wall Ball',
	jJacks: 'J Jacks',
	sideSuicides: 'Suicidas Laterales',
	jumpLenght1: 'Salto Longitud 1',
	jumpLenght2: 'Salto Longitud 2',
	jumpLenght3: 'Salto Longitud 3',
	front: 'Frente',
	back: 'Vuelta',
	addNewImage: 'Agregar nueva imagen',
	arrival: 'Entrada',
	departure: 'Salida',
	// Admin views categories fields strings.
	category: 'Categoría',
	// Admin views states fields strings.
	state: 'Estado',
	// Admin views locations fields strings.
	location: 'Academia',
	price20: 'Precio Iniciador',
	price55: 'Precio Agoge',
	// Admin views categories fields strings.
	category: 'Categoría',
	// Admin views classes fields strings.
	classe: 'Clase',
	startTime: 'Hora',
	hours: 'Horas',
	minutes: 'Minutos',
	ampm: 'Periodo',
	am: 'A.M.',
	pm: 'P.M.',
	price: 'Precio',
	classRoom: 'Salón',
	// Admin views generations fields strings.
	generation: 'Generación',
	startDate: 'Fecha Inicial',
	finishDate: 'Fecha Final',
	// Admin views controllers strings.
	add: 'Agregar',
	edit: 'Editar',
	delete: 'Eliminar',
	save: 'Guardar',
	cancel: 'Cancelar',
	close: 'Cerrar',
	// Public views titles strings.
	home: 'Inicio',
	about: 'Somos',
	terms: 'Términos y condiciones',
	privacy: 'Aviso de privacidad',
	facebook: 'Facebook',
	twitter: 'Twitter',
	instagram: 'Instagram',
	thanksForYourPreference: '¡Gracias por tu preferencia!',
	teamBrand: 'Equipo ' + brand,
	copyright: '2017 © ' + brand + ' / Todos los derechos reservados.',
	// Models users strings.
	pleaseEnterTheName: 'Por favor ingresa el nombre (al menos 3 caracteres)',
	pleaseEnterTheLastName: 'Por favor ingresa el apellido (al menos 3 caracteres)',
	pleaseSelectTheRole: 'Por favor selecciona el rol',
	pleaseEnterTheEmail: 'Por favor ingresa el email (al menos 5 caracteres)',
	pleaseEnterAValidEmail: 'Por favor ingresa un email válido',
	pleaseEnterThePassword: 'Por favor ingresa la contraseña (al menos 8 caracteres)',
	pleaseSelectAValidRole: 'Por favor selecciona un rol válido',
	pleaseSelectTheCity: 'Por favor selecciona la ciudad',
	pleaseSelectASmallerFile: 'Por favor selecciona un archivo mas pequeño',
	pleaseSelectAJPGFile: 'Por favor selecciona un archivo jpg',
	pleaseSelectAPDFFile: 'Por favor selecciona un archivo pdf',
	// Models states strings.
	pleaseSelectTheState: 'Por favor selecciona el estado',
	// Models locations strings.
	pleaseEnterThePhone: 'Por favor ingresa el teléfono (al menos 3 caracteres)',
	pleaseEnterTheAddress: 'Por favor ingresa la dirección (al menos 3 caracteres)',
	pleaseEnterThePostCode: 'Por favor ingresa el código postal (al menos 3 caracteres)',
	pleaseEnterThePrice20: 'Por favor ingresa el precio de 20 días (al menos 3 caracteres)',
	pleaseEnterThePrice55: 'Por favor ingresa el precio de 55 días (al menos 3 caracteres)',
	// Models classes strings.
	pleaseEnterTheStartTime: 'Por favor ingresa la hora de inicio',
	pleaseEnterThePrice: 'Por favor ingresa el precio (al menos 3 caracteres)',
	pleaseEnterTheClassRoom: 'Por favor ingresa el salón (al menos 3 caracteres)',
	pleaseSelectTheLocation: 'Por favor selecciona la academia',
	pleaseSelectTheCategory: 'Por favor selecciona la categoría',
	// Models generations strings.
	pleaseEnterTheStartDate: 'Por favor ingresa la fecha inicial',
	pleaseEnterTheFinishDate: 'Por favor ingresa la fecha final',
	// Model contracts strings.
	pleaseSelectTheClasse: 'Por favor selecciona la clase',
	pleaseSelectThePaymentMethod: 'Por favor selecciona el método de pago',
	pleaseSelectThePaymentQuantity: 'Por favor selecciona la cantidad de pagos',
	// Controllers general strings.
	accessDeniedPleaseLogin: 'Acceso denegado, por favor inicia sesión',
	accessDenied: 'Acceso denegado',
	thereWasAnErrorPleaseTryLater: 'Ha ocurrido un error, por favor inténtalo más tarde',
	accessDeniedAssistanceAlreadyRegistered: 'Acceso denegado, asistencia registrada anteriormente',
	// Controllers emails strings.
	hi: 'Hola',
	welcomeEmail: 'Bienvenido a ' + brand + '.</br></br>Nuestra vida se basa en las decisiones y acabas de hacer una muy importante, ¡darle el cuidado que tu cuerpo necesita! Nuestro objetivo en ' + brand + ' es agregar años a tu vida y vida a tus años.</br></br>Para lograr que obtengas resultados garantizados trabajaremos en dos áreas fundamentales para una transformación rápida y completa, la cual comienza con un entrenamiento funcional desarrollado por expertos para tener un impacto en tu cuerpo, además de un "entrenamiento de vida" para motivarte día con día en alcanzar tus objetivos propuestos, obteniendo así un cambio integral y, por último, nutrición personalizada para apoyarte en tu nuevo estilo de vida que tendrás para acelerar tu transformación.</br></br>No te preocupes, el camino podrá parecer difícil pero poco a poco descubrirás lo fuerte que eres con nuestro programa que traerá resultados asombrosos a tu cuerpo, salud y estilo de vida. Siéntete orgulloso de tu nuevo estilo de vida, comienza tu transformación porque YA ERES UN SPARTANO.</br></br>Una aventura está por comenzar, tu compromiso y tu constancia serán claves para llegar juntos a la meta, recuerda "el dolor es temporal, la gloria permanente".</br></br>Para comenzar a utilizar nuestra aplicación da clic en el siguiente enlace para verificar tu cuenta e iniciar sesión.',
	welcomeEmailEmployee: 'Bienvenido a ' + brand + '.</br></br>Para comenzar a utilizar nuestra aplicación da clic en el siguiente enlace para verificar tu cuenta e iniciar sesión.',
	verifyEmail: 'Has solicitado verificar tu cuenta en ' + brand + '. Da clic en el siguiente enlace para verificar tu cuenta.',
	passwordEmail: 'Has solicitado restaurar la contraseña de tu cuenta en ' + brand + '. Da clic en el siguiente enlace para introducir tu nueva contraseña.',
	// Controllers users strings.
	theUserWasNotFound: 'El alumno no fue encontrado',
	noUsersFound: 'No se encontraron alumnos',
	theUserWasNotSaved: 'El alumno no fue guardado',
	theUserAlreadyExists: 'El alumno ya existe',
	theUserWasDeleted: 'El alumno fue eliminado',
	theUserCanNotBeDeleted: 'El alumno no puede ser eliminado',
	// Controllers users public strings.
	theEmailOrPasswordEnteredAreInvalid: 'El email ó la contraseña ingresados son inválidos',
	theCitySelectedIsInvalid: 'La ciudad seleccionada es inválida',
	theEmailEnteredIsAlreadyAssociatedWithAnAccount: 'El email ingresado ya se encuentra relacionado a una cuenta',
	welcomeToBrand: 'Bienvenido a ' + brand,
	thanksForVisitingBrandComeBackSoon: 'Gracias por visitar ' + brand + ', vuelve pronto',
	theAccountEnteredWasNotFound: 'La cuenta ingresada no fue encontrada',
	pleaseCheckYourEmailWeHaveSentYouAMessage: 'Por favor revisa tu email, te hemos enviado un mensaje',
	thanksForVerifyingYourAccountPleaseLogin: 'Gracias por verificar tu cuenta, por favor inicia sesión',
	yourAccountHasAlreadyBeenVerifiedPleaseLogin: 'Tu cuenta ya ha sido verificada, por favor inicia sesión',
	yourPasswordHasBeenRestoredPleaseLogin: 'Tu contraseña ha sido restaurada, por favor inicia sesión',
	// Controllers states strings.
	theStateWasNotFound: 'El estado no fue encontrado',
	noStatesFound: 'No se encontraron estados',
	theStateAlreadyExists: 'El estado ya existe',
	theStateWasNotSaved: 'El estado no fue guardado',
	theStateWasDeleted: 'El estado fue eliminado',
	theStateCanNotBeDeleted: 'El estado no puede ser eliminado',
	// Controllers cities strings.
	theCityWasNotFound: 'La ciudad no fue encontrada',
	noCitiesFound: 'No se encontraron ciudades',
	theCityWasNotSaved: 'La ciudad no fue guardada',
	theCityAlreadyExists: 'La ciudad ya existe',
	theCityWasDeleted: 'La ciudad fue eliminada',
	theCityCanNotBeDeleted: 'La ciudad no puede ser eliminada',
	// Controllers locations strings.
	theLocationWasNotFound: 'La academia no fue encontrada',
	noLocationsFound: 'No se encontraron academias',
	theLocationAlreadyExists: 'La academia ya existe',
	theLocationWasNotSaved: 'La academia no fue guardada',
	theLocationWasDeleted: 'La academia fue eliminada',
	theLocationCanNotBeDeleted: 'La academia no puede ser eliminada',
	// Controllers categories strings.
	theCategoryWasNotFound: 'La categoría no fue encontrada',
	noCategoriesFound: 'No se encontraron categorías',
	theCategoryWasNotSaved: 'La categoría no fue guardada',
	theCategoryAlreadyExists: 'La categoría ya existe',
	theCategoryWasDeleted: 'La categoría fue eliminada',
	theCategoryCanNotBeDeleted: 'La categoría no puede ser eliminada',
	// Controllers classes strings.
	theClasseWasNotFound: 'La clase no fue encontrada',
	noClassesFound: 'No se encontraron clases',
	theClasseWasNotSaved: 'La clase no fue guardada',
	theClasseAlreadyExists: 'La clase ya existe',
	theClasseWasDeleted: 'La clase fue eliminada',
	theClasseCanNotBeDeleted: 'La clase no puede ser eliminada',
	// Controllers generations strings.
	theGenerationWasNotFound: 'La generación no fue encontrada',
	noGenerationsFound: 'No se encontraron generaciones',
	theGenerationWasNotSaved: 'La generación no fue guardada',
	theGenerationAlreadyExists: 'La generación ya existe',
	theGenerationWasDeleted: 'La generación fue eliminada',
	theGenerationCanNotBeDeleted: 'La generación no puede ser eliminada',
	// Controllers general strings.
	profileDescriptionEdit: 'Bienvenido a ' + brand + ', completa tu perfil antes de continuar.'
}

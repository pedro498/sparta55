$(function(){
	var element = 'assistance';
	$('#form').submit(function(){
		assistance_(element);
		return false;
	});
});
function assistance_(element){
	disabled_();
	var error = '';
	var assistance = $('#form #assistance').val();
	if(assistance.length != 24){
		$('#assistance').addClass('error');
		error += '<li>' + language['pleaseEnterYourID'] + '</li>';
	}
	if(error == ''){
		var data = {
			assistance: assistance
		};
		sendRequest_(element, data);
	}
	else{
		error_(error);
		setTimeout(function(){
			reset_(element);
		}, 2000);
	}
}

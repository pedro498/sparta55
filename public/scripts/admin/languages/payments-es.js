var element = 'pago';
var elements = 'pagos';
var language = {
	theRecordWasDeleted: 'El ' + element + ' fue eliminado',
	theRecordWasSaved: 'El ' + element + ' fue guardado',
	thereWasAnErrorDeletingThisRecord: 'Ha ocurrido un error al eliminar este ' + element
}
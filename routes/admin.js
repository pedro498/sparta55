// Modules declarations.
var express = require('express'),
	router = express.Router(),
	AdminController = require('../controllers/admin/admin.js'),
	TokenController = require('../controllers/token/token.js');
router.use(TokenController.adminToken);
// Employees routes.
router.route('/employees')
.get(AdminController.getEmployees)
router.route('/employees/edit')
.get(AdminController.getEmployeesEdit)
router.route('/employees/edit/:id')
.get(AdminController.getEmployeesEdit)
router.route('/employees/profile')
.get(AdminController.getEmployeesProfile)
// Users routes.
router.route('/users')
.get(AdminController.getUsers)
router.route('/users/edit')
.get(AdminController.getUsersEdit)
router.route('/users/edit/:id')
.get(AdminController.getUsersEdit)
router.route('/users/profile')
.get(AdminController.getUsersProfile)
// States routes.
router.route('/states')
.get(AdminController.getStates)
router.route('/states/edit')
.get(AdminController.getStatesEdit)
router.route('/states/edit/:id')
.get(AdminController.getStatesEdit)
// Cities routes.
router.route('/cities')
.get(AdminController.getCities)
router.route('/cities/edit')
.get(AdminController.getCitiesEdit)
router.route('/cities/edit/:id')
.get(AdminController.getCitiesEdit)
// Locations routes.
router.route('/locations')
.get(AdminController.getLocations)
router.route('/locations/edit')
.get(AdminController.getLocationsEdit)
router.route('/locations/edit/:id')
.get(AdminController.getLocationsEdit)
// Categories routes.
router.route('/categories')
.get(AdminController.getCategories)
router.route('/categories/edit')
.get(AdminController.getCategoriesEdit)
router.route('/categories/edit/:id')
.get(AdminController.getCategoriesEdit)
// Classes routes.
router.route('/classes')
.get(AdminController.getClasses)
router.route('/classes/edit')
.get(AdminController.getClassesEdit)
router.route('/classes/edit/:id')
.get(AdminController.getClassesEdit)
// Generations routes.
router.route('/generations')
.get(AdminController.getGenerations)
router.route('/generations/edit')
.get(AdminController.getGenerationsEdit)
router.route('/generations/edit/:id')
.get(AdminController.getGenerationsEdit)
module.exports = router;

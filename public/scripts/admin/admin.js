$(function(){
	$('.info').popup({
		position: 'bottom center',
		variation: 'small',
		offset: -3
	});
	$('.description').popup({
		position: 'top left',
		variation: 'small wide',
		offset: -3
	});
	$('.pop').popup({
		position: 'bottom left',
		variation: 'small',
		offset: -3
	});
	$('.ui.accordion').accordion();
	$('.ui.calendar').datepicker({
 		language: 'es-ES',
    format: 'dd/mm/yyyy',
		autoHide: true,
    days: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    daysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
    daysMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
    weekStart: 1,
    months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
	});
	$('#logout').click(logout_);
});
function success_(message, url){
	$('#message .content').html(message);
	$('#message').addClass('success');
	$('#message').transition('fade in', 1000);
	enabled_();
	if(url !== undefined && url != ''){
		setTimeout(function(){
			location_(url);
		}, 500);
	}
}
function error_(message, url){
	$('#message .content').html(message);
	$('#message').addClass('error');
	$('#message').transition('fade in', 1000);
	enabled_();
	if(url !== undefined && url != ''){
		setTimeout(function(){
			location_(url);
		}, 500);
	}
}
function alert_(message){
	alert(message);
	enabled_();
}
function disabled_(){
	if($('#message').css('display') != 'none'){
		$('#message').transition('fade out', 0);
		$('#message').removeClass('success');
		$('#message').removeClass('error');
		$('#message .content').html('');
	}
	$('.field').removeClass('error');
	$('.search').removeClass('error');
	$('.wrapper').addClass('ui form loading');
}
function enabled_(){
	$('.delete').popup('hide all');
	$('.wrapper').removeClass('ui form loading');
}
function location_(url){
	$(location).attr('href', url);
}
function logout_(){
	$.ajax({
		url: '../../../api/public/users/logout',
		dataType: 'JSON',
		type: 'GET',
		cache: false,
		timeout: 5000,
		success: function(response){
			success_('<li>' + language['thanksForVisitingBrandComeBackSoon'] + '</li>', '/secure/login');
		},
		error: function(error){
			error_('<li>' + language['thereWasAnErrorPleaseTryLater'] + '</li>');
		}
	});
}

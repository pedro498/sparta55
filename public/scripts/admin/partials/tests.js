$(function(){
	var element = 'tests';
	var elementParent = 'users';
	var tableTests = $('#tableTests').DataTable({
		'language': datatablesLanguagePartial,
		'autoWidth': false,
		'responsive': false,
		'columnDefs': [{
			'targets': 0,
			'className': 'hidden',
			'searchable': true,
			'orderable': false
		}, {
			'targets': [5,6,7,8,9,10,11,12,13,14,15,16,17,18],
			'className': 'hidden'
		}],
		'order': [[1, 'dsc']],
		'buttons': ['copy', 'print'],
    'pagingType': datatablesPagingType,
		'drawCallback': datatablesDrawCallback,
		'dom': 't<"ui grid"<"ten wide column"Bi><"right floated six wide column"p>>',
		'initComplete': datatablesInitComplete
	});
	$('#tableTests tbody').on('click', 'tr', function(){
		var tr = $(this);
		var row = tableTests.row(tr);
		if(row.child.isShown()){
			row.child.hide();
			tr.removeClass('shown');
		}else{
			row.child(formatTests(row.data())).show();
			tr.addClass('shown');
		}
	});
	$('#showTests').click(function(){
		$('#formTests')[0].reset();
		$('.ui.modal#modalTests').modal('show');
	});
	$('#addTests').click(function(){
		disabled_();
		var error = '';
		var _id = $('#form #_id').val(),
			squats = $('#formTests #squats').val(),
			pushUps = $('#formTests #pushUps').val(),
			sitUps = $('#formTests #sitUps').val(),
			burpees = $('#formTests #burpees').val(),
			joggys = $('#formTests #joggys').val(),
			longeJump = $('#formTests #longeJump').val(),
			vUps = $('#formTests #vUps').val(),
			inchworms = $('#formTests #inchworms').val(),
			bottoms = $('#formTests #bottoms').val(),
			trusters = $('#formTests #trusters').val(),
			wallBall = $('#formTests #wallBall').val(),
			jJacks = $('#formTests #jJacks').val(),
			sideSuicides = $('#formTests #sideSuicides').val(),
			jumpLenght1 = $('#formTests #jumpLenght1').val(),
			jumpLenght2 = $('#formTests #jumpLenght2').val(),
			jumpLenght3 = $('#formTests #jumpLenght3').val();
		var data = {
			tests: {
				squats: squats,
				pushUps: pushUps,
				sitUps: sitUps,
				burpees: burpees,
				joggys: joggys,
				longeJump: longeJump,
				vUps: vUps,
				inchworms: inchworms,
				bottoms: bottoms,
				trusters: trusters,
				wallBall: wallBall,
				jJacks: jJacks,
				sideSuicides: sideSuicides,
				jumpLenght1: jumpLenght1,
				jumpLenght2: jumpLenght2,
				jumpLenght3: jumpLenght3
			}
		}
		for(var key in data.tests){
			if(data.tests[key] == '' && typeof(data.tests[key]) !== 'boolean')
				if(key != 'longeJump' && key != 'vUps' && key != 'inchworms' && key != 'bottoms' && key != 'trusters' && key != 'wallBall' && key != 'jJacks' && key != 'sideSuicides' && key != 'jumpLenght1' && key != 'jumpLenght2' && key != 'jumpLenght3')
					error = language['pleaseEnterTheInfoRequired'];
		}
		if(error == ''){
			function successCallback(response){
				$('.ui.modal#modalTests').modal('hide');
				tableTests.row.add([
					response._id,
					formatDateTime(response.createdAt),
					response.squats,
					response.pushUps,
					response.sitUps,
					response.burpees,
					response.joggys,
					response.longeJump,
					response.vUps,
					response.inchworms,
					response.bottoms,
					response.trusters,
					response.wallBall,
					response.jJacks,
					response.sideSuicides,
					response.jumpLenght1,
					response.jumpLenght2,
					response.jumpLenght3,
					response.createdBy
				]).column(1).order('desc').draw(false);
				$('#formTests')[0].reset();
			}
			savePartialsRequest_(element, elementParent, data, successCallback);
		}
		else{
			alert_(error);
		}
		return false;
	});
});
function formatTests(data){
	return '<table class="details">' +
		'<tr>' +
		rowString('burpees', data[5]) +
		rowString('joggys', data[6]) +
		'</tr>' +
		'<tr>' +
		rowString('longeJump', data[7]) +
		rowString('vUps', data[8]) +
		'</tr>' +
		'<tr>' +
		rowString('inchworms', data[9]) +
		rowString('bottoms', data[10]) +
		'</tr>' +
		'<tr>' +
		rowString('trusters', data[11]) +
		rowString('wallBall', data[12]) +
		'</tr>' +
		'<tr>' +
		rowString('jJacks', data[13]) +
		rowString('sideSuicides', data[14]) +
		'</tr>' +
		'<tr>' +
		rowString('jumpLenght1', data[15]) +
		rowString('jumpLenght2', data[16]) +
		'</tr>' +
		'<tr>' +
		rowString('jumpLenght3', data[17]) +
		rowString('createdBy', data[18]) +
		'</tr>' +
		'</table>';
}

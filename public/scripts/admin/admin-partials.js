$(function(){
	$('.menu .item').tab({
		'onLoad': function(){
			if($(this).attr('data-tab') == 'contracts')
		  	$('#tableContracts').DataTable().draw(false);
			if($(this).attr('data-tab') == 'payments')
		  	$('#tablePayments').DataTable().draw(false);
			if($(this).attr('data-tab') == 'clinical')
		  	$('#tableClinical').DataTable().draw(false);
			if($(this).attr('data-tab') == 'dietary')
		  	$('#tableDietary').DataTable().draw(false);
			if($(this).attr('data-tab') == 'anthropometric')
				$('#tableAnthropometric').DataTable().draw(false);
			if($(this).attr('data-tab') == 'tests')
		  	$('#tableTests').DataTable().draw(false);
			if($(this).attr('data-tab') == 'photos')
		  	$('#tablePhotos').DataTable().draw(false);
			if($(this).attr('data-tab') == 'assistance')
		  	$('#tableAssists').DataTable().draw(false);
		},
		'onFirstLoad': function(){
			if($(this).attr('data-tab') == 'contracts')
		  	$('#tableContracts').DataTable().column(1).order('desc').draw(false);
			if($(this).attr('data-tab') == 'payments')
		  	$('#tablePayments').DataTable().column(1).order('desc').draw(false);
			if($(this).attr('data-tab') == 'clinical')
		  	$('#tableClinical').DataTable().column(1).order('desc').draw(false);
			if($(this).attr('data-tab') == 'dietary')
		  	$('#tableDietary').DataTable().column(1).order('desc').draw(false);
			if($(this).attr('data-tab') == 'anthropometric')
				$('#tableAnthropometric').DataTable().column(1).order('desc').draw(false);
			if($(this).attr('data-tab') == 'tests')
				$('#tableTests').DataTable().column(1).order('desc').draw(false);
			if($(this).attr('data-tab') == 'photos')
				$('#tablePhotos').DataTable().column(1).order('desc').draw(false);
			if($(this).attr('data-tab') == 'assistance')
				$('#tableAssists').DataTable().column(1).order('desc').draw(false);
		}
	});
});
function rowSection(key, val){
	return '<td class="key title" colspan="4">' + language[key] + ' | ' + language[val] + '</td>';
}
function rowString(key, val){
	return '<td class="key">' + language[key] + ':</td><td class="value">' + val + '</td>';
}
function rowText(key, val){
	return '<td class="key">' + language[key] + ':</td><td class="value" colspan="3">' + val + '</td>';
}
function rowImage(key, val){
	return '<td class="key">' + language[key] + ':</td><td class="value"><a href="https://s3-us-west-2.amazonaws.com/sparta55testing/photos/' + val + '" target="_blank"><img src="https://s3-us-west-2.amazonaws.com/sparta55testing/photos/' + val + '" style="width: 100%"/></a></td>';
}
function rowDiet(key, val){
	return '<td class="key">' + language[key] + ':</td><td class="value"><a href="https://s3-us-west-2.amazonaws.com/sparta55testing/diets/' + val + '.pdf" target="_blank"><img src="https://s3-us-west-2.amazonaws.com/sparta55testing/diets/' + val + '.pdf" style="width: 20%"/></a></td>';
}
function rowBoolean(key, val){
	return '<td class="key">' + language[key] + ':</td><td class="value"><i class="icon ' + (val == 'true' ? 'checkmark box' : 'minus square outline') + '"></i></td>';
}
function rowEmpty(){
	return '<td class="key"></td><td class="value"></td>';
}
function savePartialsRequest_(element, elementParent, data, successCallback, file){
	file = file || false;
	var _id = $('#_id').val();
	var url = '../../../api/' + elementParent + '/' + _id + '/' + element,
		type = 'POST';
	$.ajax({
		url: url,
		dataType: 'JSON',
		type: type,
		cache: false,
		timeout: 15000,
		data: data,
		processData: (file ? false : true),
		contentType: (file ? false : 'application/x-www-form-urlencoded; charset=UTF-8'),
		success: function(response){
			success_('<li>' + language['theRecordWasSaved'] + '</li>');
			successCallback(response);
		},
		error: function(error){
			var message = language['thereWasAnErrorPleaseTryLater'];
			if(error.responseJSON != undefined)
				message = error.responseJSON.message;
			if(message == language['accessDeniedPleaseLogin'])
				error_(message, '/admin');
			error_('<li>' + message + '</li>');
		}
	});
}

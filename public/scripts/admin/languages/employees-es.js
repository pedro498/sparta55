var element = 'usuario';
var elements = 'usuarios';
var language = {
	avatar: 'Avatar',
	theRecordWasDeleted: 'El ' + element + ' fue eliminado',
	theRecordWasSaved: 'El ' + element + ' fue guardado',
	thereWasAnErrorDeletingThisRecord: 'Ha ocurrido un error al eliminar este ' + element
}
// Modules declarations.
var mongoose = require('mongoose'),
	validation = require('../../libraries/validation.js'),
	languages = require('../../libraries/languages.js'),
	language = 'es',
	capitalize = require('capitalize'),
	Classes = mongoose.model('Classes'),
	Locations = mongoose.model('Locations'),
	Categories = mongoose.model('Categories');
// Classes controllers.
function saveClasse(classe, res){
	classe.save(function(err, classe){
		if(validation.save(err, classe, res, 'classe'))
			res.status(200).jsonp(classe);
	});
}
function saveClasseLocation(classe, location, res){
	classe.location = location._id;
	classe.save(function(err, classe){
		if(validation.save(err, classe, res, 'classe')){
			location.classes.push(classe);
			location.save(function(err, location){
				if(validation.save(err, location, res, 'location'))
					res.status(200).jsonp(classe);
			});
		}
	});
}
function saveClasseCategory(classe, category, res){
	classe.category = category._id;
	classe.save(function(err, classe){
		if(validation.save(err, classe, res, 'classe')){
			category.classes.push(classe);
			category.save(function(err, category){
				if(validation.save(err, category, res, 'category'))
					res.status(200).jsonp(classe);
			});
		}
	});
}
function saveClasseLocationCategory(classe, location, category, res){
	classe.location = location._id;
	classe.category = category._id;
	classe.save(function(err, classe){
		if(validation.save(err, classe, res, 'classe')){
			location.classes.push(classe);
			location.save(function(err, location){
				if(validation.save(err, location, res, 'location')){
					category.classes.push(classe);
					category.save(function(err, category){
						if(validation.save(err, category, res, 'category'))
							res.status(200).jsonp(classe);
					});
				}
			});
		}
	});
}
function removeClasse(classe, res){
	classe.remove(function(err){
		validation.remove(err, classe, res, 'classe');
	});
}
module.exports.getClasses = function(req, res){
	Classes.find(function(err, classes){
		if(validation.find(err, classes, res, 'classes'))
			res.status(200).jsonp(classes);
	}).select('startTime location category classRoom users').populate([{
		path: 'location',
		select: 'name'
	}, {
		path: 'category',
		select: 'name'
	}]).lean();
};
module.exports.postClasse = function(req, res){
	var classe = new Classes();
	if(typeof req.body.startTime !== 'undefined')
		classe.startTime = req.body.startTime;
	if(typeof req.body.classRoom !== 'undefined')
		classe.classRoom = req.body.classRoom.toUpperCase();
	Locations.findById(req.body.location, function(err, location){
		if(validation.find(err, location, res, 'location')){
			Categories.findById(req.body.category, function(err, category){
				if(validation.find(err, category, res, 'category'))
					saveClasseLocationCategory(classe, location, category, res);
			});

		}
	});
};
module.exports.getClasse = function(req, res){
	Classes.findById(req.params.id, function(err, classe){
		if(validation.find(err, classe, res, 'classe'))
			res.status(200).jsonp(classe);
	}).select('startTime location category classRoom users').populate([{
		path: 'location',
		select: 'name'
	}, {
		path: 'category',
		select: 'name'
	}, {
		path: 'users',
		select: 'name lastName',
		options: {
			sort: {
				'name': 1,
				'lastName': 1
			}
		}
	}]).lean();
};
module.exports.putClasse = function(req, res){
	Classes.findById(req.params.id, function(err, classe){
		if(validation.find(err, classe, res, 'classe')){
			if(typeof req.body.startTime !== 'undefined')
				classe.startTime = req.body.startTime;
			if(typeof req.body.classRoom !== 'undefined')
				classe.classRoom = req.body.classRoom.toUpperCase();
			if(typeof req.body.location === 'undefined')
				req.body.location = classe.location;
			if(typeof req.body.category === 'undefined')
				req.body.category = classe.category;
			if((typeof req.body.location !== 'undefined' && req.body.location == String(classe.location)) && (typeof req.body.category !== 'undefined' && req.body.category == String(classe.category)))
				saveClasse(classe, res);
			else{
				if((typeof req.body.location !== 'undefined' && req.body.location != String(classe.location)) && (typeof req.body.category !== 'undefined' && req.body.category != String(classe.category))){
					Locations.findById(req.body.location, function(err, location){
						if(validation.find(err, location, res, 'location')){
							Locations.findById(classe.location, function(err, locationRel){
								if(validation.find(err, locationRel, res, 'location')){
									locationRel.classes.pull(classe);
									locationRel.save(function(err, locationRel){
										if(validation.save(err, locationRel, res, 'location')){
											Categories.findById(req.body.category, function(err, category){
												if(validation.find(err, category, res, 'category')){
													Categories.findById(classe.category, function(err, categoryRel){
														if(validation.find(err, categoryRel, res, 'category')){
															categoryRel.classes.pull(classe);
															categoryRel.save(function(err, categoryRel){
																if(validation.save(err, categoryRel, res, 'category'))
																	saveClasseCategory(classe, category, res);
															});
														}
													});
												}
											});
										}
									});
								}
							});
						}
					});
				}
				else if(typeof req.body.location !== 'undefined' && req.body.location != String(classe.location)){
					Locations.findById(req.body.location, function(err, location){
						if(validation.find(err, location, res, 'location')){
							Locations.findById(classe.location, function(err, locationRel){
								if(validation.find(err, locationRel, res, 'location')){
									locationRel.classes.pull(classe);
									locationRel.save(function(err, locationRel){
										if(validation.save(err, locationRel, res, 'location'))
											saveClasseLocation(classe, location, res);
									});
								}
							});
						}
					});
				}
				else if(typeof req.body.category !== 'undefined' && req.body.category != String(classe.category)){
					Categories.findById(req.body.category, function(err, category){
						if(validation.find(err, category, res, 'category')){
							Categories.findById(classe.category, function(err, categoryRel){
								if(validation.find(err, categoryRel, res, 'category')){
									categoryRel.classes.pull(classe);
									categoryRel.save(function(err, categoryRel){
										if(validation.save(err, categoryRel, res, 'category'))
											saveClasseCategory(classe, category, res);
									});
								}
							});
						}
					});
				}
			}
		}
	});
};
module.exports.deleteClasse = function(req, res){
	Classes.findById(req.params.id, function(err, classe){
		if(validation.find(err, classe, res, 'classe')){
			if(classe.users.length > 0)
				validation.notRemove(err, classe, res, 'classe');
			else{
				Locations.findById(classe.location, function(err, locationRel){
					if(validation.canRemove(err, locationRel, res, 'location')){
						Categories.findById(classe.category, function(err, categoryRel){
							if(validation.canRemove(err, categoryRel, res, 'category'))
								removeClasse(classe, res);
							else{
								categoryRel.classes.pull(classe);
								categoryRel.save(function(err, categoryRel){
									if(validation.save(err, categoryRel, res, 'category'))
										removeClasse(classe, res);
								});
							}
						});
					}
					else{
						locationRel.classes.pull(classe);
						locationRel.save(function(err, locationRel){
							if(validation.save(err, locationRel, res, 'location')){
								Categories.findById(classe.category, function(err, categoryRel){
									if(validation.canRemove(err, categoryRel, res, 'category'))
										removeClasse(classe, res);
									else{
										categoryRel.classes.pull(classe);
										categoryRel.save(function(err, categoryRel){
											if(validation.save(err, categoryRel, res, 'category'))
												removeClasse(classe, res);
										});
									}
								});
							}
						});
					}
				});
			}
		}
	});
};
// Public classes controllers.
module.exports.getPublicClasses = function(req, res){
	Classes.find(function(err, classes){
		if(validation.find(err, classes, res, 'classes'))
			res.status(200).jsonp(classes);
	}).select('startTime location category classRoom users').populate([{
		path: 'location',
		select: 'name price20 price55'
	}, {
		path: 'category',
		select: 'name'
	}]).lean();
};

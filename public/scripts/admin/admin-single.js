function initialize_(element){
	$('#save').click(function(){
		save_(element);
	});
	$('#cancel').click(function(){
		cancel_(element);
	});
	$('#delete').click(function(){
		delete_(element);
	});
	$('#form').submit(function(){
		save_(element);
		return false;
	});
	$('.ui.dropdown').dropdown();
}
function cancel_(element){
	location_('/admin/' + element);	
}
function delete_(element){
	disabled_();
	deleteRequest_(element);
}
function deleteRequest_(element){
	var _id = $('#_id').val();
	$.ajax({
		url: '../../../api/' + element + '/' + _id,
		dataType: 'JSON',
		type: 'DELETE',
		cache: false,
		timeout: 15000,
		success: function(response){
			success_('<li>' + language['theRecordWasDeleted'] + '</li>', '/admin/' + element + '/');
		},
		error: function(error){
			error_('<li>' + language['thereWasAnErrorDeletingThisRecord'] + '</li>');
		}
	});
}
function saveRequest_(element, data, file){
	file = file || false;
	var _id = $('#_id').val();
	var url = '../../../api/' + element,
		type = 'POST';
	if(_id.length > 0){
		url = url + '/' + _id;
		type = 'PUT';
	}
	$.ajax({
		url: url,
		dataType: 'JSON',
		type: type,
		cache: false,
		timeout: 15000,
		data: data,
		processData: (file ? false : true),
		contentType: (file ? false : 'application/x-www-form-urlencoded; charset=UTF-8'),
		success: function(response){
			success_('<li>' + language['theRecordWasSaved'] + '</li>', '/admin/' + element);
		},
		error: function(error){
			var message = language['thereWasAnErrorPleaseTryLater'];
			if(error.responseJSON != undefined)
				message = error.responseJSON.message;
			if(message == language['accessDeniedPleaseLogin'])
				error_(message, '/admin');
			error_('<li>' + message + '</li>');
		}
	});
}
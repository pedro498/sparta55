// Modules declarations.
var mongoose = require('mongoose'),
	validation = require('../libraries/validation.js'),
	languages = require('../libraries/languages.js'),
	language = 'es',
	validator = require('validator'),
	schema = mongoose.Schema,
	generationsSchema = new schema({
		name: {
			type: String,
			min: 3,
			required: true
		},
		startDate: {
			type: Date,
			required: true,
			index: true
		},
		finishDate: {
			type: Date,
			required: true,
			index: true
		},
		price: {
			type: Number,
			min: 0,
			required: true
		},
		location: {
			type: schema.Types.ObjectId,
			ref: 'Locations',
			required: true,
			index: true
		},
		category: {
			type: schema.Types.ObjectId,
			ref: 'Categories',
			required: true,
			index: true
		},
		users: [{
			type: schema.Types.ObjectId,
			ref: 'Users',
			index: true
		}],
		createdAt: {
			type: Date
		},
		updatedAt: {
			type: Date
		}
	});
generationsSchema.pre('validate', function(next){
	if(typeof this.name === 'undefined' || this.name.length < 3)
		validation.nextError(next, 'pleaseEnterTheName');
	if(typeof this.startDate === 'undefined')
		validation.nextError(next, 'pleaseEnterTheStartDate');
	if(typeof this.finishDate === 'undefined')
		validation.nextError(next, 'pleaseEnterTheFinishDate');
	else if(typeof this.price === 'undefined' || this.price.length < 3)
		validation.nextError(next, 'pleaseEnterThePrice');
	else if(typeof this.location === 'undefined')
		validation.nextError(next, 'pleaseSelectTheLocation');
	else if(typeof this.category === 'undefined')
		validation.nextError(next, 'pleaseSelectTheCategory');
	next();
});
generationsSchema.pre('save', function(next){
	this.updatedAt = new Date();
	if(!this.createdAt)
		this.createdAt = this.updatedAt;
	next();
});
module.exports = mongoose.model('Generations', generationsSchema);

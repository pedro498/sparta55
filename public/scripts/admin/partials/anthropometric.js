$(function(){
	var element = 'anthropometric';
	var elementParent = 'users';
	var tableAnthropometric = $('#tableAnthropometric').DataTable({
		'language': datatablesLanguagePartial,
		'autoWidth': false,
		'responsive': false,
		'columnDefs': [{
			'targets': 0,
			'className': 'hidden',
			'searchable': true,
			'orderable': false
		}, {
			'targets': [5,6,7,8,9,10,11,12,13,14,15,16,17,18,19],
			'className': 'hidden'
		}],
		'order': [[1, 'dsc']],
		'buttons': ['copy', 'print'],
    'pagingType': datatablesPagingType,
		'drawCallback': datatablesDrawCallback,
		'dom': 't<"ui grid"<"ten wide column"Bi><"right floated six wide column"p>>',
		'initComplete': datatablesInitComplete
	});
	$('#tableAnthropometric tbody').on('click', 'tr', function(){
		var tr = $(this);
		var row = tableAnthropometric.row(tr);
		if(row.child.isShown()){
			row.child.hide();
			tr.removeClass('shown');
		}else{
			row.child(formatAnthropometric(row.data())).show();
			tr.addClass('shown');
		}
	});
	$('#showAnthropometric').click(function(){
		$('#formAnthropometric')[0].reset();
		$('.ui.modal#modalAnthropometric').modal('show');
	});
	$('#addAnthropometric').click(function(){
		disabled_();
		var error = '';
		var _id = $('#form #_id').val(),
			weight = $('#formAnthropometric #weight').val(),
			height = $('#formAnthropometric #height').val(),
			imc = $('#formAnthropometric #imc').val(),
			percentBodyFat = $('#formAnthropometric #percentBodyFat').val(),
			percentBodyMass = $('#formAnthropometric #percentBodyMass').val(),
			percentVisceralFat = $('#formAnthropometric #percentVisceralFat').val(),
			waist = $('#formAnthropometric #waist').val(),
			abdomen = $('#formAnthropometric #abdomen').val(),
			hip = $('#formAnthropometric #hip').val(),
			bicep = $('#formAnthropometric #bicep').val(),
			tricep = $('#formAnthropometric #tricep').val(),
			thigh = $('#formAnthropometric #thigh').val(),
			leg = $('#formAnthropometric #leg').val(),
			subscapular = $('#formAnthropometric #subscapular').val(),
			suprailiac = $('#formAnthropometric #suprailiac').val(),
			supraspinal = $('#formAnthropometric #supraspinal').val(),
			goal = $('#formAnthropometric #goal').val();
		var data = {
			anthropometric: {
				weight: weight,
				height: height,
				imc: imc,
				percentBodyFat: percentBodyFat,
				percentBodyMass: percentBodyMass,
				percentVisceralFat: percentVisceralFat,
				waist: waist,
				abdomen: abdomen,
				hip: hip,
				bicep: bicep,
				tricep: tricep,
				thigh: thigh,
				leg: leg,
				subscapular: subscapular,
				suprailiac: suprailiac,
				supraspinal: supraspinal,
				goal: goal
			}
		}
		for(var key in data.anthropometric){
			if(data.anthropometric[key] == '' && typeof(data.anthropometric[key]) !== 'boolean')
				if(key != 'waist' && key != 'abdomen' && key != 'hip' && key != 'bicep' && key != 'tricep' && key != 'thigh' && key != 'leg' && key != 'subscapular' && key != 'suprailiac' && key != 'supraspinal' && key != 'goal')
					error = language['pleaseEnterTheInfoRequired'];
		}
		if(error == ''){
			function successCallback(response){
				$('.ui.modal#modalAnthropometric').modal('hide');
				tableAnthropometric.row.add([
					response._id,
					formatDateTime(response.createdAt),
					response.weight,
					response.height,
					response.imc,
					response.percentBodyFat,
					response.percentBodyMass,
					response.percentVisceralFat,
					response.waist,
					response.abdomen,
					response.hip,
					response.bicep,
					response.tricep,
					response.thigh,
					response.leg,
					response.subscapular,
					response.suprailiac,
					response.supraspinal,
					response.goal,
					response.createdBy
				]).column(1).order('desc').draw(false);
				$('#formAnthropometric')[0].reset();
			}
			savePartialsRequest_(element, elementParent, data, successCallback);
		}
		else{
			alert_(error);
		}
		return false;
	});
});
function formatAnthropometric(data){
	return '<table class="details">' +
		'<tr>' +
		rowString('percentBodyFat', data[5]) +
		rowString('percentBodyMass', data[6]) +
		'</tr>' +
		'<tr>' +
		rowString('percentVisceralFat', data[7]) +
		rowString('waist', data[8]) +
		'</tr>' +
		'<tr>' +
		rowString('abdomen', data[9]) +
		rowString('hip', data[10]) +
		'</tr>' +
		'<tr>' +
		rowString('thigh', data[11]) +
		rowString('leg', data[12]) +
		'</tr>' +
		'<tr>' +
		rowString('bicep', data[13]) +
		rowString('tricep', data[14]) +
		'</tr>' +
		'<tr>' +
		rowString('subscapular', data[15]) +
		rowString('suprailiac', data[16]) +
		'</tr>' +
		'<tr>' +
		rowString('supraspinal', data[17]) +
		rowEmpty() +
		'</tr>' +
		'<tr>' +
		rowText('goal', data[18]) +
		'</tr>' +
		'<tr>' +
		rowString('createdBy', data[19]) +
		rowEmpty() +
		'</tr>' +
		'</table>';
}

$(function(){
	var element = 'users';
	var table = $('#table').DataTable({
		'language': datatablesLanguage,
		'autoWidth': false,
		'responsive': false,
		'columnDefs': [{
			'targets': 0,
			'className': 'hidden',
			'searchable': false,
			'orderable': false
		}, {
			'targets': [-1],
			'width': '30px',
			'searchable': false,
			'orderable': false,
			'className': 'center padding'
		}],
		'order': [[1, 'asc'], [2, 'asc']],
		'buttons': ['copy', 'print'],
		'pagingType': datatablesPagingType,
		'drawCallback': datatablesDrawCallback,
		'dom': datatablesDom,
		'initComplete': datatablesInitComplete
	});
	initialize_(table, element);
});

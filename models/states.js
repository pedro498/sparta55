// Modules declarations.
var mongoose = require('mongoose'),
	validation = require('../libraries/validation.js'),
	languages = require('../libraries/languages.js'),
	language = 'es',
	schema = mongoose.Schema,
	statesSchema = new schema({
		name: {
			type: String,
			min: 3,
			required: true,
			unique: true,
			index: true
		},
		cities: [{
			type: schema.Types.ObjectId,
			ref: 'Cities',
			index: true
		}],
		createdAt: {
			type: Date
		},
		updatedAt: {
			type: Date
		}
	});
statesSchema.pre('validate', function(next){
	if(typeof this.name === 'undefined' || this.name.length < 3)
		validation.nextError(next, 'pleaseEnterTheName');
	next();
});
statesSchema.pre('save', function(next){
	this.updatedAt = new Date();
	if(!this.createdAt)
		this.createdAt = this.updatedAt;
	next();
});
module.exports = mongoose.model('States', statesSchema);
// Modules declarations.
var mongoose = require('mongoose'),
	validation = require('../../libraries/validation.js'),
	languages = require('../../libraries/languages.js'),
	language = 'es',
	capitalize = require('capitalize'),
	Categories = mongoose.model('Categories');
// Categories controllers.
function saveCategory(category, res){
	category.save(function(err, category){
		if(validation.save(err, category, res, 'category'))
			res.status(200).jsonp(category);
	});
}
function removeCategory(category, res){
	category.remove(function(err){
		validation.remove(err, category, res, 'category');
	});
}
module.exports.getCategories = function(req, res){
	Categories.find(function(err, categories){
		if(validation.find(err, categories, res, 'categories'))
			res.status(200).jsonp(categories);
	}).select('name classes').sort('name').lean();
};
module.exports.postCategory = function(req, res){
	var category = new Categories();
	if(typeof req.body.name !== 'undefined')
		category.name = capitalize.words(req.body.name);
	saveCategory(category, res);
};
module.exports.getCategory = function(req, res){
	Categories.findById(req.params.id, function(err, category){
		if(validation.find(err, category, res, 'category'))
			res.status(200).jsonp(category);
	}).select('name classes').populate([{
		path: 'classes',
		select: 'startTime location classRoom',
		populate: {
			path: 'location',
			select: 'name'
		},
		options: {
			sort: {
				'startTime': 1
			}
		}
	}]).lean();
};
module.exports.putCategory = function(req, res){
	Categories.findById(req.params.id, function(err, category){
		if(validation.find(err, category, res, 'category')){
			if(typeof req.body.name !== 'undefined')
				category.name = capitalize.words(req.body.name);
			saveCategory(category, res);
		}
	});
};
module.exports.deleteCategory = function(req, res){
	Categories.findById(req.params.id, function(err, category){
		if(validation.find(err, category, res, 'category')){
			if(category.classes.length > 0)
				validation.notRemove(err, category, res, 'category');
			else
				removeCategory(category, res);
		}
	});
};
// Public categories controllers.
module.exports.getPublicCategories = function(req, res){
	Categories.find(function(err, categories){
		if(validation.find(err, categories, res, 'categories'))
			res.status(200).jsonp(categories);
	}).select('name').sort('name');
};

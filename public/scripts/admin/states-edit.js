$(function(){
	var element = 'states';
	initialize_(element);
});
function save_(element){
	disabled_();
	var error = '';
	var _id = $('#form #_id').val(),
		name = $('#form #name').val();
	if(name.length < 3){
		$('#name_').addClass('error');
		error += '<li>' + language['pleaseEnterTheName'] + '</li>';
	}
	if(error == ''){
		var data = {
			name: name
		};
		saveRequest_(element, data);
	}
	else
		error_(error);
}
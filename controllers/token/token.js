// Modules declarations.
var mongoose = require('mongoose'),
	languages = require('../../libraries/languages.js'),
	language = 'es',
	bcryptjs = require('bcryptjs'),
	jsonWebToken = require('jsonwebtoken'),
	Users = mongoose.model('Users');
// Secure token controllers.
exports.secureToken = function(req, res, next){
	if(!formatURL(req, res, '/secure')){
		var userToken = getUserToken(req, res);
		if(userToken){
			jsonWebToken.verify(userToken, req.app.get('secret'), function(err, decoded){
				if(err || !decoded)
					secureClearToken(req, res, next);
				else{
					Users.findOne({
						email: decoded.email
					}, function(err, user){
						if(err || !user)
							secureClearToken(req, res, next);
						else{
							if(user.role == 'system' || user.role == 'nutrologist' || user.role == 'receptionist')
								res.redirect('/admin/users');
							else if(user.role == 'user')
								res.redirect('/admin/users/profile');
							else
								secureClearToken(req, res, next);
						}
					});
				}
			});
		}
		else
			secureRoutes(req, res, next);
	}
};
function secureClearToken(req, res, next){
	clearUserToken(req, res);
	secureRoutes(req, res, next);
}
function secureRoutes(req, res, next){
	if(req.method == 'GET' && (
		req.url == '/login' ||
		req.url == '/verify' ||
		req.url == '/forgot' ||
		req.url.substring(0, 8) == '/verify/' ||
		req.url.substring(0, 8) == '/forgot/' ||
		req.url == '/assistance')
	)
		next();
	else
		res.redirect('/secure/login');
}
// API token controllers.
exports.apiToken = function(req, res, next){
	if(!formatURL(req, res, '/api')){
		if(!apiPublicRoutes(req, res, next)){
			var userToken = getUserToken(req, res);
			if(userToken){
				jsonWebToken.verify(userToken, req.app.get('secret'), function(err, decoded){
					if(err || !decoded)
						apiClearToken(req, res, next)
					else{
						Users.findOne({
							email: decoded.email
						}, function(err, user){
							if(err || !user)
								apiClearToken(req, res, next)
							else{
								if(user.role == 'system' || user.role == 'nutrologist' || user.role == 'receptionist' || user.role == 'user'){
									req.user = {
										_id: user._id,
										name: user.name,
										lastName: user.lastName,
										role: user.role,
										status: user.status,
										location: user.location,
										avatar: user.avatar
									};
									apiRoutes(req, res, next);
								}
								else
									apiClearToken(req, res, next)
							}
						});
					}
				});
			}
			else
				apiClearToken(req, res, next)
		}
	}
};
function apiClearToken(req, res, next){
	clearUserToken(req, res);
	res.status(401).jsonp({'message': languages.strings(language, 'accessDeniedPleaseLogin')});
}
function apiRoutes(req, res, next){
	if(req.user.role == 'system' && req.method == 'GET' && (
		req.url == '/employees' ||
		req.url.substring(0, 11) == '/employees/' ||
		req.url == '/users' ||
		req.url.substring(0, 7) == '/users/' ||
		req.url == '/states' ||
		req.url.substring(0, 8) == '/states/' ||
		req.url == '/cities' ||
		req.url.substring(0, 8) == '/cities/' ||
		req.url == '/locations' ||
		req.url.substring(0, 11) == '/locations/' ||
		req.url == '/categories' ||
		req.url.substring(0, 12) == '/categories/' ||
		req.url == '/classes' ||
		req.url.substring(0, 9) == '/classes/' ||
		req.url == '/generations' ||
		req.url.substring(0, 13) == '/generations/')
	)
		next();
	else if(req.user.role == 'system' && req.method == 'POST' && (
		req.url == '/employees' ||
		req.url == '/users' ||
		req.url.substring(31, 41) == '/contracts' ||
		req.url.substring(31, 40) == '/payments' ||
		req.url.substring(31, 40) == '/clinical' ||
		req.url.substring(31, 39) == '/dietary' ||
		req.url.substring(31, 46) == '/anthropometric' ||
		req.url.substring(31, 37) == '/tests' ||
		req.url.substring(31, 38) == '/photos' ||
		req.url == '/states' ||
		req.url == '/cities' ||
		req.url == '/locations' ||
		req.url == '/categories' ||
		req.url == '/classes' ||
		req.url == '/generations')
	)
		next();
	else if(req.user.role == 'system' && req.method == 'PUT' && (
		req.url.substring(0, 11) == '/employees/' ||
		req.url.substring(0, 7) == '/users/' ||
		req.url.substring(0, 8) == '/states/' ||
		req.url.substring(0, 8) == '/cities/' ||
		req.url.substring(0, 11) == '/locations/' ||
		req.url.substring(0, 12) == '/categories/' ||
		req.url.substring(0, 9) == '/classes/' ||
		req.url.substring(0, 13) == '/generations/')
	)
		next();
	else if(req.user.role == 'system' && req.method == 'DELETE' && (
		req.url.substring(0, 11) == '/employees/' ||
		req.url.substring(0, 7) == '/users/' ||
		req.url.substring(0, 8) == '/states/' ||
		req.url.substring(0, 8) == '/cities/' ||
		req.url.substring(0, 11) == '/locations/' ||
		req.url.substring(0, 12) == '/categories/' ||
		req.url.substring(0, 9) == '/classes/' ||
		req.url.substring(0, 13) == '/generations/')
	)
		next();
	else if(req.user.role == 'nutrologist' && req.method == 'GET' && (
		req.url == '/users' ||
		req.url.substring(0, 7) == '/users/')
	)
		next();
	else if(req.user.role == 'nutrologist' && req.method == 'POST' && (
		req.url.substring(31, 40) == '/clinical' ||
		req.url.substring(31, 39) == '/dietary' ||
		req.url.substring(31, 46) == '/anthropometric' ||
		req.url.substring(31, 38) == '/photos')
	)
		next();
	else if(req.user.role == 'receptionist' && req.method == 'GET' && (
		req.url == '/users' ||
		req.url.substring(0, 7) == '/users/')
	)
		next();
	else if(req.user.role == 'receptionist' && req.method == 'POST' && (
		req.url == '/users' ||
		req.url.substring(31, 41) == '/contracts' ||
		req.url.substring(31, 40) == '/payments' ||
		req.url.substring(31, 37) == '/tests')
	)
		next();
	else if(req.user.role == 'receptionist' && req.method == 'PUT' && (
		req.url.substring(0, 7) == '/users/')
	)
		next();
	else if(req.user.role == 'receptionist' && req.method == 'DELETE' && (
		req.url.substring(0, 7) == '/users/')
	)
		next();
	else if(req.user.role == 'user' && req.method == 'GET' && (
		req.url.substring(0, 7) == '/users/')
	)
		next();
	else if(req.user.role == 'user' && req.method == 'PUT' && (
		req.url.substring(0, 7) == '/users/')
	)
		next();
	else
		res.status(401).jsonp({'message': languages.strings(language, 'accessDeniedPleaseLogin')});
}
function apiPublicRoutes(req, res, next){
	if(req.method == 'GET' && (
		req.url == '/public/users/logout' ||
		req.url == '/public/users/setup' ||
		req.url == '/public/states' ||
		req.url == '/public/cities' ||
		req.url == '/public/locations' ||
		req.url == '/public/categories' ||
		req.url == '/public/classes' ||
		req.url == '/public/generations' ||
		req.url.substring(0, 20) == '/public/users/login?' ||
		req.url.substring(0, 21) == '/public/users/logout?' ||
		req.url.substring(0, 21) == '/public/users/verify/' ||
		req.url.substring(0, 21) == '/public/users/forgot/')
	){
		next();
		return true;
	}
	else if(req.method == 'PUT' && (
		req.url == '/public/users/verify' ||
		req.url == '/public/users/forgot' ||
		req.url.substring(0, 21) == '/public/users/verify/' ||
		req.url.substring(0, 21) == '/public/users/forgot/' ||
		req.url == '/public/users/assistance')
	){
		next();
		return true;
	}
	else
		return false;
}
// Admin token controllers.
exports.adminToken = function(req, res, next){
	if(!formatURL(req, res, '/admin')){
		var userToken = getUserToken(req, res);
		if(userToken){
			jsonWebToken.verify(userToken, req.app.get('secret'), function(err, decoded){
				if(err || !decoded)
					adminClearToken(req, res, next);
				else{
					Users.findOne({
						email: decoded.email
					}, function(err, user){
						if(err || !user)
							adminClearToken(req, res, next);
						else{
							if(user.role == 'system' || user.role == 'nutrologist' || user.role == 'receptionist' || user.role == 'user'){
								req.user = {
									_id: user._id,
									name: user.name,
									lastName: user.lastName,
									role: user.role,
									avatar: user.avatar
								};
								adminRoutes(req, res, next);
							}
							else
								adminClearToken(req, res, next);
						}
					});
				}
			});
		}
		else
			adminClearToken(req, res, next);
	}
};
function adminClearToken(req, res, next){
	clearUserToken(req, res);
	res.redirect('/secure/login');
}
function adminRoutes(req, res, next){
	if(req.method == 'GET' && ((req.user.role == 'system' && req.url == '/employees/profile') || (req.user.role == 'nutrologist' && req.url == '/employees/profile') || (req.user.role == 'receptionist' && req.url == '/employees/profile') || (req.user.role == 'user' && req.url == '/users/profile')))
		next();
	else if(req.user.role == 'system' && req.method == 'GET' && (
		req.url == '/employees' ||
		req.url == '/users' ||
		req.url == '/states' ||
		req.url == '/cities' ||
		req.url == '/locations' ||
		req.url == '/categories' ||
		req.url == '/classes' ||
		req.url == '/generations' ||
		req.url == '/employees/edit' ||
		req.url == '/users/edit' ||
		req.url == '/states/edit' ||
		req.url == '/cities/edit' ||
		req.url == '/locations/edit' ||
		req.url == '/categories/edit' ||
		req.url == '/classes/edit' ||
		req.url == '/generations/edit' ||
		req.url.substring(0, 16) == '/employees/edit/' ||
		req.url.substring(0, 12) == '/users/edit/' ||
		req.url.substring(0, 13) == '/states/edit/' ||
		req.url.substring(0, 13) == '/cities/edit/' ||
		req.url.substring(0, 16) == '/locations/edit/' ||
		req.url.substring(0, 17) == '/categories/edit/' ||
		req.url.substring(0, 14) == '/classes/edit/' ||
		req.url.substring(0, 18) == '/generations/edit/')
	)
		next();
	else if(req.user.role == 'nutrologist' && req.method == 'GET' && (
		req.url == '/users' ||
		req.url == '/users/edit' ||
		req.url.substring(0, 12) == '/users/edit/')
	)
		next();
	else if(req.user.role == 'receptionist' && req.method == 'GET' && (
		req.url == '/users' ||
		req.url == '/users/edit' ||
		req.url.substring(0, 12) == '/users/edit/')
	)
		next();
	else if(req.user.role == 'system' || req.user.role == 'nutrologist' || req.user.role == 'receptionist')
		res.redirect('/admin/users');
	else if(req.user.role == 'user')
		res.redirect('/admin/users/profile');
	else
		res.redirect('/admin/');
}
// General token controllers.
function clearUserToken(req, res){
	res.clearCookie('userToken', {path: '/'});
}
function formatURL(req, res, path){
	if(req.url.substr(-1) == '/' && req.url.length > 1){
		res.redirect(301, path + req.url.slice(0, -1));
		return true
	}
	return false;
}
function getUserToken(req, res){
	var userToken = req.body.userToken || req.query.userToken || req.params.userToken || req.cookies.userToken;
	return userToken;
}

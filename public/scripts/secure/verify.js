$(function(){
	var element = 'verify';
	$('#verify').click(function(){
		verify_(element);
	});
	$('#form').submit(function(){
		return false;
	});
});
function verify_(element){
	disabled_();
	var error = '';
	var verify = $('#form #_verify').val(),
		email = $('#form #email').val(),
		password = $('#form #password').val();
	if(verify.length > 0){
		email = undefined;
		if(password.length < 8){
			$('#password_').addClass('error');
			error += '<li>' + language['pleaseEnterYourPassword'] + '</li>';
		}
	}
	else{
		verify = undefined;
		if(email.length < 5){
			$('#email_').addClass('error');
			error += '<li>' + language['pleaseEnterYourEmail'] + '</li>';
		}
		else if(!validator.isEmail(email)){
			$('#email_').addClass('error');
			error += '<li>' + language['pleaseEnterAValidEmail'] + '</li>';
		}
	}
	if(error == ''){
		var data = {
			email: email,
			password: password
		};
		sendRequest_(element, data);
	}
	else
		error_(error);
}
